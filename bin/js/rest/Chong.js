var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
* name
*/
var rest;
(function (rest) {
    var Chong = /** @class */ (function (_super) {
        __extends(Chong, _super);
        function Chong() {
            return _super.call(this) || this;
        }
        /**
         * 获取问答题库
         */
        Chong.prototype.info = function (callback) {
            var headers = Rest.HEADERS.concat();
            headers[1] = localStorage.getItem("Token");
            Http.post(Rest.DHOSTS + "/dbex/activity/", JSON.stringify({}), Rest.RESPONSE_TYPE_JSON, headers, function (data) {
                callback && callback(data);
                console.log(data);
            });
        };
        Chong.prototype.getTopicId = function (callback) {
            var headers = Rest.HEADERS.concat();
            headers[1] = localStorage.getItem("Token");
            Http.post(Rest.DHOSTS + "/dbex/activity/topic", JSON.stringify({}), Rest.RESPONSE_TYPE_JSON, headers, function (data) {
                callback && callback(data);
                console.log(data);
            });
        };
        Chong.prototype.getTopic = function (topicId, callback) {
            var headers = Rest.HEADERS.concat();
            Http.get(Rest.DHOSTS + "/dbex/topic/" + topicId, Rest.RESPONSE_TYPE_JSON, headers, function (data) {
                callback && callback(data);
                console.log(data);
            });
        };
        Chong.prototype.answer = function (answers, callback) {
            var headers = Rest.HEADERS.concat();
            headers[1] = localStorage.getItem("Token");
            headers = headers.concat(["Content-Type", "application/json; charset=utf-8"]);
            Http.post(Rest.DHOSTS + "/dbex/activity/answer", answers, Rest.RESPONSE_TYPE_JSON, headers, function (data) {
                callback && callback(data);
                console.log(data);
            });
        };
        Chong.prototype.live = function (activityId, callback) {
            var headers = Rest.HEADERS.concat();
            headers[1] = localStorage.getItem("Token");
            headers = headers.concat(["Content-Type", "application/json; charset=utf-8"]);
            Http.post(Rest.DHOSTS + "/dbex/activity/revive/" + activityId, JSON.stringify({}), Rest.RESPONSE_TYPE_JSON, headers, function (data) {
                callback && callback(data);
                console.log(data);
            });
        };
        Chong.prototype.reward = function (activityId, callback) {
            var headers = Rest.HEADERS.concat();
            headers[1] = localStorage.getItem("Token");
            Http.get(Rest.DHOSTS + "/dbex/activity/getActivityRewards?activityId=" + activityId, Rest.RESPONSE_TYPE_JSON, headers, function (data) {
                callback && callback(data);
                console.log(data);
            });
        };
        Chong.prototype.getPrizeNotice = function (callback) {
            var headers = Rest.HEADERS.concat();
            headers[1] = localStorage.getItem("Token");
            Http.get(Rest.DHOSTS + "/dbex/activity/getNotReceivedActivityList", Rest.RESPONSE_TYPE_JSON, headers, function (data) {
                callback && callback(data);
                console.log(data);
            });
        };
        return Chong;
    }(Rest));
    rest.Chong = Chong;
    rest.chong = new Chong();
})(rest || (rest = {}));
//# sourceMappingURL=Chong.js.map