var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
* name
*/
var rest;
(function (rest) {
    var Friend = /** @class */ (function (_super) {
        __extends(Friend, _super);
        function Friend() {
            return _super.call(this) || this;
        }
        // 好友列表
        Friend.prototype.list = function (page, pageSize, callback) {
            Http.get(Rest.HOST + "/friends/list", Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                callback && callback(data);
            });
        };
        /**
         * 添加好友
         * @param nickName
         * @param callback
         */
        Friend.prototype.addFriend = function (_a, callback) {
            var nickName = _a.nickName;
            Http.post(Rest.HOST + "/friends/add", {
                nickName: nickName,
            }, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                callback && callback(data);
            });
        };
        /**
         *好友浇水记录
         * @param page
         * @param pageSize
         * @param callback
         */
        Friend.prototype.wateringRecord = function (callback) {
            Http.get(Rest.HOST + "/wateringRecord", Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                callback && callback(data);
            });
        };
        /**
         * 算力排行榜
         * @param page
         * @param pageSize
         * @param callback
         */
        Friend.prototype.bbmCalcPoint = function (page, pageSize, callback) {
            Http.get(Rest.DHOSTS + "/dbex/force/week", Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                callback && callback(data);
            });
            // Http.get(`${Rest.HOST}/rank/bbmCalcPoint`, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
            // 	callback && callback(data);
            // })
        };
        Friend.prototype.getAnking = function (page, pageSize, callback) {
            Http.get(Rest.HOST + "/dbex/getAnking", Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                callback && callback(data);
            });
        };
        return Friend;
    }(Rest));
    rest.friend = new Friend();
})(rest || (rest = {}));
//# sourceMappingURL=Friend.js.map