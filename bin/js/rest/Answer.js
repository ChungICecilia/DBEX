var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
* name
*/
var rest;
(function (rest) {
    var Answer = /** @class */ (function (_super) {
        __extends(Answer, _super);
        function Answer() {
            return _super.call(this) || this;
        }
        /**
         * 获取问答题库
         */
        Answer.prototype.getQuestion = function (callback) {
            var headers = Rest.HEADERS.concat();
            headers[1] = localStorage.getItem("Token");
            Http.post(Rest.DHOSTS + "/dbex/question/topic", JSON.stringify({}), Rest.RESPONSE_TYPE_JSON, headers, function (data) {
                callback && callback(data);
                console.log(data);
            });
        };
        Answer.prototype.getTopic = function (questionId, topicId, token, callback) {
            var headers = Rest.HEADERS.concat();
            headers[1] = localStorage.getItem("Token");
            Http.get(Rest.DHOSTS + "/dbex/topic/user/" + topicId, Rest.RESPONSE_TYPE_JSON, headers.concat(["questionId", questionId]), function (data) {
                callback && callback(data);
                console.log(data);
            });
        };
        Answer.prototype.setAnswerTime = function (questionId, topicId, callback) {
            var headers = Rest.HEADERS.concat();
            headers[1] = localStorage.getItem("Token");
            Http.get(Rest.DHOSTS + "/dbex/topic/user/answer/" + topicId, Rest.RESPONSE_TYPE_JSON, headers.concat(["questionId", questionId]), function (data) {
                callback && callback(data);
                console.log(data);
            });
        };
        Answer.prototype.submitAnswers = function (answers, callback) {
            var headers = Rest.HEADERS.concat();
            headers[1] = localStorage.getItem("Token");
            headers = headers.concat(["Content-Type", "application/json; charset=utf-8"]);
            Http.post(Rest.DHOSTS + "/dbex/questionLog/", answers, Rest.RESPONSE_TYPE_JSON, headers, function (data) {
                callback && callback(data);
                console.log(data);
            });
        };
        Answer.prototype.force = function (questionId, callback) {
            var headers = Rest.HEADERS.concat();
            headers[1] = localStorage.getItem("Token");
            Http.post(Rest.DHOSTS + "/dbex/questionLog/receive/force?questionId=" + questionId, JSON.stringify({}), Rest.RESPONSE_TYPE_JSON, headers, function (data) {
                callback && callback(data);
                console.log(data);
            });
        };
        Answer.prototype.getWrongList = function (questionId, callback) {
            var headers = Rest.HEADERS.concat();
            headers[1] = localStorage.getItem("Token");
            Http.get(Rest.DHOSTS + "/dbex/questionLog/getWrongAnsweredList?questionId=" + questionId, Rest.RESPONSE_TYPE_JSON, headers, function (data) {
                callback && callback(data);
            });
        };
        return Answer;
    }(Rest));
    rest.answer = new Answer();
})(rest || (rest = {}));
//# sourceMappingURL=Answer.js.map