var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
* name
*/
var rest;
(function (rest) {
    var News = /** @class */ (function (_super) {
        __extends(News, _super);
        function News() {
            return _super.call(this) || this;
        }
        News.prototype.list = function (page, pageSize, callback) {
            Http.get(Rest.HOST + "/news/list", Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                console.log(data);
                callback && callback(data);
            });
        };
        News.prototype.info = function (id, callback) {
            Http.get(Rest.HOST + "/api/news/" + id, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                //console.log(data);
                callback && callback(data);
            });
        };
        News.prototype.read = function (id, callback) {
            Http.put(Rest.HOST + "/api/news/read/" + id, JSON.stringify({}), Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                //console.log(data);
                callback && callback(data);
            });
        };
        return News;
    }(Rest));
    rest.news = new News();
})(rest || (rest = {}));
//# sourceMappingURL=News.js.map