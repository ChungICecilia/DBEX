/*
* name;
*/
var rest;
(function (rest) {
    var Draw = /** @class */ (function () {
        function Draw() {
        }
        Draw.prototype.list = function (callback) {
            var headers = Rest.HEADERS.concat();
            headers[1] = localStorage.getItem("Token");
            Http.get(Rest.DHOSTS + "/dbex/rewardSector/list", Rest.RESPONSE_TYPE_JSON, headers, function (data) {
                callback && callback(data);
                console.log(data);
            });
        };
        Draw.prototype.drawPrize = function (type, callback) {
            var headers = Rest.HEADERS.concat();
            headers[1] = localStorage.getItem("Token");
            Http.get(Rest.DHOSTS + "/dbex/rewardSector/randomDraw/" + type, Rest.RESPONSE_TYPE_JSON, headers, function (data) {
                callback && callback(data);
                console.log(data);
            });
        };
        return Draw;
    }());
    rest.draw = new Draw();
})(rest || (rest = {}));
//# sourceMappingURL=Draw.js.map