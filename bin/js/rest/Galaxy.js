var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
* name
*/
var rest;
(function (rest) {
    var Galaxy = /** @class */ (function (_super) {
        __extends(Galaxy, _super);
        function Galaxy() {
            return _super.call(this) || this;
        }
        /**
         * 查询推荐星系节点
         * @param callback
         */
        Galaxy.prototype.groomGalaxy = function (callback) {
            Http.get(Rest.HOST + "/galaxy/getRecs", Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                //console.log(data);
                callback && callback(data);
            });
        };
        //星球大战前景色
        /**
         * 查询全部星系节点
         * @param callback
         */
        Galaxy.prototype.allGalaxy = function (callback) {
            Http.get(Rest.HOST + "/galaxy/listAll", Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                callback && callback(data);
            });
        };
        /**
         * 设置星系
         * @param galaxyld
         */
        Galaxy.prototype.setGalaxy = function (_a, callback) {
            var galaxyld = _a.galaxyld;
            Http.post(Rest.HOST + "/player/chooseGalaxy", {
                galaxyId: galaxyld,
            }, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                callback && callback(data);
                //console.log(data);
            });
        };
        return Galaxy;
    }(Rest));
    rest.galaxy = new Galaxy();
})(rest || (rest = {}));
//# sourceMappingURL=Galaxy.js.map