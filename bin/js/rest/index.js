/*
* name;
*/
var Browser = Laya.Browser;
var Rest = /** @class */ (function () {
    function Rest() {
    }
    Rest.prototype._setUserInfo = function (user) {
        Rest.USERINFO = user.data.player;
        console.log(Rest.USERINFO);
    };
    Rest.prototype._setOption = function (option) {
        Rest.OPTION = option;
    };
    Rest.prototype._setInviteCode = function (inviteCode) {
        Rest.INVITECODE = inviteCode;
        console.log(Rest.INVITECODE);
    };
    /**
    * 登录成功
    * @param token
    */
    Rest.prototype._login = function (token, type) {
        Rest.TOKEN = token;
        Rest.TOKEN_TYPE = type;
        Rest.HEADERS = Rest.CONTENT_TYPE_JSON.concat(['Authorization', type + ' ' + token]);
        localStorage.setItem('Token', token);
        var Token = localStorage.getItem("Token");
        console.log(Token);
    };
    /**
     * 登出
     */
    Rest.prototype._logout = function () {
        Rest.TOKEN = '';
        Rest.HEADERS = Rest.CONTENT_TYPE_JSON;
    };
    // public static HOSTS: String = 'http://192.168.31.23:8101';//测试登录注册发短息
    // public static DHOSTS: String = 'http://192.168.31.23:9005';//晨曦新游戏功能接口
    // // //线上测试服
    Rest.HOST = 'http://39.108.117.15:9005'; //游戏功能接口
    Rest.HOSTS = 'http://test.bcrealm.com/api/user'; //登录注册发短息
    Rest.DHOSTS = 'http://test.bcrealm.com/api'; //登录注册发短息
    // 线上正式服
    // public static HO0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000S222222222222222222222222222222                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                2222222222T: String = 'http://47.105.106.146:9005';//游戏功能接口
    // public static HOSTS: String = 'http://api.bcrealm.com/api/user';//登录注册发短息
    // public static DHOSTS: String = 'http://api.bcrealm.com/api';//登录注册发短息
    Rest.USERINFO = {
        baseBbm: 0, bbmCalcPoint: 0, starterBbm: 0, bgmStatus: 0, btnMusicStatus: 0, guidMoveSteps: 0, headIcon: '', id: 0,
        nickName: "", pickableCoins: 0, selfWateringTimes: 0, upgradeToNextStatus: 0, wateringFriendsTimes: 0, yqsCurrentExps: 0,
        yqsGrade: 0, yqsNextGradeExps: 0, marketBbm: 0, idcardStatus: 0, newsCalcPoint: 0,
    };
    Rest.GALAXY = {
        id: 0, galaxyName: "", galaxyPower: "", rankPosi: "", lastUpdateTimestamp: "", rankLevel: "",
    };
    Rest.INVITECODE = '';
    Rest.OPTION = {};
    Rest.TOKEN = '';
    Rest.TOKEN_TYPE = '';
    Rest.RESPONSE_TYPE_JSON = 'json';
    Rest.RESPONSE_TYPE_TEXT = 'text';
    Rest.CONTENT_TYPE_JSON = [];
    Rest.HEADERS = Rest.CONTENT_TYPE_JSON;
    Rest.COINSLIST = [];
    Rest.SKIP = false;
    return Rest;
}());
//# sourceMappingURL=index.js.map