var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
* name
*/
var rest;
(function (rest) {
    var User = /** @class */ (function (_super) {
        __extends(User, _super);
        function User() {
            return _super.call(this) || this;
        }
        // 登录
        // public login({name, password}, callback): void {
        // 	Http.post(`${Rest.HOST}/admin/login`, JSON.stringify({
        // 		name: name,
        // 		password: password
        // 	}), Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
        // 		// 设置token
        // 		super._login(data.token)
        // 		// 同步登录用户信息
        // 		this.userInfo((data) => {
        // 			if (data.stauts == 'success') {
        // 				super._setUserInfo(data.data)
        // 			} else {
        // 				console.log(data.description)
        // 			}
        // 			callback && callback(data)
        // 		})
        // 	})
        // }
        // // 注册
        // public register({name, password, code}, callback): void {
        // 	Http.post(`${Rest.HOST}/admin/register`, JSON.stringify({
        // 		name: name,
        // 		password: password,
        // 		code: code,
        // 	}), Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
        // 		callback && callback(data)
        // 	})
        // }
        // // 登出
        // public logout(callback): void {
        // 	Http.post(`${Rest.HOST}/admin/logout`,
        // 		'', Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
        // 			super._logout()
        // 			callback && callback(data)
        // 		})
        // }
        /**
         * 注册 http://192.168.31.62:8070/user/
         * @param param0
         * @param callback
         */
        User.prototype.register = function (_a, callback) {
            var phone = _a.phone, code = _a.code, passWord = _a.passWord, userCode = _a.userCode, userId = _a.userId;
            //http://api.test.bcrealm.com:8080/user/register
            Http.post(Rest.HOSTS + "/user/register", {
                code: code,
                fromChannel: 1,
                fromType: 3,
                password: passWord,
                phone: phone,
                userCode: userCode,
                userId: "",
                userLogoImg: "",
            }, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS.concat(["Content-Type", "application/json; charset=utf-8"]), function (data) {
                callback && callback(data);
            });
        };
        /**
         *确认修改密码
         * @param param0
         * @param callback
         */
        User.prototype.changePassword = function (_a, callback) {
            var code = _a.code, passWord = _a.passWord, phone = _a.phone, repeatPassword = _a.repeatPassword;
            //http://api.test.bcrealm.com:8080/user/register
            Http.post(Rest.HOSTS + "/user/back/pwd", {
                code: code,
                password: passWord,
                phone: phone,
                repeatPassword: repeatPassword,
            }, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS.concat(["Content-Type", "application/json; charset=utf-8"]), function (data) {
                callback && callback(data);
            });
        };
        //获取注册验证码  /user/back/ /user/back/msg
        User.prototype.getSMS = function (_a, callback) {
            var phone = _a.phone, type = _a.type;
            Http.post(Rest.HOSTS + "/user/send/msg?phone=" + phone, {
                phone: phone,
            }, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                callback && callback(data);
            });
        };
        /**
         *修改密码获取短信
         * @param param0
         * @param callback
         */
        User.prototype.getMSBack = function (_a, callback) {
            var phone = _a.phone, type = _a.type;
            Http.post(Rest.HOSTS + "/user/back/msg?phone=" + phone, {
                phone: phone,
            }, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                callback && callback(data);
            });
        };
        // 登录
        User.prototype.login = function (_a, callback) {
            var _this = this;
            var name = _a.name, password = _a.password, invite_code = _a.invite_code;
            Http.post(Rest.HOSTS + "/user/login", {
                userName: name,
                password: password,
                fromChannel: 1,
                fromType: 3,
                invite_code: invite_code
            }, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS.concat(["Content-Type", "application/json; charset=utf-8"]), function (data) {
                console.log(data);
                if (data.code == 1) {
                    if (data.data.token != null) {
                        // 设置token
                        _super.prototype._login.call(_this, data.data.token, '');
                        _this.userInfo({ invite_code: invite_code }, function (userInfo) {
                            //console.log(userInfo)
                            if (userInfo.status != "error") {
                                _super.prototype._setUserInfo.call(_this, userInfo);
                                Rest.SKIP = data.data.isSkip;
                                rest.user.coinsList(function (param) {
                                    console.log(param);
                                    if (param.status == "error") {
                                        console.log("拉取coins信息失败");
                                    }
                                    else {
                                        Rest.COINSLIST = param.data;
                                    }
                                });
                            }
                            callback(data); ///
                            // this.option((optionInfo) => {
                            // 	super._setOption(optionInfo);
                            // 	callback(data);
                            // });
                        });
                    }
                }
                else {
                    callback(data);
                }
            });
        };
        // 注册
        // public register({phone, code, passWord, userCode,userId}, callback): void {
        // 	Http.post(`${Rest.HOST}/v1/user/register`, {
        // 		phone: phone,
        // 		passWord: passWord,
        // 		code: code,
        // 		userCode: userCode,
        // 	}, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
        // 		callback && callback(data);
        // 	})
        // }
        // //获取验证码
        // public getSMS({phone, type}, callback): void {
        // 	Http.post(`${Rest.HOST}/v1/user/getSMS`, {
        // 		phone: phone,
        // 		type: type,
        // 	}, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
        // 		callback && callback(data);
        // 	})
        // }
        // // 登录
        // public login({name, password, invite_code}, callback): void {
        // 	Http.post(`${Rest.HOST}/jwtToken`, {//`${Rest.HOST}/jwtToken`
        // 		username: name,
        // 		password: password,
        // 		grant_type: 'password',
        // 		invite_code: invite_code
        // 	}, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS.concat(['Authorization', 'Basic YWRtaW46YWRtaW4=']), (data) => {//'Authorization', 'Basic YWRtaW46YWRtaW4='
        // 		console.log(data);
        // 		if (data.token != null) {
        // 			// 设置token
        // 			super._login(data.token, 'Bearer')
        // 			var inviteCode: '';
        // 			this.userInfo(inviteCode, (userInfo) => {
        // 				//console.log(userInfo)
        // 				if (userInfo.status != "error") {
        // 					super._setUserInfo(userInfo);
        // 				}
        // 				callback(data);///
        // 				// this.option((optionInfo) => {
        // 				// 	super._setOption(optionInfo);
        // 				// 	callback(data);
        // 				// });
        // 			})
        // 		} else {
        // 			callback(data);
        // 		}
        // 	})
        // }
        /*
     * 获取邀请码
     */
        User.prototype.getInviteCode = function (callback) {
            Http.get(Rest.HOST + "/v1/user/getInviteCode", Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                callback && callback(data);
            });
        };
        /**
         * 查询注册验证码是否正确
         * @param param0
         * @param callback
         */
        User.prototype.Msg = function (_a, callback) {
            var code = _a.code, phone = _a.phone;
            Http.post(Rest.HOSTS + "/user/Msg/", {
                phone: phone,
                code: code
            }, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS.concat(["Content-Type", "application/json; charset=utf-8"]), function (data) {
                callback && callback(data);
                console.log(data);
            });
        };
        /**
         * 查询更改密码验证码是否正确
         * @param param0
         * @param callback
         */
        User.prototype.sMsg = function (_a, callback) {
            var code = _a.code, phone = _a.phone;
            Http.post(Rest.HOSTS + "/user/back/", {
                phone: phone,
                code: code
            }, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS.concat(["Content-Type", "application/json; charset=utf-8"]), function (data) {
                callback && callback(data);
                console.log(data);
            });
        };
        /**
         * 用户信息
         * @param callback
         */
        User.prototype.userInfo = function (inviteCode, callback) {
            Http.post(Rest.HOST + "/player/details", { inviteCode: inviteCode }, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                callback && callback(data);
                console.log(data);
            });
        };
        /**
         * 平台入口来的用户需发送
         * @param callback
         */
        User.prototype.onHandoverGame = function (callback) {
            Http.get(Rest.HOSTS + "/user/handoverGame", Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                callback && callback(data);
            });
        };
        /**
         * 请求coinsList信息
         *
         */
        User.prototype.coinsList = function (callback) {
            Http.get(Rest.DHOSTS + "/dbex/coins/list", Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (param) {
                callback && callback(param);
            });
        };
        // 配置列表
        User.prototype.option = function (callback) {
            Http.get(Rest.HOST + "/option/list", Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                callback && callback(data);
            });
        };
        //实名认证
        User.prototype.realName = function (_a, callback) {
            var name = _a.name, card = _a.card;
            Http.post(Rest.HOST + "/idcardAuth", {
                idcardName: name,
                idcardNo: card,
            }, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                callback && callback(data);
            });
        };
        //红点提示
        User.prototype.redPoint = function (callback) {
            Http.get(Rest.HOST + "/unfinished", Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                console.log(data);
                callback && callback(data);
            });
        };
        /**
         * 创建玩家角色
         * @param param0
         * @param callback
         */
        User.prototype.createRole = function (_a, callback) {
            var userName = _a.userName, headIcon = _a.headIcon, invite_code = _a.invite_code;
            Http.post(Rest.HOST + "/player/createPlayer", {
                userName: userName,
                headIcon: headIcon,
                invite_code: invite_code,
            }, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                callback && callback(data);
                //console.log(data);
            });
        };
        /**
         * 设置音乐音效
         * @param type
         * @param status
         * @param callback
         */
        User.prototype.setMusicStatus = function (_a, callback) {
            var type = _a.type, status = _a.status;
            Http.post(Rest.HOST + "/player/setMusicStatus", {
                type: type,
                status: status,
            }, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                callback && callback(data);
            });
        };
        /**
         *更新新手指引步骤
         * @param param0
         * @param callback
         */
        User.prototype.moveGuidingSteps = function (_a, callback) {
            var step = _a.step;
            Http.post(Rest.HOST + "/player/moveGuidingSteps", {
                step: step,
            }, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                callback && callback(data);
                console.log(data);
            });
        };
        return User;
    }(Rest));
    rest.user = new User();
})(rest || (rest = {}));
//# sourceMappingURL=User.js.map