var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
* name
*/
var rest;
(function (rest) {
    var Notice = /** @class */ (function (_super) {
        __extends(Notice, _super);
        function Notice() {
            return _super.call(this) || this;
        }
        Notice.prototype.list = function (callback) {
            Http.get(Rest.DHOSTS + "/dbex/notice/list", Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                // console.log(data)
                callback && callback(data);
            });
        };
        return Notice;
    }(Rest));
    rest.notice = new Notice();
})(rest || (rest = {}));
//# sourceMappingURL=Notice.js.map