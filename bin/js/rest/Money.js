var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
* name
*/
var rest;
(function (rest) {
    var Money = /** @class */ (function (_super) {
        __extends(Money, _super);
        function Money() {
            return _super.call(this) || this;
        }
        Money.prototype.total = function (page, pageSize, callback) {
            Http.get(Rest.HOST + "/api/money/total", Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                //console.log(data);
                callback && callback(data);
            });
        };
        Money.prototype.add = function (num, callback) {
            Http.post(Rest.HOST + "/api/money/add", JSON.stringify({
                num: num
            }), Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                //console.log(data);
                callback && callback(data);
            });
        };
        return Money;
    }(Rest));
    rest.money = new Money();
})(rest || (rest = {}));
//# sourceMappingURL=Money.js.map