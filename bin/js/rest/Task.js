var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var rest;
(function (rest) {
    var Task = /** @class */ (function (_super) {
        __extends(Task, _super);
        function Task() {
            return _super.call(this) || this;
        }
        /**
         * 获取任务列表
         * @param callback
         */
        Task.prototype.tasks = function (callback) {
            Http.get(Rest.HOST + "/tasks", Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                callback && callback(data);
            });
        };
        /**
         * 领取任务奖励
         * @param param0
         * @param callback
         */
        Task.prototype.askRewards = function (taskId, callback) {
            Http.post(Rest.HOST + "/askRewards", {
                taskId: taskId
            }, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                callback && callback(data);
            });
        };
        return Task;
    }(Rest));
    rest.task = new Task();
})(rest || (rest = {}));
//# sourceMappingURL=Task.js.map