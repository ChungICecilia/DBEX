var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
* name
*/
var rest;
(function (rest) {
    var Mail = /** @class */ (function (_super) {
        __extends(Mail, _super);
        function Mail() {
            return _super.call(this) || this;
        }
        /**
         * 获取邮件列表
         * @param page
         * @param pageSize
         * @param callback
         */
        Mail.prototype.list = function (page, pageSize, callback) {
            Http.get(Rest.HOST + "/api/mail", Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                //console.log(data);
                callback && callback(data);
            });
        };
        /**
         * 获取指定邮件的详情
         * @param id
         * @param callback
         */
        Mail.prototype.info = function (id, callback) {
            Http.get(Rest.HOST + "/api/mail/" + id, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                //console.log(data);
                callback && callback(data);
            });
        };
        /**
         * 阅读一封邮件
         * @param id
         * @param callback
         */
        Mail.prototype.read = function (id, callback) {
            Http.put(Rest.HOST + "/api/mail/read/" + id, '', Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                //console.log(data);
                callback && callback(data);
            });
        };
        /**
         * 阅读所有的邮件
         * @param ids
         * @param callback
         */
        Mail.prototype.readAll = function (callback) {
            Http.put(Rest.HOST + "/api/mail/read", JSON.stringify({}), Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                //console.log(data);
                callback && callback(data);
            });
        };
        /**
         * 删除所有的邮件
         * @param ids
         * @param callback
         */
        Mail.prototype.deleteAll = function (callback) {
            Http.delete(Rest.HOST + "/api/mail/deleteAll", Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                //console.log(data);
                callback && callback(data);
            });
        };
        return Mail;
    }(Rest));
    rest.mail = new Mail();
})(rest || (rest = {}));
//# sourceMappingURL=Mail.js.map