var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
* name
*/
var rest;
(function (rest) {
    var Work = /** @class */ (function (_super) {
        __extends(Work, _super);
        function Work() {
            return _super.call(this) || this;
        }
        /**
         * 给自己浇水
         * @param callback
         */
        Work.prototype.wateringTree = function (callback) {
            Http.get(Rest.HOST + "/wateringTree", Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                //console.log(data);
                callback && callback(data);
            });
        };
        /**
         * 升级树
         * @param callback
         */
        Work.prototype.upgradeTree = function (callback) {
            Http.get(Rest.HOST + "/upgradeTree", Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                callback && callback(data);
            });
        };
        /**
         * 给好友浇水
         * @param friendId
         * @param callback
         */
        Work.prototype.wateringFriendsTree = function (_a, callback) {
            var friendId = _a.friendId;
            Http.post(Rest.HOST + "/wateringFriendsTree", {
                friendId: friendId,
            }, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (data) {
                callback && callback(data);
                //console.log(data)
            });
        };
        /**
         * 领取每天刷新的金币
         * @param callback
         */
        // public pickCoin(callback): void {
        // 	Http.get(`${Rest.HOST}/pickCoin`, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
        // 		callback && callback(data);
        // 	})
        // }
        /**
         * 新的领取每天刷新的金币
         * @param callback
         */
        Work.prototype.pickCoin = function (_a, callback) {
            var id = _a.id;
            Http.post(Rest.DHOSTS + "/dbex/coins/gainCoinsBbm", { id: id }, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, function (param) {
                callback && callback(param);
            });
        };
        return Work;
    }(Rest));
    rest.work = new Work();
})(rest || (rest = {}));
//# sourceMappingURL=Work.js.map