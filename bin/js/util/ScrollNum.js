var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Texture = laya.resource.Texture;
var NetLoader = laya.net.Loader;
/**
 * 数字滚动组件
 */
var ScrollNum = /** @class */ (function (_super) {
    __extends(ScrollNum, _super);
    function ScrollNum() {
        var _this = _super.call(this) || this;
        _this.masks = new Laya.Sprite();
        _this.cols = [];
        _this.value = 0;
        return _this;
    }
    ScrollNum.prototype.resLoaded = function (num1) {
        this.adds = NetLoader.getAtlas("res/atlas/num.atlas");
        this.makeCol(this.value, num1);
        this.value = num1;
    };
    ScrollNum.prototype.makeCol = function (num, num1) {
        this.removeChildren(0, this.numChildren);
        var le = String(num1).length - String(num).length;
        var numStr = String(num);
        var numStr1 = String(num1);
        if (le > 0) {
            while (le > 0) {
                numStr += "0";
                le--;
            }
        }
        var len = numStr.length;
        var txt;
        while (this.cols.length < len) {
            var sm = this.cols.length;
            var max = len - this.cols.length;
            var col = void 0;
            for (var k = 0; k < max; k++) {
                var offY = 0;
                col = new Laya.Sprite();
                for (var i = 0; i < 30; i++) {
                    var j = 0;
                    if (i > 9 && i <= 19) {
                        j = i - 10;
                    }
                    else if (i > 19) {
                        j = i - 20;
                    }
                    else {
                        j = i;
                    }
                    txt = NetLoader.getRes(this.adds[j]);
                    col.graphics.drawTexture(txt, 0, offY, 15, 20);
                    offY += 20;
                }
            }
            this.cols.push(col);
        }
        var offX = 0;
        var dfsd = this.cols.length;
        for (var i = 0; i < len; i++) {
            var n = 0;
            var ss = numStr.substr(i, 1);
            if (ss == ".")
                n = 0;
            else
                n = parseFloat(ss);
            this.addChild(this.cols[i]);
            this.cols[i].y = -n * 20;
            this.cols[i].x = offX;
            //var anys:Laya.Sprite[] = ScrollNum.cols;
            offX += 15;
        }
        this.masks.graphics.clear();
        this.masks.graphics.drawRect(0, 0, 1500, 20, "#FFFFFF"); //
        this.mask = this.masks;
        var le = String(num1).length;
        var loi = false;
        var po;
        for (var i = 0; i < le; i++) {
            if (numStr1.substr(i, 1) == ".") {
                var col_1 = this.cols[i];
                po = new Laya.Sprite();
                var txt_1 = NetLoader.getRes(this.adds[10]);
                po.graphics.drawTexture(txt_1, 0, 0, 15, 20);
                this.removeChild(col_1);
                this.addChild(po);
                po.x = col_1.x;
                po.y = 0;
                continue;
            }
            var l = parseFloat(numStr.substr(i, 1));
            if (!l)
                l = 0;
            var l1 = parseFloat(numStr1.substr(i, 1));
            if (l == l1) {
                if (!loi)
                    continue;
            }
            loi = true;
            var ge = 0;
            if (l1 > l) {
                ge = l1 - l;
            }
            else {
                ge = 10 - l + l1;
            }
            var col = this.cols[i];
            Laya.Tween.to(col, { y: col.y - 20 * 10 - ge * 20 }, 1000, Laya.Ease.linearIn, null);
        }
        //
        var dd = this.cols.length;
        if (le < dd) {
            while (dd > le) {
                this.removeChild(this.cols[dd - 1]);
                dd--;
            }
        }
    };
    ScrollNum.prototype.setNum = function (num1) {
        Laya.loader.load(["res/atlas/num.atlas"], new Laya.Handler(this, this.resLoaded, [num1]));
    };
    return ScrollNum;
}(Laya.Sprite));
//# sourceMappingURL=ScrollNum.js.map