var DateUtil = /** @class */ (function () {
    function DateUtil() {
    }
    //返回yy:mm:dd格式时间
    DateUtil.second2String = function (second) {
        var hour = Math.floor(second / 3600);
        var minute = Math.floor(second / 60) % 60;
        var second = Math.floor(second % 60);
        var str;
        var h = hour < 10 ? ('0' + hour) : hour;
        var m = minute < 10 ? ('0' + minute) : minute;
        var s = second < 10 ? ('0' + second) : second;
        return h + ":" + m + ":" + s;
    };
    //    public static second2String(second: number): string {
    //     var hour: number = Math.floor(second / 3600)
    //     var minute: number = Math.floor(second / 60) % 60
    //     var second: number = Math.floor(second % 60)
    //     var str: string;
    //     if (hour > 0) {
    //         return `${hour}:${minute}:${second}`
    //     } else if (minute > 0) {
    //         return `${minute}:${second}`
    //     } else {
    //         return `${second}`
    //     }
    // }
    // 转成成简单的时间格式
    DateUtil.second2SimpleStr = function (second) {
        var hour = Math.floor(second / 3600);
        if (hour > 0) {
            return hour + "\u5C0F\u65F6";
        }
        var minute = Math.floor(second / 60) % 60;
        if (minute > 0) {
            return minute + "\u5206\u949F";
        }
        var second = Math.floor(second % 60);
        if (second > 0) {
            return '1分钟';
        }
        return '0分钟';
    };
    DateUtil.Timestamp = function (time) {
        var str = time.replace(/-/g, '/');
        var date = new Date(str);
        var t = date.getTime();
        return t;
    };
    return DateUtil;
}());
//# sourceMappingURL=DateUtil.js.map