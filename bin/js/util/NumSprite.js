var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/*
* name;
*/
// import Texture = laya.resource.Texture;
// import NetLoader = laya.net.Loader;
var NumSprite = /** @class */ (function (_super) {
    __extends(NumSprite, _super);
    function NumSprite() {
        var _this = _super.call(this) || this;
        _this.row = new Laya.Sprite();
        return _this;
    }
    NumSprite.prototype.setNumber = function (num, url, gap) {
        if (gap === void 0) { gap = 0; }
        this.num = num;
        this.gap = gap;
        this.url = url;
        this.loadRes();
    };
    //res/atlas/num.atlas
    NumSprite.prototype.loadRes = function () {
        Laya.loader.load([this.url], new Laya.Handler(this, this.resLoaded));
    };
    NumSprite.prototype.resLoaded = function () {
        this.adds = NetLoader.getAtlas(this.url);
        this.removeChildren(0, this.numChildren);
        this.make();
    };
    NumSprite.prototype.make = function () {
        this.row.graphics.clear();
        var str = this.num.toString();
        var offX = 0;
        var i;
        for (i = 0; i < str.length; i++) {
            var n = Number(str.charAt(i));
            this.txt = NetLoader.getRes(this.adds[n]);
            this.row.graphics.drawTexture(this.txt, offX, 0, this.txt.width, this.txt.height);
            offX += (this.txt.width + this.gap);
        }
        this.addChild(this.row);
        this.totalWidth = (this.txt.width + this.gap) * (i - 1) + this.txt.width;
        this.event("LoadOk");
    };
    return NumSprite;
}(Laya.Sprite));
//# sourceMappingURL=NumSprite.js.map