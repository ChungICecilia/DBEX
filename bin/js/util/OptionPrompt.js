/*
* name;
*/
var OptionPrompt = /** @class */ (function () {
    function OptionPrompt() {
        this.imgurl = "";
        this.imgCache = [];
        this.isPlay = false;
        this.imgurl = "res/singleImages/tips.png";
    }
    Object.defineProperty(OptionPrompt, "ins", {
        get: function () {
            return OptionPrompt._ins ? OptionPrompt._ins : new OptionPrompt();
        },
        enumerable: true,
        configurable: true
    });
    OptionPrompt.prototype.play = function (msg, pos) {
        if (pos === void 0) { pos = [Laya.stage.width / 2, Laya.stage.height / 2]; }
        var prompt;
        if (msg == null) {
            return;
        }
        if (this.imgCache.length > 0) {
            prompt = this.imgCache.pop();
            prompt.alpha = 1;
        }
        else {
            prompt = new Laya.Image();
            prompt.sizeGrid = "21,73,28,56";
            prompt.skin = this.imgurl;
            prompt.mouseThrough = true;
            prompt.alpha = 1;
            //prompt.graphics.drawTexture(imgurl,0,0,imgurl.width,imgurl.height);
            var htmlText = new Laya.Text;
            htmlText.fontSize = 24;
            htmlText.color = "#ffffff";
            htmlText.align = "center";
            //htmlText.wordWrap = true;
            prompt["txt"] = htmlText;
            prompt.addChild(htmlText);
            prompt.zOrder = 10001;
            prompt.height = 59;
        }
        prompt["txt"].text = msg;
        prompt["txt"].x = 20;
        prompt.width = prompt["txt"].textWidth + 40;
        prompt.x = pos[0] - prompt.width / 2;
        prompt.y = pos[1] - prompt.height / 2;
        Laya.stage.addChild(prompt);
        Laya.timer.once(1000, this, this.clearMsg, [prompt], false);
        //166 59
        prompt["txt"].y = (prompt.height - prompt["txt"].textHeight) / 2;
        //this.piao(prompt);
    };
    OptionPrompt.prototype.clearMsg = function (msg) {
        Laya.Tween.to(msg, { alpha: 0 }, 1500, null, Laya.Handler.create(this, this.remove, [msg]));
    };
    OptionPrompt.prototype.remove = function (msg) {
        msg.removeSelf();
        msg.alpha = 1;
        this.imgCache.push(msg);
    };
    /**piao动画*/
    OptionPrompt.prototype.piao = function (prompt) {
        Laya.Tween.to(prompt, { y: prompt.y - 300 }, 800);
    };
    return OptionPrompt;
}());
//# sourceMappingURL=OptionPrompt.js.map