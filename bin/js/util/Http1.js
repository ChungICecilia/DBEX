var Http1 = /** @class */ (function () {
    function Http1() {
    }
    Http1.json2Params = function (json) {
        return Object.keys(json).map(function (key) {
            return encodeURIComponent(key) + "=" + encodeURIComponent(json[key]);
        }).join("&");
    };
    /**
     * Get 查询资源
     * @param url
     * @param responseType
     * @param headers
     * @param complete
     */
    Http1.get = function (url, responseType, headers, complete) {
        var xhr = new Laya.HttpRequest();
        xhr.http.timeout = 10000; //设置超时时间；
        xhr.once(Laya.Event.COMPLETE, this, complete);
        xhr.send(url, "", "get", responseType, headers);
    };
    /**
     * Post 提交资源
     * @param url
     * @param data
     * @param responseType
     * @param headers
     * @param complete
     */
    Http1.post = function (url, data, responseType, headers, complete) {
        var xhr = new Laya.HttpRequest();
        xhr.http.timeout = 10000; //设置超时时间；
        xhr.once(Laya.Event.COMPLETE, this, complete);
        xhr.send(url, data, "post", responseType, headers);
    };
    /**
     * Put 资源进行新增或者修改
     * @param url
     * @param data
     * @param responseType
     * @param headers
     * @param complete
     */
    Http1.put = function (url, data, responseType, headers, complete) {
        var xhr = new Laya.HttpRequest();
        xhr.http.timeout = 10000; //设置超时时间；
        xhr.once(Laya.Event.COMPLETE, this, complete);
        xhr.send(url, data, "put", responseType, headers);
    };
    /**
     * Patch 对已知资源进行局部更新
     * @param url
     * @param responseType
     * @param headers
     * @param complete
     */
    Http1.patch = function (url, responseType, headers, complete) {
        var xhr = new Laya.HttpRequest();
        xhr.http.timeout = 10000; //设置超时时间；
        xhr.once(Laya.Event.COMPLETE, this, complete);
        xhr.send(url, "", "patch", responseType, headers);
    };
    /**
     * Delete 删除
     * @param url
     * @param responseType
     * @param headers
     * @param complete
     */
    Http1.delete = function (url, responseType, headers, complete) {
        var xhr = new Laya.HttpRequest();
        xhr.http.timeout = 10000; //设置超时时间；
        xhr.once(Laya.Event.COMPLETE, this, complete);
        xhr.send(url, "", "delete", responseType, headers);
    };
    return Http1;
}());
//# sourceMappingURL=Http1.js.map