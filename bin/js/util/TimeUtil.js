/*
* name;
*/
var TimeUtil = /** @class */ (function () {
    function TimeUtil() {
    }
    /**
         * 毫秒转时间格式(00:00:00)
         * @param msec
         * @return
         *
         */
    TimeUtil.msec2THMS3 = function (msec, isContainsSec) {
        if (isContainsSec === void 0) { isContainsSec = true; }
        var sec = Math.floor(msec / 1000);
        var hours = Math.floor(sec / 3600);
        var minutes = Math.floor(Math.floor(sec % 3600) / 60);
        var seconds = Math.floor(Math.floor(sec % 3600) % 60);
        if (hours == 0) {
            if (isContainsSec) {
                return minutes + ":" + seconds;
            }
            else {
                return minutes + "分";
            }
        }
        else {
            if (isContainsSec) {
                return hours + ":" + minutes + ":" + seconds;
            }
            else {
                return hours + ":" + minutes;
            }
        }
    };
    return TimeUtil;
}());
//# sourceMappingURL=TimeUtil.js.map