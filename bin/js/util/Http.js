var Http = /** @class */ (function () {
    function Http() {
    }
    /**
     * Get 查询资源
     * @param url
     * @param responseType
     * @param headers
     * @param complete
     */
    Http.get = function (url, responseType, headers, complete) {
        var xhr = new Laya.HttpRequest();
        xhr.http.timeout = 10000; //设置超时时间；
        xhr.http.withCredentials = false;
        xhr.once(Laya.Event.COMPLETE, this, complete);
        xhr.send(url, Http.formatData({}), "get", responseType, headers);
    };
    /**
     * Post 提交资源
     * @param url
     * @param data
     * @param responseType
     * @param headers
     * @param complete
     */
    Http.post = function (url, data, responseType, headers, complete, user, password) {
        var xhr = new Laya.HttpRequest();
        xhr.http.timeout = 10000; //设置超时时间；
        xhr.http.withCredentials = false;
        // xhr.http.user = "admin";
        // xhr.http.password = "admin";
        xhr.once(Laya.Event.COMPLETE, this, complete);
        xhr.send(url, Http.formatData(data), "post", responseType, headers); //formatData(data)
    };
    /**
     *
     * @param url
     * @param data
     * @param timeout
     * @param responseType
     * @param headers
     * @param complete
     * @param user
     * @param password
     */
    Http.postWithTimeout = function (url, data, timeout, responseType, headers, complete, user, password) {
        var xhr = new Laya.HttpRequest();
        xhr.http.timeout = timeout; //设置超时时间；
        xhr.http.withCredentials = false;
        // xhr.http.user = "admin";
        // xhr.http.password = "admin";
        xhr.once(Laya.Event.COMPLETE, this, complete);
        xhr.send(url, Http.formatData(data), "post", responseType, headers);
    };
    /**
     * Put 资源进行新增或者修改
     * @param url
     * @param data
     * @param responseType
     * @param headers
     * @param complete
     */
    Http.put = function (url, data, responseType, headers, complete) {
        var xhr = new Laya.HttpRequest();
        xhr.http.timeout = 10000; //设置超时时间；
        xhr.http.withCredentials = false;
        xhr.once(Laya.Event.COMPLETE, this, complete);
        xhr.send(url, Http.formatData(data), "put", responseType, headers);
    };
    /**
     * Patch 对已知资源进行局部更新
     * @param url
     * @param responseType
     * @param headers
     * @param complete
     */
    Http.patch = function (url, responseType, headers, complete) {
        var xhr = new Laya.HttpRequest();
        xhr.http.timeout = 10000; //设置超时时间；
        xhr.http.withCredentials = false;
        xhr.once(Laya.Event.COMPLETE, this, complete);
        xhr.send(url, Http.formatData({}), "patch", responseType, headers);
    };
    /**
     * Delete 删除
     * @param url
     * @param responseType
     * @param headers
     * @param complete
     */
    Http.delete = function (url, responseType, headers, complete) {
        var xhr = new Laya.HttpRequest();
        xhr.http.timeout = 10000; //设置超时时间；
        xhr.http.withCredentials = false;
        xhr.once(Laya.Event.COMPLETE, this, complete);
        xhr.send(url, Http.formatData({}), "delete", responseType, headers);
    };
    Http.formatData = function (json) {
        if (Http.DATA_TYPE == 'FORMDATA') {
            var data = new FormData();
            for (var key in json) {
                data.append(key, json[key]);
            }
            return data;
        }
        try {
            return JSON.stringify(json);
        }
        catch (e) {
            return json;
        }
    };
    Http.jsonData = function (json) {
        try {
            return JSON.stringify(json);
        }
        catch (e) {
            return json;
        }
    };
    Http.DATA_TYPE = 'FORMDATA'; //'JSON'
    return Http;
}());
//# sourceMappingURL=Http.js.map