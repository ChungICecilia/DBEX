var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var view;
(function (view) {
    var Event = laya.events.Event;
    var Button = laya.ui.Button;
    var TaskPage = /** @class */ (function (_super) {
        __extends(TaskPage, _super);
        function TaskPage(game) {
            var _this = _super.call(this) || this;
            _this.data = [];
            _this.game = game;
            _this.list_task.array = [];
            _this.btn_close.on(Laya.Event.CLICK, _this, _this.onClickClose);
            // switch (UILayout.stepNum) {//Rest.USERINFO.guidMoveSteps) {//
            //     case 6:
            //     case 7:
            _this.guid = new Laya.Animation();
            _this.guid.loadAnimation("guid.ani", Laya.Handler.create(_this, _this.onLoaded), null);
            _this.guid.visible = false;
            _this.addChild(_this.guid);
            return _this;
            //         break;
            // }
        }
        // 动画加载完成
        TaskPage.prototype.onLoaded = function () {
            this.guid.on(Laya.Event.COMPLETE, this, null);
        };
        TaskPage.prototype.onClickClose = function () {
            if (7 == view.UILayout.stepNum) { //Rest.USERINFO.guidMoveSteps
                view.UILayout.stepNum = 8;
                this.game.ui.onStep(8);
                this.game.ui.guid.visible = true;
                this.guid.rotation = 0;
                this.game.ui.guid.pos(this.game.ui.btn_shareIt.x + (this.game.ui.btn_shareIt.width >> 1) + 10, this.game.ui.btn_shareIt.y - (this.game.ui.btn_shareIt.height - 10));
                this.guid.removeSelf();
            }
            // this.event("redPoint");
            this.game.ui.redPoint();
            this.close();
        };
        TaskPage.prototype.onOpened = function () {
            var _this = this;
            rest.task.tasks(function (data) {
                _this.data = data.data;
                _this.list_task.array = _this.data;
            });
            this.list_task.renderHandler = new Laya.Handler(this, this.onRender);
            this.list_task.mouseHandler = new Laya.Handler(this, this.onMouse);
        };
        TaskPage.prototype.onRender = function (cell, index) {
            if (index >= this.data.length) {
                return;
            }
            var boxTask = this.list_task.getChildByName("box_task");
            var sp_1 = cell.getChildByName("sp_1");
            var img_gift = sp_1.getChildByName("img_gift");
            var img_giftOpen = sp_1.getChildByName("img_giftOpen");
            var text_title = sp_1.getChildByName("text_title");
            var text_rewardQuantity = sp_1.getChildByName("text_rewardQuantity");
            var progressBar = sp_1.getChildByName("progressBar");
            var text_probText = sp_1.getChildByName("text_probText");
            var img_complete = sp_1.getChildByName("img_complete");
            var btn_receive = sp_1.getChildByName("btn_receive");
            var btn_undone = sp_1.getChildByName("btn_undone");
            var sp_2 = cell.getChildByName("sp_2");
            var img_checkInGift = sp_2.getChildByName("img_checkInGift");
            var img_checkInGiftOpen = sp_2.getChildByName("img_checkInGiftOpen");
            var text_checkInReward = sp_2.getChildByName("text_checkInReward");
            var img_checkInComplete = sp_2.getChildByName("img_checkInComplete");
            var btn_checkInReceive = sp_2.getChildByName("btn_checkInReceive");
            var btn_undone2 = sp_2.getChildByName("btn_undone2");
            var text_title2 = sp_2.getChildByName("text_title2");
            var sp_3 = cell.getChildByName("sp_3");
            var text_transactionTitle = sp_3.getChildByName("text_transactionTitle");
            var text_transactionText = sp_3.getChildByName("text_transactionText");
            var text_transactionReward = sp_3.getChildByName("text_transactionReward");
            var text_arithmeticForce = sp_3.getChildByName("text_arithmeticForce");
            var text_BBMQuantity = sp_3.getChildByName("text_BBMQuantity");
            console.log(this.data[index]);
            switch (this.data[index].task.type) {
                case 1:
                    // let num = Rest.USERINFO.baseBbm + Rest.USERINFO.starterBbm;
                    sp_2.visible = false;
                    sp_1.visible = false;
                    sp_3.visible = true;
                    text_arithmeticForce.text = "+" + (+this.data[index].instance.pickedCoins / Rest.OPTION.transform_rate).toFixed(4);
                    text_transactionTitle.text = this.data[index].task.taskDesc;
                    text_BBMQuantity.text = this.data[index].instance.pickedCoins;
                    text_transactionText.text = this.data[index].task.rewardsDesc;
                    this.data[index].instance.pickedCoins != 0 ? text_arithmeticForce.text = '+' + (+this.data[index].instance.pickedCoins / Rest.OPTION.transform_rate).toFixed(4) : text_arithmeticForce.text = '+' + 0;
                    break;
                // case 2:
                //     sp_2.visible = true;
                //     text_checkInReward.text = this.data[index].task.rewardsCalcPoints;
                //     if (this.data[index].instance.rewardStatus == 1) {
                //         img_checkInGift.visible = true;
                //         btn_checkInReceive.visible = true;
                //     } else {
                //         img_checkInGift.visible = false;
                //         btn_checkInReceive.visible = false;
                //         img_checkInGiftOpen.visible = true;
                //         img_checkInComplete.visible = true;
                //     }
                //     switch (UILayout.stepNum) {//Rest.USERINFO.guidMoveSteps
                //         case 6:
                //             var point = sp_2.localToGlobal(new Laya.Point(0, 0));
                //             // console.log("point.x:" + point.x, "point.y:" + point.y);
                //             this.guid.pos(point.x + btn_checkInReceive.x + (btn_checkInReceive.width >> 1), point.y + btn_checkInReceive.y - btn_checkInReceive.height - 10);
                //             // this.guid.pos(this.btn_close.x + (this.btn_close.width >> 1),this.btn_close.y - (this.btn_close.height >> 1) );
                //             this.guid.play(0, true, "ani1");
                //             this.guid.visible = true;
                //             // this.list_task.refresh();
                //             break;
                //         case 7:
                //             // this.guid.pos(this.btn_close.x + (this.btn_close.width >> 1), this.btn_close.y - (this.btn_close.height));
                //             // this.guid.play(0, true, "ani1");
                //             // this.guid.visible = true;
                //             break;
                //     }
                //     break;
                case 2:
                    sp_3.visible = false;
                    sp_1.visible = false;
                    sp_2.visible = true;
                    text_title2.text = this.data[index].task.taskDesc;
                    text_checkInReward.text = this.data[index].task.rewardsCalcPoints;
                    if (this.data[index].instance.finishStatus == 1) {
                        img_checkInGift.visible = true;
                        btn_undone2.visible = true;
                    }
                    else {
                        if (this.data[index].instance.rewardStatus == 1) {
                            img_checkInGift.visible = true;
                            btn_checkInReceive.visible = true;
                        }
                        else {
                            img_checkInGift.visible = false;
                            btn_checkInReceive.visible = false;
                            img_checkInGiftOpen.visible = true;
                            img_checkInComplete.visible = true;
                        }
                    }
                    break;
                case 3:
                    sp_3.visible = false;
                    sp_2.visible = false;
                    sp_1.visible = true;
                    text_title.text = this.data[index].task.taskDesc;
                    text_rewardQuantity.text = "" + this.data[index].task.rewardsCalcPoints;
                    text_probText.text = this.data[index].instance.pickedCoins + "/" + this.data[index].task.requirePickedCoins;
                    progressBar.value = this.data[index].instance.pickedCoins / this.data[index].task.requirePickedCoins;
                    switch (this.data[index].instance.rewardStatus) {
                        case 1:
                            if (this.data[index].instance.finishStatus == 1) {
                                img_gift.visible = true;
                                btn_undone.visible = true;
                            }
                            else {
                                img_gift.visible = true;
                                btn_receive.visible = true;
                                btn_undone.visible = false;
                            }
                            break;
                        case 2:
                            img_gift.visible = false;
                            btn_receive.visible = false;
                            img_giftOpen.visible = true;
                            img_complete.visible = true;
                            break;
                    }
                    break;
                case 4:
                    sp_3.visible = false;
                    sp_1.visible = false;
                    sp_2.visible = true;
                    text_title2.text = this.data[index].task.taskDesc;
                    text_checkInReward.text = this.data[index].task.rewardsCalcPoints;
                    if (this.data[index].instance.finishStatus == 1) {
                        img_checkInGift.visible = true;
                        btn_checkInReceive.visible = true;
                    }
                    else {
                        if (this.data[index].instance.rewardStatus == 1) {
                            img_checkInGift.visible = true;
                            btn_checkInReceive.visible = true;
                        }
                        else {
                            img_checkInGift.visible = false;
                            btn_checkInReceive.visible = false;
                            img_checkInGiftOpen.visible = true;
                            img_checkInComplete.visible = true;
                        }
                    }
                    switch (view.UILayout.stepNum) { //Rest.USERINFO.guidMoveSteps
                        case 6:
                            var point = sp_2.localToGlobal(new Laya.Point(0, 0));
                            // console.log("point.x:" + point.x, "point.y:" + point.y);
                            this.guid.pos(point.x + btn_checkInReceive.x + (btn_checkInReceive.width >> 1), point.y + btn_checkInReceive.y - btn_checkInReceive.height - 10);
                            // this.guid.pos(this.btn_close.x + (this.btn_close.width >> 1),this.btn_close.y - (this.btn_close.height >> 1) );
                            this.guid.play(0, true, "ani1");
                            this.guid.visible = true;
                            // this.list_task.refresh();
                            break;
                        case 7:
                            var point = sp_2.localToGlobal(new Laya.Point(0, 0));
                            this.guid.pos(point.x + this.btn_close.x - (this.btn_close.width >> 1), 170);
                            this.guid.rotation = 270;
                            this.guid.play(0, true, "ani1");
                            this.guid.visible = true;
                            break;
                    }
                    break;
            }
        };
        TaskPage.prototype.onMouse = function (e, index) {
            var _this = this;
            if (e.type == Event.CLICK) {
                var cell = e.target;
                if (cell instanceof Button) {
                    if (this.data[index].task.type == 4) {
                        if (6 == view.UILayout.stepNum) { //Rest.USERINFO.guidMoveSteps
                            view.UILayout.stepNum = 7;
                            this.guid.visible = true;
                            this.game.ui.onStep(7);
                            var point = this.btn_close.localToGlobal(new Laya.Point(0, 0));
                            this.guid.pos(point.x + (this.btn_close.width >> 1), point.y - (this.btn_close.height));
                            // this.guid.pos(point.x,point.y);
                            this.list_task.refresh();
                        }
                    }
                    var taskId = this.data[index].instance.id;
                    var inviteCode;
                    rest.task.askRewards(taskId, function (data) {
                        rest.user.userInfo(inviteCode, (function (data) {
                            console.log(data);
                            Rest.USERINFO = data.data.player;
                            if (data.data) {
                                _this.game.event('UPDATEUSER');
                                _this.data[index].instance.rewardStatus = 2;
                                _this.list_task.array = _this.data;
                            }
                        }));
                    });
                }
            }
        };
        return TaskPage;
    }(ui.TaskPageUI));
    view.TaskPage = TaskPage;
})(view || (view = {}));
//# sourceMappingURL=TaskPage.js.map