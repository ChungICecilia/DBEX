var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var view;
(function (view) {
    var Event = laya.events.Event;
    var Box = laya.ui.Box;
    var AllGalaxyPage = /** @class */ (function (_super) {
        __extends(AllGalaxyPage, _super);
        function AllGalaxyPage(chooseHead) {
            var _this = _super.call(this) || this;
            _this.data = [];
            _this.chooseHead = chooseHead;
            _this.list_galaxy.array = [];
            _this.enterGameBtn.on(Laya.Event.CLICK, _this, _this.onEenterGame);
            _this.text_back.on(Laya.Event.CLICK, _this, _this.onGoBack);
            _this.getGroomGalaxy();
            return _this;
        }
        AllGalaxyPage.prototype.getGroomGalaxy = function () {
            var _this = this;
            rest.galaxy.allGalaxy(function (data) {
                if (data.status == "error") {
                    _this.showErrorTips(data.msg);
                }
                else {
                    _this.data = data.data;
                    _this.onOpened();
                }
            });
        };
        AllGalaxyPage.prototype.onOpened = function () {
            console.log(this.data);
            this.list_galaxy.array = this.data;
            // this.list_galaxy.height = this.data.length * 120;
            this.list_galaxy.renderHandler = new Laya.Handler(this, this.onRender);
            this.list_galaxy.mouseHandler = new Laya.Handler(this, this.onmouse);
            // if (this.list_galaxy.cells[0]) {
            //     var img_pick: Laya.Image = this.list_galaxy.cells[0].getChildByName("img_pick") as Laya.Image;
            //     img_pick.visible = true;
            //     this.galaxyId = this.data[0].id;
            // }
        };
        AllGalaxyPage.prototype.onRender = function (cell, index) {
            if (index >= this.data.length) {
                return;
            }
            var img_icon = cell.getChildByName("img_icon");
            var text_galaxy = cell.getChildByName("text_galaxy");
            //var text_region: Laya.Text = cell.getChildByName("text_region") as Laya.Text;
            var text_computingPower = cell.getChildByName("text_computingPower");
            var text_playerNumer = cell.getChildByName("text_playerNumer");
            var img_pick = cell.getChildByName("img_pick");
            img_pick.visible = false;
            switch (this.data[index].rankLevel) {
                case 1:
                    img_icon.skin = "changeGalaxy/xingqiu_high.png";
                    text_computingPower.text = "高";
                    text_computingPower.color = "#f48731";
                    break;
                case 2:
                    img_icon.skin = "changeGalaxy/xingqiu_mid.png";
                    text_computingPower.text = "中";
                    text_computingPower.color = "#6958fb";
                    break;
                case 3:
                    img_icon.skin = "changeGalaxy/xingqiu_low.png";
                    text_computingPower.text = "低";
                    text_computingPower.color = "#18bd36";
                    break;
            }
            text_galaxy.text = this.data[index].galaxyName;
            //text_region.text = this.data[index].region;
            text_playerNumer.text = this.data[index].galaxyPlayers + '';
            if (index == 0) {
                img_pick.visible = true;
                this.galaxyId = this.data[0].id;
            }
        };
        AllGalaxyPage.prototype.onmouse = function (e, index) {
            console.log(index);
            if (e.type == Event.CLICK) {
                if ((e.target) instanceof Box) {
                    for (var i = 0; i < this.list_galaxy.cells.length; i++) {
                        var img = this.list_galaxy.cells[i].getChildByName("img_pick");
                        img.visible = false;
                    }
                    var img_pick = (e.target).getChildByName("img_pick");
                    img_pick.visible = true;
                    this.galaxyId = this.data[index].id;
                }
            }
        };
        AllGalaxyPage.prototype.onGoBack = function () {
            if (this.parent != null) {
                this.removeSelf();
            }
        };
        AllGalaxyPage.prototype.onEenterGame = function () {
            var _this = this;
            this.ani_beginGameLoading.visible = true;
            this.ani_beginGameLoading.play(0, true, "ani1");
            this.enterGameBtn.disabled = true;
            this.text_back.mouseEnabled = false;
            rest.galaxy.setGalaxy({
                galaxyld: this.galaxyId,
            }, function (data) {
                //console.log(data)
                if (data.status == "error") {
                    _this.showErrorTips(data.msg);
                    _this.ani_beginGameLoading.clear();
                }
                else {
                    Rest.USERINFO = data.data;
                    //console.log(Rest.USERINFO);
                    if (_this.chooseHead) {
                        _this.chooseHead.event("CreatedRole");
                    }
                    _this.event("CLOSE");
                    _this.parent.removeChild(_this);
                }
            });
        };
        AllGalaxyPage.prototype.showErrorTips = function (msg) {
            this.tips = 3;
            this.img_createPlayer.visible = true;
            this.img_createPlayer.width = msg.length * 30;
            this.text_createPlayer.width = msg.length * 30;
            this.img_createPlayer.centerX = 1;
            this.text_createPlayer.text = msg;
            Laya.timer.loop(1000, this, this.tipsLoop);
        };
        AllGalaxyPage.prototype.tipsLoop = function () {
            this.tips--;
            if (this.tips <= 0) {
                if (this.enterGameBtn.disabled) {
                    this.enterGameBtn.disabled = false;
                    this.text_back.mouseEnabled = true;
                }
                this.img_createPlayer.visible = false;
                Laya.timer.clear(this, this.tipsLoop);
            }
        };
        return AllGalaxyPage;
    }(ui.AllGalaxyPageUI));
    view.AllGalaxyPage = AllGalaxyPage;
})(view || (view = {}));
//# sourceMappingURL=AllGalaxyPage.js.map