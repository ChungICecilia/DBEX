var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
* name
*/
var view;
(function (view) {
    var ChongPage = /** @class */ (function (_super) {
        __extends(ChongPage, _super);
        function ChongPage(game) {
            var _this = _super.call(this) || this;
            _this.activityId = 0;
            _this.topicId = 0;
            _this.optionPrompt = new OptionPrompt();
            _this.game = game;
            _this.btn_start.on(Laya.Event.CLICK, _this, _this.onClickStart);
            _this.btn_next.on(Laya.Event.CLICK, _this, _this.onClickNext);
            _this.btn_reward.on(Laya.Event.CLICK, _this, _this.onClickReward);
            _this.btn_live.on(Laya.Event.CLICK, _this, _this.onClickLive);
            _this.numSp = new NumSprite();
            _this.numSp.on("LoadOk", _this, _this.onLoaded);
            _this.mainPg.addChild(_this.numSp);
            _this.list_answers.renderHandler = new Laya.Handler(_this, _this.onListRender);
            _this.txt_wenti.style.fontSize = 24;
            _this.txt_wenti.style.wordWrap = true;
            _this.numSp.y = 243;
            _this.mainPg.addChild(_this.numSp);
            _this.list_answers.renderHandler = new Laya.Handler(_this, _this.onListRender);
            return _this;
        }
        ChongPage.prototype.initialize = function () {
            _super.prototype.initialize.call(this);
            this.btn_close.on(Laya.Event.CLICK, this, this.onClickClose);
            this.list_answers.selectEnable = true;
            this.list_answers.selectHandler = new Laya.Handler(this, this.listSelectHandler);
            var str = this.txt_describe.text;
            str = str.replace("@", Rest.OPTION.revive_use_card);
            this.txt_describe.text = str;
        };
        ChongPage.prototype.onLoaded = function () {
            var total = this.numSp.totalWidth + this.pic_dbex.width;
            var tx = (this.mainPg.width - total) / 2;
            this.numSp.x = tx;
            this.pic_dbex.x = this.numSp.x + this.numSp.totalWidth - 20;
            this.numSp.y = this.pic_dbex.y;
            this.numSp.x += 10;
            this.pic_dbex.x += 10;
        };
        ChongPage.prototype.onClickClose = function () {
            this.close();
        };
        ChongPage.prototype.onOpened = function () {
            var _this = this;
            this.ansInx = 0;
            view.LoadingPage.loading();
            rest.chong.info(function (data) {
                view.LoadingPage.dispose();
                if (data.code == 1) {
                    _this.topicInx = 0;
                    _this.setInfo(data.data, data.code);
                    _this.viewStack.selectedIndex = 0;
                }
                else {
                    if (data.code == -101) {
                        _this.setInfo(data.data, data.code);
                    }
                    else {
                        _this.optionPrompt.play(data.msg, [Laya.stage.width / 2, Laya.stage.height / 2]);
                    }
                }
            });
        };
        ChongPage.prototype.setInfo = function (datas, state) {
            this.activityId = datas.activityId;
            this.totalNum = datas.questionNumber;
            this.txt_total.text = "\u4E00\u5171" + datas.questionNumber + "\u9898";
            this.txt_des.text = "\u7B54\u5BF9" + datas.questionNumber + "\u9053\u9898\u7684\u6240\u6709\u73A9\u5BB6\u5171\u540C\u5206\u4EAB";
            this.answerTime = datas.answerTime;
            this.rewardQuantity = datas.rewardQuantity;
            this.numSp.setNumber(datas.rewardQuantity, "res/atlas/dnum.atlas", -20);
            var date = new Date();
            console.log(datas);
            var str = datas.previewTime.replace(/-/g, '/');
            this.prevTime = (new Date(str).getTime());
            str = datas.startTime.replace(/-/g, '/');
            this.startTime = (new Date(str)).getTime();
            str = datas.endTime.replace(/-/g, '/');
            this.endTime = (new Date(str)).getTime();
            this.curTime = (new Date()).getTime();
            this.pic_leftTime.visible = false;
            this.backRoll = datas.backRoll;
            if (this.curTime < this.startTime) {
                //预览时间
                if (this.curTime > this.prevTime) {
                    this.tInx = this.startTime - this.curTime;
                    this.txt_time.text = DateUtil.second2String(this.tInx / 1000);
                    Laya.timer.loop(1000, this, this.time);
                }
                else {
                    this.txt_time.text = "未开始";
                }
            }
            else if (this.curTime >= this.startTime && this.curTime < this.endTime) {
                if (state == -101) {
                    if (datas.state == 2) {
                        this.goCorrectPg();
                    }
                    else if (datas.state = 3) {
                        this.goErrorPg();
                    }
                }
                else if (state) {
                    this.txt_time.text = "开始答题";
                }
                this.pic_leftTime.visible = true;
                this.curTime = new Date().getTime();
                this.aInx = this.endTime - this.curTime;
                this.txt_leftTime.text = "活动剩余时间:" + DateUtil.second2String(this.aInx / 1000);
                Laya.timer.loop(1000, this, this.atime);
            }
            else {
                this.txt_time.text = "不在答题时间";
            }
            //console.log(oldTime);
        };
        ChongPage.prototype.time = function () {
            this.tInx -= 1000;
            this.txt_time.text = DateUtil.second2String(this.tInx / 1000);
            if (this.tInx < 0) {
                Laya.timer.clear(this, this.time);
            }
        };
        ChongPage.prototype.onAnswerTime = function () {
            this.sInx -= 1000;
            this.txt_ansTime.text = String(this.sInx / 1000);
            if (this.sInx <= 0) {
                this.onClickNext();
            }
        };
        ChongPage.prototype.onClosed = function () {
            Laya.timer.clear(this, this.time);
            Laya.timer.clear(this, this.atime);
            Laya.timer.clear(this, this.onAnswerTime);
        };
        ChongPage.prototype.onClickStart = function () {
            this.curTime = (new Date()).getTime();
            if (this.curTime < this.endTime && this.curTime > this.startTime) {
                this.answerPage();
            }
        };
        ChongPage.prototype.onClickNext = function () {
            var _this = this;
            Http.DATA_TYPE = 'JSON';
            var submitObj = new Object();
            submitObj["topicId"] = this.topicId;
            submitObj["headerId"] = this.activityId;
            submitObj["answerIdList"] = this.getAnswer();
            Laya.timer.clear(this, this.onAnswerTime);
            view.LoadingPage.loading();
            rest.chong.answer(submitObj, function (data) {
                view.LoadingPage.dispose();
                Http.DATA_TYPE = 'FORMDATA';
                if (data.status == "error") {
                    // this.ani_loginLoading.clear();
                    // this.showErrorTips(data.msg);
                    _this.optionPrompt.play(data.msg, [Laya.stage.width / 2, Laya.stage.height / 2]);
                }
                else {
                    //this.event('LoginOK');
                    // this.ani_loginLoading.clear();
                    var datas = data.data;
                    _this.backRoll = datas.backRoll;
                    var errNum = datas.errNumber;
                    var sucNum = datas.successNumber;
                    var reward = datas.rewardIsForce;
                    if (data.code == 1) {
                        _this.curTime = (new Date()).getTime();
                        if (_this.curTime < _this.endTime && _this.curTime > _this.startTime) {
                            _this.game.event('Sounds', 6);
                            _this.answerPage();
                        }
                    }
                    else if (data.code == 2001) {
                        _this.goCorrectPg();
                    }
                    else {
                        _this.goErrorPg();
                    }
                    //this.resultPage(datas);
                }
            });
        };
        ChongPage.prototype.onClickReward = function () {
            var _this = this;
            rest.chong.reward(this.activityId, function (data) {
                if (data.status == "error") {
                    // this.ani_loginLoading.clear();
                    // this.showErrorTips(data.msg);
                    _this.optionPrompt.play(data.msg, [Laya.stage.width / 2, Laya.stage.height / 2]);
                }
                else {
                    //this.setInfo(data.data);
                    if (data.code) {
                        //this.getTopic(data.data.topicId);
                        _this.optionPrompt.play("奖励领取成功", [Laya.stage.width / 2, Laya.stage.height / 2]);
                        _this.close();
                        var inviteCode = void 0;
                        rest.user.userInfo(inviteCode, function (data) {
                            Rest.USERINFO = data.data.player;
                            Rest.GALAXY = data.data.byDbexGalaxy;
                            _this.game.onCreatedRole();
                        });
                    }
                    else {
                        _this.optionPrompt.play(data.msg, [Laya.stage.width / 2, Laya.stage.height / 2]);
                    }
                }
            });
        };
        ChongPage.prototype.onClickLive = function () {
            var _this = this;
            view.LoadingPage.loading();
            rest.chong.live(this.activityId, function (data) {
                view.LoadingPage.dispose();
                if (data.status == "error") {
                    // this.ani_loginLoading.clear();
                    // this.showErrorTips(data.msg);
                    _this.optionPrompt.play(data.msg, [Laya.stage.width / 2, Laya.stage.height / 2]);
                }
                else {
                    //this.setInfo(data.data);
                    if (data.code) {
                        _this.topicInx = 0;
                        _this.answerPage();
                    }
                }
            });
        };
        ChongPage.prototype.answerPage = function () {
            this.viewStack.selectedIndex = 1;
            this.getTopicId();
        };
        ChongPage.prototype.getTopicId = function () {
            var _this = this;
            view.LoadingPage.loading();
            this.ansInx++;
            rest.chong.getTopicId(function (data) {
                if (data.status == "error") {
                    // this.ani_loginLoading.clear();
                    // this.showErrorTips(data.msg);
                }
                else {
                    //this.setInfo(data.data);
                    _this.getTopic(data.data.topicId);
                }
            });
        };
        ChongPage.prototype.getTopic = function (topicId) {
            var _this = this;
            Laya.timer.clear(this, this.onAnswerTime);
            this.topicId = topicId;
            rest.chong.getTopic(topicId, function (data) {
                view.LoadingPage.dispose();
                if (data.status == "error") {
                    // this.ani_loginLoading.clear();
                    // this.showErrorTips(data.msg);
                    _this.optionPrompt.play(data.msg, [Laya.stage.width / 2, Laya.stage.height / 2]);
                }
                else {
                    _this.topicInx++;
                    var datas = data.data;
                    _this.txt_wenti.innerHTML = Font.getSizeColor("#5d3500", 24, (_this.topicInx) + ".") + Font.getSizeColor("#5d3500", 24, datas.title);
                    _this.list_answers.array = datas.answerList;
                    _this.answerList = datas.answerList;
                    _this.sInx = _this.answerTime * 1000;
                    _this.txt_ansTime.text = String(_this.sInx / 1000);
                    Laya.timer.loop(1000, _this, _this.onAnswerTime);
                    _this.clearList();
                }
            });
        };
        ChongPage.prototype.atime = function () {
            this.aInx -= 1000;
            if (this.aInx < 0) {
                Laya.timer.clear(this, this.atime);
                this.txt_leftTime.text = "活动剩余时间:" + DateUtil.second2String(0);
                return;
            }
            this.txt_leftTime.text = "活动剩余时间:" + DateUtil.second2String(this.aInx / 1000);
        };
        ChongPage.prototype.onListRender = function (cell, index) {
            var text = cell.getChildByName("txt_answer");
            text.text = this.list_answers.array[index].answer;
            text.y = (55 - text.textHeight) / 2;
        };
        ChongPage.prototype.listSelectHandler = function (index) {
            this.chooseList(index);
        };
        ChongPage.prototype.clearList = function () {
            var len = this.list_answers.cells.length;
            for (var i = 0; i < len; i++) {
                var checkBox = this.list_answers.getCell(i).getChildByName("check");
                checkBox.selected = false;
            }
        };
        ChongPage.prototype.chooseList = function (inx) {
            var checkBox = this.list_answers.getCell(inx).getChildByName("check");
            var len = this.list_answers.cells.length;
            checkBox.selected = true;
            for (var i = 0; i < len; i++) {
                if (inx != i) {
                    checkBox = this.list_answers.getCell(i).getChildByName("check");
                    checkBox.selected = false;
                }
            }
        };
        ChongPage.prototype.getAnswer = function () {
            var ids = [];
            var sprites = this.list_answers.cells;
            var spri;
            for (var i = 0; i < sprites.length; i++) {
                spri = sprites[i];
                var cheBox = spri.getChildByName("check");
                if (cheBox.selected) {
                    var ansId = this.list_answers.getItem(i).answerId;
                    ids.push(ansId);
                }
            }
            return ids;
        };
        ChongPage.prototype.goCorrectPg = function () {
            this.viewStack.selectedIndex = 2;
            this.txt_corNum.text = "\u60A8\u7B54\u5BF9\u4E86" + this.totalNum + "\u9898";
            this.game.event('Sounds', 7);
        };
        ChongPage.prototype.goErrorPg = function () {
            this.viewStack.selectedIndex = 3;
            this.txt_desLive.text = this.rewardQuantity + "\u4E2ADBEXD\u7B49\u7740\u60A8\u5462\uFF0C\u590D\u6D3B\u91CD\u6765\uFF01";
            console.log(this.backRoll);
            this.txt_liveNum.text = "\u590D\u6D3B\u5377:" + this.backRoll;
            this.txt_liveNeed.text = "(复活x" + Rest.OPTION.revive_use_card + "张)";
            this.game.event('Sounds', 8);
        };
        return ChongPage;
    }(ui.ChongPageUI));
    view.ChongPage = ChongPage;
})(view || (view = {}));
//# sourceMappingURL=ChongPage.js.map