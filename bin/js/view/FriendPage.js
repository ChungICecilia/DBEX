var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**Created by the LayaAirIDE*/
var view;
(function (view) {
    var Button = laya.ui.Button;
    var Event = laya.events.Event;
    var FriendPage = /** @class */ (function (_super) {
        __extends(FriendPage, _super);
        function FriendPage(game) {
            var _this = _super.call(this) || this;
            _this.data = [];
            _this.shareLink = '';
            _this.game = game;
            var strings = Browser.window.location.href.split("?");
            _this.shareLink = strings[0] + '?userCode=' + Rest.INVITECODE;
            _this.inviteHref = Laya.Browser.window.document.getElementById("bar");
            _this.copyDiv = Laya.Browser.window.document.getElementById("addFriend_btn");
            _this.copy = Laya.Browser.window.document.getElementById("copy1_div");
            _this.btn_close.on(Laya.Event.CLICK, _this, _this.onClickClose);
            _this.list_friends.array = [];
            _this.tips = 0;
            _this.commonlyUsed = new CommonlyUsed;
            return _this;
        }
        FriendPage.prototype.onClickClose = function () {
            this.hiddenCopyBtn();
            this.close();
        };
        FriendPage.prototype.onOpened = function () {
            var _this = this;
            this.showCopyBtn();
            this.text_wateringFriendsTimes.text = Rest.USERINFO.wateringFriendsTimes + '/50';
            rest.friend.list(0, 10, function (data) {
                if (data.data.pageResult) {
                    _this.data = data.data.pageResult;
                    _this.list_friends.array = _this.data;
                }
                //console.log(data);
            });
            this.list_friends.renderHandler = new Laya.Handler(this, this.onRender);
            this.list_friends.mouseHandler = new Laya.Handler(this, this.onMouse);
        };
        FriendPage.prototype.showCopyBtn = function () {
            if (this.inviteHref && this.copyDiv) {
                this.copyDiv.style.visibility = "visible";
                this.inviteHref.style.visibility = "visible";
                this.inviteHref.innerHTML = this.shareLink;
                console.log(this.inviteHref.text);
            }
        };
        FriendPage.prototype.hiddenCopyBtn = function () {
            if (this.inviteHref && this.copyDiv) {
                this.copyDiv.style.visibility = "hidden";
                this.inviteHref.style.visibility = "hidden";
            }
        };
        FriendPage.prototype.onRender = function (cell, index) {
            if (index >= this.data.length) {
                return;
            }
            var lab_friendName = cell.getChildByName("lab_friendName");
            var lab_fame = cell.getChildByName("lab_fame");
            var img_avatar = cell.getChildByName("img_avatar");
            var btn_water = cell.getChildByName("btn_water");
            var btn_waterOK = cell.getChildByName("btn_waterOK");
            var img_water = btn_water.getChildByName("img_water");
            var text_addQuantity = cell.getChildByName("text_addQuantity");
            var lab_bbm = cell.getChildByName("lab_bbm");
            var lab_bbmPointTotal = cell.getChildByName("lab_bbmPointTotal");
            lab_friendName.text = this.data[index].nickName;
            lab_bbm.text = this.data[index].bbmTotal + '';
            lab_bbmPointTotal.text = this.data[index].calcPointTotal + '';
            if (this.data[index].wateringCount == 0) {
                btn_water.visible = true;
                btn_waterOK.visible = false;
                text_addQuantity.visible = false;
            }
            else {
                btn_water.visible = false;
                btn_waterOK.visible = true;
                text_addQuantity.visible = true;
            }
            this.commonlyUsed.avatar(this.data[index].headIcon, img_avatar);
        };
        FriendPage.prototype.onMouse = function (e, index) {
            var _this = this;
            if (e.type == Event.CLICK) {
                var cell = e.target;
                if (cell instanceof Button) {
                    var friendId = this.data[index].id;
                    rest.work.wateringFriendsTree({ friendId: friendId }, (function (data) {
                        if (data.status == "error") {
                            _this.showErrorTips(data.msg);
                        }
                        else {
                            Rest.USERINFO = data.data;
                            _this.text_wateringFriendsTimes.text = Rest.USERINFO.wateringFriendsTimes + '/50';
                            _this.data[index].wateringCount = 1;
                            _this.list_friends.array = _this.data;
                            _this.game.event('UPDATEUSER');
                        }
                    }));
                }
            }
        };
        FriendPage.prototype.showErrorTips = function (msg) {
            this.tips = 3;
            this.img_friendTips.visible = true;
            this.img_friendTips.width = msg.length * 30;
            this.text_friendTips.width = msg.length * 30;
            this.img_friendTips.centerX = 1;
            this.text_friendTips.text = msg;
            Laya.timer.loop(1000, this, this.tipsLoop);
        };
        FriendPage.prototype.tipsLoop = function () {
            this.tips--;
            if (this.tips <= 0) {
                this.img_friendTips.visible = false;
                Laya.timer.clear(this, this.tipsLoop);
            }
        };
        return FriendPage;
    }(ui.FriendPageUI));
    view.FriendPage = FriendPage;
})(view || (view = {}));
//# sourceMappingURL=FriendPage.js.map