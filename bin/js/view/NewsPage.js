var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**Created by the LayaAirIDE*/
var view;
(function (view) {
    var Box = laya.ui.Box;
    var Event = laya.events.Event;
    var NewsPage = /** @class */ (function (_super) {
        __extends(NewsPage, _super);
        function NewsPage(game) {
            var _this = _super.call(this) || this;
            _this.data = [];
            _this.game = game;
            _this.list_news.array = [];
            _this.btn_close.on(Laya.Event.CLICK, _this, _this.onClickClose);
            //this.game.on("READNID", this, this.isRead)
            console.log(Browser.window.location.href);
            return _this;
        }
        NewsPage.prototype.onClickClose = function () {
            this.event("redPoint");
            this.close();
        };
        NewsPage.prototype.onOpened = function () {
            var _this = this;
            this.text_total.style.align = "center";
            // this.text_total._getCSSStyle().valign = "middle";
            var html = "<span style='fontSize:22;vertical-align:middle;color:#666666;'>" + "已增加算力:" + "</span>";
            html += "<span style='fontSize:24;color:#6958fb'  >" + Rest.USERINFO.newsCalcPoint + "</span>";
            this.text_total.innerHTML = "" + html;
            // "<font  style='fontSize:22' color='#666666' >" + "已增加算力:" + "<font  style='fontSize:24' color='#6958fb' valign='bottom' >"
            //  + Rest.USERINFO.newsCalcPoint + "</font></font>";
            rest.news.list(0, 1, function (data) {
                _this.data = data.data;
                _this.list_news.array = _this.data;
                console.log(data.data);
            });
            this.list_news.renderHandler = new Laya.Handler(this, this.onRender);
            this.list_news.mouseHandler = new Laya.Handler(this, this.onMouse);
        };
        NewsPage.prototype.onRender = function (cell, index) {
            if (index >= this.data.length) {
                return;
            }
            //根据子节点的名字title，获取子节点对象。 
            var lab_title = cell.getChildByName("lab_title");
            var lab_time = cell.getChildByName("lab_time");
            var img_read = cell.getChildByName("img_read");
            var img_fhaveRead = cell.getChildByName("img_fhaveRead");
            //label渲染列表文本（序号）
            // lab_title.text = this.data[index].title;
            // img_read.visible = false;
            // img_fhaveRead.visible = false;
            lab_title.text = this.data[index].newsTitle;
            if (this.data[index].playerReadStatus == 1) {
                img_read.visible = true;
                img_fhaveRead.visible = false;
                lab_title.color = "#333333";
            }
            else {
                lab_title.color = "#aeb4bb";
                img_fhaveRead.visible = true;
                img_read.visible = false;
            }
            //时间戳转换
            var date = new Date();
            date.setTime(this.data[index].newsCreateTimestamp * 1000);
            lab_time.text = date.toLocaleDateString();
            lab_title.wordWrap = true;
        };
        NewsPage.prototype.onMouse = function (e, index) {
            var data = new Date();
            var clickOnNewsTime = data.getTime();
            localStorage.setItem('clickOnNewsTime', clickOnNewsTime + '');
            var timer = localStorage.getItem("clickOnNewsTime");
            console.log(timer);
            //鼠标单击事件触发
            if (e.type == Event.CLICK) {
                //console.log("1111")
                //判断点击事件类型,如果点中的是Box组件执行
                if ((e.target) instanceof Box) {
                    // if (this.news == null) {
                    // 	this.news = new view.News(this.game);
                    // }
                    // this.news.setNews(this.data[index])
                    // this.news.popup(false, true)
                    //http://39.108.117.15/news.html?newsId=7
                    var strings = Browser.window.location.href.split("?");
                    // this.shareLink = strings[0] + '?userCode=' + Rest.INVITECODE;
                    Browser.window.location.href = strings[0] + "news.html?newsId=" + this.data[index].newsId;
                    //Browser.window.open(strings[0] + "news.html?newsId=" + this.data[index].id, '_blank');
                    var cell = this.list_news.getCell(index);
                    var lab_title = cell.getChildByName("lab_title");
                    lab_title.color = "#aaaaaa";
                    this.list_news.refresh();
                }
            }
        };
        return NewsPage;
    }(ui.NewsPageUI));
    view.NewsPage = NewsPage;
})(view || (view = {}));
//# sourceMappingURL=NewsPage.js.map