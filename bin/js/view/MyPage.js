var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**Created by the LayaAirIDE*/
var view;
(function (view) {
    var SoundManager = Laya.SoundManager;
    var Handler = Laya.Handler;
    var MyPage = /** @class */ (function (_super) {
        __extends(MyPage, _super);
        function MyPage(game) {
            var _this = _super.call(this) || this;
            _this.game = game;
            _this.commonlyUsed = new CommonlyUsed;
            _this.btn_close.on(Laya.Event.CLICK, _this, _this.onClickClose);
            return _this;
        }
        MyPage.prototype.onClickClose = function () {
            this.close();
        };
        MyPage.prototype.onOpened = function () {
            switch (Rest.USERINFO.bgmStatus) {
                case 1:
                    this.ckb_music.selected = false;
                    break;
                case 2:
                    this.ckb_music.selected = true;
                    break;
            }
            switch (Rest.USERINFO.btnMusicStatus) {
                case 1:
                    this.ckb_sound.selected = false;
                    break;
                case 2:
                    this.ckb_sound.selected = true;
                    break;
            }
            this.ckb_music.on(Laya.Event.CLICK, this, this.onMusic);
            this.ckb_sound.on(Laya.Event.CLICK, this, this.onSound);
            this.text_nickname.text = Rest.USERINFO.nickName || '';
            //描边
            //this.text_nickname.stroke = 2
            //this.text_nickname.strokeColor = "#D35E36";
            this.text_bbm.text = (Rest.USERINFO.baseBbm + Rest.USERINFO.starterBbm).toFixed(4) || '0';
            this.text_quantity.text = Rest.USERINFO.bbmCalcPoint + Rest.USERINFO.marketCalcPoint + Rest.USERINFO.newsCalcPoint || '0';
            this.commonlyUsed.avatar(Rest.USERINFO.headIcon, this.img_avatar);
        };
        //背景音乐
        MyPage.prototype.onMusic = function () {
            var _this = this;
            if (this.ckb_music.selected) {
                //SoundManager.stopMusic();
                rest.user.setMusicStatus({ type: 1, status: 2, }, function (data) {
                    Rest.USERINFO.bgmStatus = data.data.bgmStatus;
                    console.log(data);
                });
                this.game.event("GameMusic", false);
            }
            else {
                rest.user.setMusicStatus({ type: 1, status: 1, }, function (data) {
                    Rest.USERINFO.bgmStatus = data.data.bgmStatus;
                    SoundManager.playMusic("res/sounds/dbexBGM.mp3", 0, new Handler(_this, _this.onComplete));
                });
                this.game.event("GameMusic", true);
            }
        };
        //音效
        MyPage.prototype.onSound = function () {
            if (this.ckb_sound.selected) {
                rest.user.setMusicStatus({ type: 2, status: 2, }, function (data) {
                    Rest.USERINFO = data.data.btnMusicStatus;
                });
                this.game.event("GameSound", false);
            }
            else {
                rest.user.setMusicStatus({ type: 2, status: 1, }, function (data) {
                    Rest.USERINFO = data.data.btnMusicStatus;
                });
                this.game.event("GameSound", true);
            }
        };
        MyPage.prototype.onComplete = function () {
            console.log("播放完成");
        };
        return MyPage;
    }(ui.MyPageUI));
    view.MyPage = MyPage;
})(view || (view = {}));
//# sourceMappingURL=MyPage.js.map