var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var view;
(function (view) {
    var RealName = /** @class */ (function (_super) {
        __extends(RealName, _super);
        function RealName() {
            var _this = _super.call(this) || this;
            _this.tips = 0;
            _this.btn_close.on(Laya.Event.CLICK, _this, _this.onClickClose);
            _this.confirmBtn.on(Laya.Event.CLICK, _this, _this.onClickConfirm);
            return _this;
        }
        RealName.prototype.onClickClose = function () {
            this.removeSelf();
        };
        RealName.prototype.onClickConfirm = function () {
            var _this = this;
            if (this.nameText.text == "" || this.cardText.text == "") {
                return;
            }
            rest.user.realName({
                name: this.nameText.text,
                card: this.cardText.text,
            }, function (data) {
                console.log(data);
                if (data.status == "error") {
                    _this.showErrorTips(data.msg);
                }
                else {
                    Rest.USERINFO = data.data;
                    //console.log(Rest.USERINFO);
                    _this.onClickClose();
                    _this.event("HIDE");
                }
            });
        };
        RealName.prototype.showErrorTips = function (msg) {
            this.tips = 3;
            this.tips = 3;
            this.img_singn.visible = true;
            this.text_tips.width = msg.length * 30;
            this.img_singn.width = msg.length * 30;
            this.img_singn.centerX = 1;
            this.text_tips.text = msg;
            Laya.timer.loop(1000, this, this.tipsLoop);
        };
        RealName.prototype.tipsLoop = function () {
            this.tips--;
            if (this.tips <= 0) {
                this.img_singn.visible = false;
                Laya.timer.clear(this, this.tipsLoop);
            }
        };
        return RealName;
    }(ui.RealNameUI));
    view.RealName = RealName;
})(view || (view = {}));
//# sourceMappingURL=RealName.js.map