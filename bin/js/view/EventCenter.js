/**
* name
*/
var view;
(function (view) {
    var EventCenter = /** @class */ (function () {
        function EventCenter() {
        }
        EventCenter.add = function (type, caller, listener, args) {
            this.dispatcher.on(type, caller, listener, args);
        };
        /**
         * 快速派发一个GameEvent事件
         * @param	type 事件名
         * @param	arg 可附带的数据对象
         * @return 是否成功派发
         */
        EventCenter.dispatchWith = function (type, args) {
            return this.dispatcher.event(type, args);
        };
        /**
         * 普通事件,和flash本身一样
         * @param	任何基于Event的对象
         * @return
         */
        EventCenter.dispatchEvent = function (type, args) {
            return this.dispatcher.event(type, args);
        };
        EventCenter.remove = function (type, caller, listener, onceOnly) {
            if (onceOnly === void 0) { onceOnly = false; }
            this.dispatcher.off(type, caller, listener, onceOnly);
        };
        EventCenter.dispatcher = new Laya.EventDispatcher;
        return EventCenter;
    }());
    view.EventCenter = EventCenter;
})(view || (view = {}));
//# sourceMappingURL=EventCenter.js.map