var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var view;
(function (view) {
    var WalletPage = /** @class */ (function (_super) {
        __extends(WalletPage, _super);
        function WalletPage() {
            var _this = _super.call(this) || this;
            _this.btn_close.on(Laya.Event.CLICK, _this, _this.onClickClose);
            _this.btn_wellet.on(Laya.Event.CLICK, _this, _this.onClickWellet);
            return _this;
        }
        WalletPage.prototype.onClickClose = function () {
            this.close();
        };
        WalletPage.prototype.onOpened = function () {
            this.text_bbm.text = "拥有" + (Rest.USERINFO.baseBbm + Rest.USERINFO.starterBbm).toFixed(4) + "BBM" || "拥有0BBM";
        };
        WalletPage.prototype.onClickWellet = function () {
            Browser.window.location.href = "http://www.bbb-home.com/appDownload.html";
        };
        return WalletPage;
    }(ui.WalletPageUI));
    view.WalletPage = WalletPage;
})(view || (view = {}));
//# sourceMappingURL=WelletPage.js.map