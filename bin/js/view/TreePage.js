var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**Created by the LayaAirIDE*/
var view;
(function (view) {
    var TreePage = /** @class */ (function (_super) {
        __extends(TreePage, _super);
        function TreePage(game) {
            var _this = _super.call(this) || this;
            _this.rcord = [];
            _this.game = game;
            _this.list_tree.array = [];
            _this.btn_close.on(Laya.Event.CLICK, _this, _this.onClickClose);
            _this.onRenderNEWS();
            return _this;
        }
        TreePage.prototype.onClickClose = function () {
            this.close();
        };
        TreePage.prototype.onRenderNEWS = function () {
            var _this = this;
            rest.friend.wateringRecord(function (data) {
                console.log(data);
                _this.rcord = data.data;
                _this.list_tree.array = _this.rcord;
            });
            this.list_tree.renderHandler = new Laya.Handler(this, this.onRender);
        };
        TreePage.prototype.onRender = function (cell, index) {
            if (index >= this.rcord.length) {
                return;
            }
            var html_rcord = cell.getChildByName("html_rcord");
            html_rcord.style.lineHeight = 24;
            html_rcord.style.align = "center";
            html_rcord.width = 513;
            html_rcord.innerHTML =
                "<font  style='fontSize:26' color='#056f00'>" + this.rcord[index].nickName + "<font style='fontSize:24' color='#402812'>" + '帮你搬砖,经验值' + "<font style='fontSize:26' color='#056f00'>" + "+" + this.rcord[index].wateringExps + "<br/></font>" + "<br/></font>" + "<br/></font> ";
        };
        return TreePage;
    }(ui.TreePageUI));
    view.TreePage = TreePage;
})(view || (view = {}));
//# sourceMappingURL=TreePage.js.map