var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**Created by the LayaAirIDE*/
var view;
(function (view) {
    var Event = laya.events.Event;
    var Box = laya.ui.Box;
    var LeaderboardPage3 = /** @class */ (function (_super) {
        __extends(LeaderboardPage3, _super);
        function LeaderboardPage3(game) {
            var _this = _super.call(this) || this;
            _this.rankData = [];
            _this.calcPointData = [];
            _this.nodeData = [];
            _this.rankListData = [];
            _this.rankTop = [];
            _this.rankPlayerRank = [];
            _this.calcPointListData = [];
            _this.calcPointTop = [];
            _this.nodeListData = [];
            _this.nodeTop = [];
            _this.nodePlayerRank = [];
            _this.game = game;
            _this.list_leaderBoard.array = [];
            _this.sp_rank.visible = true;
            _this.sp_calcPoint.visible = true;
            _this.sp_node.visible = true;
            _this.btn_close.on(Laya.Event.CLICK, _this, _this.onClickClose);
            _this.sp_rank.on(Laya.Event.CLICK, _this, _this.onRank);
            _this.sp_calcPoint.on(Laya.Event.CLICK, _this, _this.onCalcPoint);
            _this.sp_node.on(Laya.Event.CLICK, _this, _this.onNode);
            _this.commonlyUsed = new CommonlyUsed;
            return _this;
        }
        LeaderboardPage3.prototype.onClickClose = function () {
            this.rankData = [];
            this.calcPointData = [];
            this.nodeData = [];
            this.rankListData = [];
            this.rankTop = [];
            this.rankPlayerRank = [];
            this.calcPointListData = [];
            this.calcPointTop = [];
            this.calcPointPlayerRank = -1;
            this.nodeListData = [];
            this.nodeTop = [];
            this.nodePlayerRank = [];
            this.list_leaderBoard.renderHandler = new Laya.Handler(this, this.onRenderDisabled);
            this.close();
        };
        LeaderboardPage3.prototype.onRenderDisabled = function (cell, index) {
        };
        LeaderboardPage3.prototype.onOpened = function () {
            var _this = this;
            rest.friend.getAnking(0, 10, function (data) {
                _this.rankData = data.data.yqsPlayerDTOS;
                for (var i = 0; i < 3; i++) {
                    _this.rankTop.push(_this.rankData[i]);
                }
                ;
                for (var i = 3; i < _this.rankData.length; i++) {
                    _this.rankListData.push(_this.rankData[i]);
                }
                ;
                if (data.data.yqsPlayerRanking.ranking) {
                    _this.rankPlayerRank = data.data.yqsPlayerRanking.ranking;
                }
                //this.onRank();
            });
            rest.friend.bbmCalcPoint(0, 10, function (data) {
                _this.calcPointData = data.data.powerWeekRankingDTOS;
                for (var i = 0; i < 3; i++) {
                    _this.calcPointTop.push(_this.calcPointData[i]);
                }
                ;
                for (var i = 3; i < _this.calcPointData.length; i++) {
                    _this.calcPointListData.push(_this.calcPointData[i]);
                }
                ;
                _this.calcPointPlayerRank = data.data.ranking;
                _this.onCalcPoint();
            });
            rest.galaxy.allGalaxy(function (data) {
                _this.nodeData = data.data;
                for (var i = 0; i < 3; i++) {
                    _this.nodeTop.push(_this.nodeData[i]);
                }
                ;
                for (var i = 3; i < _this.nodeData.length; i++) {
                    _this.nodeListData.push(_this.nodeData[i]);
                }
                ;
                //this.onNode();
            });
        };
        LeaderboardPage3.prototype.onRank = function () {
            if (this.rankListData.length == 0 || this.rankTop.length == 0) {
                return;
            }
            this.img_rank.visible = true;
            this.img_calcPoint.visible = false;
            this.img_node.visible = false;
            this.text_myRanking.style.align = "center";
            if (this.rankPlayerRank.length == 0) {
                this.text_myRanking.innerHTML =
                    "<span style='fontSize:22;vertical-align:middle;color:#666666;'>" + "抱歉您不在排行榜内:" + "</span>";
            }
            else {
                var html = "<span style='fontSize:22;vertical-align:middle;color:#666666;'>" + "我的排名:" + "</span>";
                html += "<span style='fontSize:24;color:#6958fb'  >" + this.rankPlayerRank + "</span>";
                this.text_myRanking.innerHTML = "" + html;
                // "<font  style='fontSize:22' color='#666666' >" + "我的排名:" + "<font  style='fontSize:26' color='#6958fb' >" + this.playerRank + "</font></font>";
            }
            this.commonlyUsed.avatar(this.rankTop[0].headIcon, this.img_num1);
            this.text_num1Name.text = this.rankTop[0].nickName;
            this.text_num1Quantity.text = this.rankTop[0].yqsGrade + '';
            this.img_n1.skin = "leaderBoard/suanli_icon-3.png";
            this.commonlyUsed.avatar(this.rankTop[1].headIcon, this.img_num2);
            this.text_num2Name.text = this.rankTop[1].nickName;
            this.text_num2Quantity.text = this.rankTop[1].yqsGrade + '';
            this.img_n2.skin = "leaderBoard/suanli_icon-3.png";
            this.commonlyUsed.avatar(this.rankTop[2].headIcon, this.img_num3);
            this.text_num3Name.text = this.rankTop[2].nickName;
            this.text_num3Quantity.text = this.rankTop[2].yqsGrade + '';
            this.img_n3.skin = "leaderBoard/suanli_icon-3.png";
            this.list_leaderBoard.array = this.rankListData;
            this.list_leaderBoard.renderHandler = new Laya.Handler(this, this.onRenderRank);
        };
        LeaderboardPage3.prototype.onCalcPoint = function () {
            if (this.calcPointTop.length == 0) {
                return;
            }
            this.img_rank.visible = false;
            this.img_calcPoint.visible = true;
            this.img_node.visible = false;
            this.text_myRanking.style.align = "center";
            if (this.calcPointPlayerRank == null || this.calcPointPlayerRank == -1) {
                this.text_myRanking.innerHTML =
                    "<span style='fontSize:22;vertical-align:middle;color:#666666;'>" + "抱歉您不在排行榜内:" + "</span>";
            }
            else {
                var html = "<span style='fontSize:22;vertical-align:middle;color:#666666;'>" + "我的排名:" + "</span>";
                html += "<span style='fontSize:24;color:#6958fb'  >" + this.calcPointPlayerRank.ranking + "</span>";
                this.text_myRanking.innerHTML = "" + html;
            }
            if (this.calcPointTop.length > 0) {
                this.commonlyUsed.avatar(this.calcPointTop[0].headIcon, this.img_num1);
                this.text_num1Name.text = this.calcPointTop[0].nickName;
                this.text_num1Quantity.text = this.calcPointTop[0].addValue + '';
                this.img_n1.skin = "leaderBoard/suanli_icon.png";
            }
            if (this.calcPointTop.length > 1) {
                this.commonlyUsed.avatar(this.calcPointTop[1].headIcon, this.img_num2);
                this.text_num2Name.text = this.calcPointTop[1].nickName;
                this.text_num2Quantity.text = this.calcPointTop[1].addValue + '';
                this.img_n2.skin = "leaderBoard/suanli_icon.png";
            }
            if (this.calcPointTop.length > 2) {
                this.commonlyUsed.avatar(this.calcPointTop[2].headIcon, this.img_num3);
                this.text_num3Name.text = this.calcPointTop[2].nickName;
                this.text_num3Quantity.text = this.calcPointTop[2].addValue + '';
                this.img_n3.skin = "leaderBoard/suanli_icon.png";
            }
            this.list_leaderBoard.array = this.calcPointListData;
            this.list_leaderBoard.renderHandler = new Laya.Handler(this, this.onRenderCalcPoint);
        };
        LeaderboardPage3.prototype.onNode = function () {
            if (this.nodeListData.length == 0 || this.nodeTop.length == 0) {
                return;
            }
            this.img_rank.visible = false;
            this.img_calcPoint.visible = false;
            this.img_node.visible = true;
            this.text_myRanking.innerHTML =
                "<span style='fontSize:22;vertical-align:middle;color:#666666;'>" + "" + "</span>";
            // this.text_myRanking.style.align = "center";
            // if (this.calcPointPlayerRank == null) {
            // 	this.text_myRanking.innerHTML =
            // 		"<span style='fontSize:22;vertical-align:middle;color:#666666;'>" + "抱歉您所在的节点不在排行榜内:" + "</span>";
            // } else {
            // 	var html: String = "<span style='fontSize:22;vertical-align:middle;color:#666666;'>" + "您所在的节点排名:" + "</span>";
            // 	html += "<span style='fontSize:24;color:#6958fb'  >" + this.nodePlayerRank + "</span>";
            // 	this.text_myRanking.innerHTML = "" + html;
            // }
            this.commonlyUsed.selectGalaxyIcon(this.nodeTop[0].rankLevel, this.img_num1);
            this.text_num1Name.text = this.nodeTop[0].galaxyName;
            this.text_num1Quantity.text = this.nodeTop[0].galaxyPower + '';
            this.img_n1.skin = "leaderBoard/suanli_icon.png";
            this.commonlyUsed.selectGalaxyIcon(this.nodeTop[1].rankLevel, this.img_num2);
            this.text_num2Name.text = this.nodeTop[1].galaxyName;
            this.text_num2Quantity.text = this.nodeTop[1].galaxyPower + '';
            this.img_n2.skin = "leaderBoard/suanli_icon.png";
            this.commonlyUsed.selectGalaxyIcon(this.nodeTop[2].rankLevel, this.img_num3);
            this.text_num3Name.text = this.nodeTop[2].galaxyName;
            this.text_num3Quantity.text = this.nodeTop[2].galaxyPower + '';
            this.img_n3.skin = "leaderBoard/suanli_icon.png";
            this.list_leaderBoard.array = this.nodeListData;
            this.list_leaderBoard.renderHandler = new Laya.Handler(this, this.onRenderPerNode);
        };
        LeaderboardPage3.prototype.onRenderRank = function (cell, index) {
            if (index >= this.rankListData.length) {
                return;
            }
            var text_friendName = cell.getChildByName("text_friendName");
            var lab_fame = cell.getChildByName("lab_fame");
            var img_avatar = cell.getChildByName("img_avatar");
            var text_quantity = cell.getChildByName("text_quantity");
            var text_rankNum = cell.getChildByName("text_rankNum");
            var text_t = cell.getChildByName("text_t");
            var img_icon = cell.getChildByName("img_icon");
            text_friendName.text = this.rankListData[index].nickName;
            text_rankNum.text = this.rankListData[index].ranking + '';
            this.commonlyUsed.avatar(this.rankListData[index].headIcon, img_avatar);
            text_quantity.text = this.rankListData[index].yqsGrade + '';
            text_t.text = "等级";
            img_icon.skin = "leaderBoard/suanli_icon-3.png";
        };
        LeaderboardPage3.prototype.onRenderCalcPoint = function (cell, index) {
            if (index >= this.calcPointListData.length) {
                return;
            }
            var text_friendName = cell.getChildByName("text_friendName");
            var lab_fame = cell.getChildByName("lab_fame");
            var img_avatar = cell.getChildByName("img_avatar");
            var text_quantity = cell.getChildByName("text_quantity");
            var text_rankNum = cell.getChildByName("text_rankNum");
            var text_t = cell.getChildByName("text_t");
            var img_icon = cell.getChildByName("img_icon");
            text_friendName.text = this.calcPointListData[index].nickName;
            text_rankNum.text = this.calcPointListData[index].ranking + '';
            this.commonlyUsed.avatar(this.calcPointListData[index].headIcon, img_avatar);
            text_quantity.text = this.calcPointListData[index].addValue + '';
            text_t.text = "算力";
            img_icon.skin = "leaderBoard/suanli_icon.png";
        };
        LeaderboardPage3.prototype.onRenderPerNode = function (cell, index) {
            if (index >= this.nodeListData.length) {
                return;
            }
            var text_friendName = cell.getChildByName("text_friendName");
            var lab_fame = cell.getChildByName("lab_fame");
            var img_avatar = cell.getChildByName("img_avatar");
            var text_quantity = cell.getChildByName("text_quantity");
            var text_rankNum = cell.getChildByName("text_rankNum");
            var text_t = cell.getChildByName("text_t");
            var img_icon = cell.getChildByName("img_icon");
            text_friendName.text = this.nodeListData[index].galaxyName;
            text_rankNum.text = this.nodeListData[index].rankPosi + '';
            this.commonlyUsed.selectGalaxyIcon(this.nodeListData[index].rankLevel, img_avatar);
            text_quantity.text = this.nodeListData[index].galaxyPower + '';
            text_t.text = "算力";
            img_icon.skin = "leaderBoard/suanli_icon.png";
        };
        LeaderboardPage3.prototype.onMouse = function (e, index) {
            if (e.type == Event.CLICK) {
                if ((e.target) instanceof Box) {
                    //console.log(index)
                }
            }
        };
        return LeaderboardPage3;
    }(ui.LeaderboardPageUI));
    view.LeaderboardPage3 = LeaderboardPage3;
})(view || (view = {}));
//# sourceMappingURL=LeaderboardPage3.js.map