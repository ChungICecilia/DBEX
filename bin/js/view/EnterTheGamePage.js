var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**Created by the LayaAirIDE*/
var view;
(function (view) {
    var EnterTheGamePage = /** @class */ (function (_super) {
        __extends(EnterTheGamePage, _super);
        function EnterTheGamePage() {
            var _this = _super.call(this) || this;
            _this.commonlyUsed = new CommonlyUsed;
            _this.btn_login.on(Laya.Event.CLICK, _this, _this.onSubmit);
            return _this;
        }
        EnterTheGamePage.prototype.onSubmit = function () {
            var _this = this;
            this.ani_EnterTheGameLoading.visible = true;
            if (this.ani_EnterTheGameLoading.visible) {
                this.btn_login.disabled = true;
            }
            this.ani_EnterTheGameLoading.play(0, true, "ani1");
            var inviteCode = this.commonlyUsed.UrlSearch();
            this.btn_login.disabled = true;
            rest.user.userInfo(inviteCode, (function (data) {
                console.log(data);
                if (data.status == "error") {
                    _this.event('ONLOGIN');
                }
                else {
                    Rest.USERINFO = data.data.player;
                    Rest.SKIP = data.data.isSkip;
                    _this.event('GO');
                    // this.ani_EnterTheGameLoading.clear();
                    rest.user.coinsList(function (param) {
                        console.log(param);
                        if (param.status == "error") {
                            console.log("拉取coins信息失败");
                        }
                        else {
                            Rest.COINSLIST = param.data;
                        }
                    });
                }
            }));
        };
        // 关闭登录
        EnterTheGamePage.prototype.close = function () {
            if (this.parent != null) {
                this.parent.removeChild(this);
            }
        };
        return EnterTheGamePage;
    }(ui.EnterTheGamePageUI));
    view.EnterTheGamePage = EnterTheGamePage;
})(view || (view = {}));
//# sourceMappingURL=EnterTheGamePage.js.map