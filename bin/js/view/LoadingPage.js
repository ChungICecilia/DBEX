var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
* name
*/
var view;
(function (view) {
    var LoadingPage = /** @class */ (function (_super) {
        __extends(LoadingPage, _super);
        function LoadingPage() {
            var _this = _super.call(this) || this;
            _this.mouseEnabled = true;
            _this.mouseThrough = false;
            _this.size(750, 1207);
            return _this;
        }
        LoadingPage.loading = function () {
            if (LoadingPage.loadingPage && LoadingPage.loadingPage.parent) {
                return;
            }
            if (!LoadingPage.loadingPage) {
                LoadingPage.loadingPage = new LoadingPage();
                LoadingPage.loadingPage.zOrder = 10001;
            }
            LoadingPage.loadingPage.mouseThrough = false;
            LoadingPage.loadingPage.makeAni();
            Laya.stage.addChild(LoadingPage.loadingPage);
        };
        LoadingPage.dispose = function () {
            if (LoadingPage.loadingPage) {
                LoadingPage.loadingPage.removeSelf();
                LoadingPage.loadingPage.dispose();
            }
        };
        LoadingPage.prototype.makeAni = function () {
            this.ani = new Laya.Animation();
            //加载动画文件
            this.ani.loadAnimation("loading1.ani");
            //添加到舞台
            this.addChild(this.ani);
            this.ani.x = Laya.stage.width / 2;
            this.ani.y = Laya.stage.height / 2;
            //播放Animation动画
            this.ani.play();
        };
        LoadingPage.prototype.dispose = function () {
            if (this.ani) {
                this.ani.stop();
                this.ani.removeSelf();
                this.ani.destroy();
                this.ani = null;
            }
        };
        return LoadingPage;
    }(Laya.Sprite));
    view.LoadingPage = LoadingPage;
})(view || (view = {}));
//# sourceMappingURL=LoadingPage.js.map