var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
* name
*/
var view;
(function (view) {
    var NoticePanel = /** @class */ (function (_super) {
        __extends(NoticePanel, _super);
        function NoticePanel() {
            var _this = _super.call(this) || this;
            _this.btn_close.on(Laya.Event.CLICK, _this, _this.close);
            _this.zOrder = 10000;
            return _this;
        }
        return NoticePanel;
    }(ui.NoticePanelUI));
    view.NoticePanel = NoticePanel;
})(view || (view = {}));
//# sourceMappingURL=NoticePanel.js.map