var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**Created by the LayaAirIDE*/
var view;
(function (view) {
    var Browser = Laya.Browser;
    var DesktopPage = /** @class */ (function (_super) {
        __extends(DesktopPage, _super);
        function DesktopPage() {
            var _this = _super.call(this) || this;
            _this.data = [];
            _this.btn_close.on(Laya.Event.CLICK, _this, _this.onClickClose);
            return _this;
        }
        DesktopPage.prototype.onClickClose = function () {
            this.close();
        };
        DesktopPage.prototype.onOpened = function () {
            if (Browser.onAndriod || Browser.onAndroid) {
                this.img_up.skin = "share/newTree_13.png";
                this.img_down.skin = "share/newTree_14.png";
            }
            else {
                this.img_up.skin = "share/newTree_16.png";
                this.img_down.skin = "share/newTree_17.png";
            }
        };
        return DesktopPage;
    }(ui.DesktopPageUI));
    view.DesktopPage = DesktopPage;
})(view || (view = {}));
//# sourceMappingURL=DesktopPage.js.map