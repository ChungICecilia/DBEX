/**
* name
*/
var view;
(function (view) {
    var JinbiElement = /** @class */ (function () {
        function JinbiElement(skin) {
            this.angle = 2;
            this._skin = skin;
            this.rX = this._skin.x;
            this.rY = this._skin.y;
            this._skin.visible = true;
            this.speed = 0.01 + Math.random() * 0.02;
            this._skin.on(Laya.Event.CLICK, this, this.click);
        }
        ;
        /**
         *
         * 缓动
         **/
        JinbiElement.prototype.tween = function () {
            /**上下回弹*/
            this._skin.y = this.rY + Math.sin(this.angle) * 10;
            this.angle += this.speed;
        };
        /**
         *
         * 点击
         **/
        JinbiElement.prototype.click = function () {
            var _this = this;
            rest.work.pickCoin({ id: this._info["id"] }, function (data) {
                console.log(_this._info["id"]);
                if (data.status == "error") {
                }
                else {
                    Rest.USERINFO = data.data;
                    var leg = Rest.COINSLIST.length;
                    for (var i = 0; i < leg; i++) {
                        var obj = Rest.COINSLIST[i];
                        if (obj["id"] == _this._info["id"]) {
                            Rest.COINSLIST.splice(i, 1);
                            i = leg;
                            curLeg = Rest.COINSLIST.length;
                            _this.moveTween(curLeg);
                        }
                    }
                }
            });
        };
        /**
         *
         * 飘往
         **/
        JinbiElement.prototype.moveTween = function (leg) {
            var _this = this;
            Laya.timer.clear(this, this.tween);
            var flyX = 550;
            var flyY = 20;
            Laya.Tween.to(this._skin, { x: flyX, y: flyY, scaleX: 0, scaleY: 0 }, 2000, Laya.Ease.quintInOut, Laya.Handler.create(this, function () {
                _this._skin.x = _this.rX;
                _this._skin.y = _this.rY;
                view.EventCenter.dispatchEvent("refreshUserInfo");
                var ind = 10 - (leg - curLeg);
                console.info(ind);
                if (leg >= 10) {
                    console.info(_this.info + ">>>>>>>>>>>>>>");
                    _this.info = Rest.COINSLIST[ind - 1];
                    Laya.Tween.to(_this._skin, { scaleX: 1, scaleY: 1 }, 1000, Laya.Ease.bounceOut);
                    view.EventCenter.dispatchEvent("moreCoins");
                }
                else {
                    _this.hide();
                }
            }));
        };
        /**
         *
         * 隐藏
         **/
        JinbiElement.prototype.hide = function () {
            this.info = null;
            this._skin.visible = false;
        };
        Object.defineProperty(JinbiElement.prototype, "info", {
            get: function () {
                return this._info;
            },
            set: function (value) {
                this._info = value;
                if (!value) {
                    return;
                }
                this.txt = this._skin["txt_value"];
                this.txt.text = this._info["gainBbmCount"] + "";
                Laya.timer.frameLoop(1, this, this.tween, null);
                this._skin.visible = true;
            },
            enumerable: true,
            configurable: true
        });
        return JinbiElement;
    }());
    view.JinbiElement = JinbiElement;
})(view || (view = {}));
//# sourceMappingURL=JinbiElement.js.map