var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
* name
*/
var view;
(function (view) {
    var DrawPage = /** @class */ (function (_super) {
        __extends(DrawPage, _super);
        function DrawPage(game) {
            var _this = _super.call(this) || this;
            //物品个数;
            _this.count = 8;
            //最少旋转圈数;
            _this.rotateNum = 8;
            _this.optionPrompt = new OptionPrompt();
            _this.drawType = 1;
            _this.curScore = 0;
            _this.angle = 360 / _this.count;
            _this.game = game;
            return _this;
        }
        DrawPage.prototype.initialize = function () {
            _super.prototype.initialize.call(this);
            this.btn_close.on(Laya.Event.CLICK, this, this.onClickClose);
            this.btn_draw.on(Laya.Event.CLICK, this, this.onDrawClick);
        };
        DrawPage.prototype.onClickClose = function (e) {
            this.close();
        };
        /**
         *
         */
        DrawPage.prototype.onClosed = function () {
            this.pic_circle.rotation = 0;
            if (this.tween)
                Laya.Tween.clear(this.tween);
            if (this.tl) {
                this.tl.stop();
                this.tl.clear();
                this.tl = null;
            }
            if (this.light) {
                this.light.stop();
                this.light.clear();
                this.light = null;
            }
        };
        /**
         *
         */
        DrawPage.prototype.onOpened = function () {
            this.pic_circle.rotation = 0;
            this.getPrizesList();
            if (!this.tl) {
                this.tl = new Laya.Animation();
                this.tl.loadAnimation("choujiang.ani");
                this.addChild(this.tl);
                this.tl.x = 370;
                this.tl.y = 648;
                this.tl.mouseEnabled = false;
                this.tl.mouseThrough = true;
            }
            if (!this.light) {
                this.light = new Laya.Animation();
                this.light.loadAnimation("choujiang1.ani");
                this.addChild(this.light);
                this.light.x = 372;
                this.light.y = 648;
                this.light.visible = false;
            }
            this.setChildIndex(this.txt_score, this.numChildren - 1);
            this.tl.play(0, true, "ani1");
            this.btn_draw.mouseEnabled = true;
        };
        DrawPage.prototype.getPrizesList = function () {
            var _this = this;
            rest.draw.list(function (data) {
                if (data.code == 1) {
                    _this.prizes = data.data.rewardSectorReturnDTO;
                    _this.makePrizeList(_this.prizes);
                    if (data.data.drawStatus) {
                        _this.drawType = 2;
                        _this.txt_score.text = Rest.OPTION.draw_every_draw_costs + "积分";
                    }
                    else {
                        _this.txt_score.text = "免费";
                        _this.drawType = 1;
                    }
                    _this.curScore = data.data.rewardPoints;
                    _this.txt_curScore.text = "当前积分为:" + _this.curScore;
                }
                else {
                    OptionPrompt.ins.play(data.msg);
                }
            });
        };
        DrawPage.prototype.makePrizeList = function (prizes) {
            var len = prizes.length;
            var i = 0;
            var obj;
            var skin = "";
            var name = "";
            for (i = 0; i < len; i++) {
                obj = prizes[i];
                switch (obj.rewardType) {
                    case 1:
                        skin = "drawPrize/power.png";
                        name = "算力";
                        break;
                    case 4:
                        skin = "drawPrize/zuan.png";
                        name = "DBEX";
                        break;
                    case 3:
                        skin = "drawPrize/exp.png";
                        name = "经验";
                        break;
                }
                var sp = this["pic_prize" + i];
                sp.skin = skin;
                this["txt_prize" + i].text = name + "+" + obj.rewardQuantity;
                if (obj.isImportant == 1 && obj.rewardType == 4) {
                    sp.skin = "drawPrize/09.png";
                }
            }
        };
        //开始抽奖
        DrawPage.prototype.draw = function (id) {
            this.tl.stop();
            this.tl.visible = false;
            this.pic_circle.rotation %= 360;
            var need = this.angle * id + 360 * this.rotateNum - this.pic_circle.rotation;
            this.light.play(0, true);
            this.light.visible = true;
            this.tween = Laya.Tween.to(this.pic_circle, { rotation: need + this.pic_circle.rotation }, 4000, Laya.Ease.circInOut, Laya.Handler.create(this, this.onReset, []));
            console.log(this.angle);
        };
        DrawPage.prototype.getPrizeInx = function (id) {
            var i = 0;
            if (this.prizes) {
                var len = this.prizes.length;
                for (i = 0; i < len; i++) {
                    if (this.prizes[i].id == id) {
                        return i;
                    }
                }
            }
            return -1;
        };
        DrawPage.prototype.onDrawClick = function (e) {
            var _this = this;
            this.btn_draw.mouseEnabled = false;
            rest.draw.drawPrize(this.drawType, function (data) {
                if (data.code == 1) {
                    if (_this.drawType == 1) {
                        _this.drawType = 2;
                        _this.txt_score.text = Rest.OPTION.draw_every_draw_costs + "积分";
                    }
                    else {
                        _this.curScore -= Rest.OPTION.draw_every_draw_costs;
                    }
                    _this.draw(_this.getPrizeInx(data.data));
                }
                else {
                    OptionPrompt.ins.play(data.msg);
                    _this.btn_draw.mouseEnabled = true;
                }
                _this.txt_curScore.text = "当前积分为:" + _this.curScore;
            });
        };
        DrawPage.prototype.onReset = function () {
            var _this = this;
            this.light.stop();
            this.light.visible = false;
            this.tl.stop();
            this.tl.visible = true;
            this.tl.on(Laya.Event.COMPLETE, this, this.lightComplete);
            this.tl.play(0, false, "ani3");
            if (this.tween)
                Laya.Tween.clear(this.tween);
            var inviteCode;
            rest.user.userInfo(inviteCode, function (data) {
                Rest.USERINFO = data.data.player;
                Rest.GALAXY = data.data.byDbexGalaxy;
                _this.game.onCreatedRole();
            });
            if (this.drawType == 2) {
                this.txt_score.text = Rest.OPTION.draw_every_draw_costs + "积分";
            }
        };
        DrawPage.prototype.lightComplete = function () {
            this.tl.stop();
            this.tl.off(Laya.Event.COMPLETE, this, this.lightComplete);
            this.tl.play(0, true, "ani1");
            this.btn_draw.mouseEnabled = true;
        };
        return DrawPage;
    }(ui.DrawPageUI));
    view.DrawPage = DrawPage;
})(view || (view = {}));
//# sourceMappingURL=DrawPage.js.map