var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
* name
*/
var view;
(function (view) {
    var PrizePage = /** @class */ (function (_super) {
        __extends(PrizePage, _super);
        function PrizePage(game) {
            var _this = _super.call(this) || this;
            _this.activityId = 0;
            _this.OptionPrompt = new OptionPrompt();
            _this.game = game;
            _this.txt_add.style.bold = true;
            _this.txt_add.style.align = "center";
            _this.btn_close.on(Laya.Event.CLICK, _this, _this.onClickClose);
            _this.btn_get.on(Laya.Event.CLICK, _this, _this.onGetClick);
            return _this;
        }
        PrizePage.prototype.setData = function (data) {
            var desc = "";
            if (data.answerStatus == 1) {
                desc = "您获得参与奖";
            }
            else if (data.answerStatus == 2) {
                desc = "您获得大奖";
            }
            else {
                desc = "您获得参与奖";
            }
            this.txt_people.text = String(data.participantsPeopleNumber);
            this.txt_prizeMan.text = String(data.getRewardsPeopleNumber);
            this.txt_total.text = String(data.rewardQuantity);
            this.txt_bigPrize.text = String(data.finishReward);
            this.txt_canyu.text = String(data.participationReward);
            this.txt_title.text = String(data.description);
            this.activityId = data.activityId;
            this.txt_add.innerHTML = Font.getSizeColor("#5d3500", 28, desc) + Font.getSizeColor("#593cce", 46, String(data.getRewardQuantity)) + Font.getSizeColor("#5d3500", 46, "DBEX");
            this.event('Sounds', 7);
        };
        PrizePage.prototype.onClickClose = function () {
            this.close();
        };
        PrizePage.prototype.onGetClick = function () {
            var _this = this;
            rest.chong.reward(this.activityId, function (data) {
                if (data.status == "error") {
                    // this.ani_loginLoading.clear();
                    // this.showErrorTips(data.msg);
                    console.log(data.msg);
                    _this.OptionPrompt.play(data.msg, [Laya.stage.width / 2, Laya.stage.height / 2]);
                }
                else {
                    //this.setInfo(data.data);
                    if (data.code) {
                        //this.getTopic(data.data.topicId);
                        console.log("奖励领取成功");
                        _this.close();
                        var inviteCode = void 0;
                        rest.user.userInfo(inviteCode, function (data) {
                            Rest.USERINFO = data.data.player;
                            Rest.GALAXY = data.data.byDbexGalaxy;
                            _this.game.onCreatedRole();
                        });
                    }
                    else {
                        console.log(data.msg);
                    }
                }
            });
        };
        return PrizePage;
    }(ui.PrizePageUI));
    view.PrizePage = PrizePage;
})(view || (view = {}));
//# sourceMappingURL=PrizePage.js.map