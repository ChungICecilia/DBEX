var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**Created by the LayaAirIDE*/
var view;
(function (view) {
    var QSJPage = /** @class */ (function (_super) {
        __extends(QSJPage, _super);
        function QSJPage(game) {
            var _this = _super.call(this) || this;
            _this.game = game;
            _this.btn_close.on(Laya.Event.CLICK, _this, _this.onClickClose);
            return _this;
        }
        QSJPage.prototype.onClickClose = function () {
            this.close();
        };
        return QSJPage;
    }(ui.QSJPageUI));
    view.QSJPage = QSJPage;
})(view || (view = {}));
//# sourceMappingURL=QSJPage.js.map