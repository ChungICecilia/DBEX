var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// 程序入口
var view;
(function (view) {
    var SoundManager = Laya.SoundManager;
    var Handler = Laya.Handler;
    var GamePage = /** @class */ (function (_super) {
        __extends(GamePage, _super);
        function GamePage() {
            var _this = _super.call(this) || this;
            _this.music = true;
            _this.buttonSound = true;
            _this.first = false;
            // this.yun = new Laya.Animation();
            // this.yun.loadAnimation("yun.ani", Laya.Handler.create(this, this.onLoaded), null);
            //加载图集资源 
            Laya.loader.load(["res/atlas/animal/lou.atlas", "res/atlas/uiLayout.atlas", "res/atlas/chooseHead.atlas",
                "res/atlas/animal/jingdutiao.atlas", "res/atlas/animal/wallet.atlas", "res/atlas/calcPoint.atlas", "res/atlas/wallet.atlas", "res/atlas/chong.atlas"], new Handler(_this, _this.init));
            return _this;
        }
        GamePage.prototype.init = function () {
            var _this = this;
            this.backgroud = new view.Background(this);
            this.addChild(this.backgroud);
            // this.ui.img_bg.skin = "res/singleImages/bg.jpg";
            var inviteCode;
            rest.user.userInfo(inviteCode, (function (data) {
                if (data.status == "error") {
                    // GamePage.first = true;
                    if (_this.ChooseHead == null) {
                        _this.ChooseHead = new view.ChooseHead(_this);
                        _this.ChooseHead.on("CreatedRole", _this, _this.onCreatedRole);
                    }
                    _this.ChooseHead.popup(true, true);
                }
                else {
                    //console.log(data)
                    if (data.data.player.refGalaxyId == null) {
                        if (_this.ChangeGalaxyPage == null) {
                            _this.ChangeGalaxyPage = new view.ChangeGalaxyPage(_this.ChooseHead);
                        }
                        Laya.stage.addChild(_this.ChangeGalaxyPage);
                    }
                    Rest.USERINFO = data.data.player;
                    Rest.GALAXY = data.data.byDbexGalaxy;
                    Rest.SKIP = data.data.isSkip;
                }
                _this.ui = new view.UILayout(_this);
                _this.addChild(_this.ui);
                _this.ui.on('Sounds', _this, _this.playSounds);
                _this.on('Sounds', _this, _this.playSounds);
                if (Rest.USERINFO.guidMoveSteps != 0) {
                    if (_this.CalcPointPage == null) {
                        _this.CalcPointPage = new view.CalcPointPage(_this);
                    }
                    _this.CalcPointPage.popup(false, true);
                }
            }));
            rest.chong.getPrizeNotice(function (data) {
                var datas = data.data;
                if (datas && datas.length > 0) {
                    if (_this.prizePage == null) {
                        _this.prizePage = new view.PrizePage(_this);
                        _this.prizePage.setData(datas[0]);
                    }
                    _this.prizePage.popup(false, true);
                }
            });
            // this.addChild(this.yun);
            // this.yun.pos(((Laya.stage.width - this.round.width) >> 1) + 365, ((Laya.stage.height - this.round.height) >> 1) - 100);
            // this.yun.play(0, true, "yun");
            // this.ui = new view.UILayout(this);
            // this.addChild(this.ui);
            switch (Rest.USERINFO.bgmStatus) {
                case 1:
                    SoundManager.playMusic("res/sounds/dbexBGM.mp3", 0, new Handler(this, this.onComplete));
                    this.onPlayMusic(true);
                    break;
                case 2:
                    this.onPlayMusic(false);
                    break;
            }
            switch (Rest.USERINFO.btnMusicStatus) {
                case 1:
                    this.onPlaySound(true);
                    break;
                case 2:
                    this.onPlaySound(false);
                    break;
            }
            // this.ui.on('Sounds', this, this.playSounds);
            this.on('GameMusic', this, this.onPlayMusic);
            this.on('GameSound', this, this.onPlaySound);
            //更新用户信息
            this.on('UPDATEUSER', this, this.onUpdateUser);
            this.event('CLOSE');
            if (this.noticePanel == null) {
                this.noticePanel = new view.NoticePanel();
            }
            this.noticePanel.popup(false, true);
        };
        //背景音乐
        GamePage.prototype.onPlayMusic = function (music) {
            this.music = music;
            if (!music) {
                SoundManager.stopMusic();
            }
        };
        //音效
        GamePage.prototype.onPlaySound = function (sound) {
            this.buttonSound = sound;
            if (!sound) {
                SoundManager.stopAllSound();
            }
        };
        GamePage.prototype.onUpdateUser = function () {
            this.ui.updateUserInfo();
        };
        GamePage.prototype.playSounds = function (num) {
            console.log(num);
            switch (num) {
                case 1:
                    if (this.buttonSound)
                        SoundManager.playSound("res/sounds/click.mp3", 1, new Handler(this, this.onComplete));
                    break;
                case 2:
                    if (this.buttonSound)
                        SoundManager.playSound("res/sounds/receive.mp3", 1, new Handler(this, this.onComplete));
                    break;
                case 3:
                    if (this.buttonSound)
                        SoundManager.playSound("res/sounds/watering.mp3", 1, new Handler(this, this.onComplete));
                    break;
                case 4:
                    if (this.buttonSound)
                        SoundManager.playSound("res/sounds/addBBM.mp3", 1, new Handler(this, this.onComplete));
                    break;
                case 5:
                    if (this.buttonSound)
                        SoundManager.playSound("res/sounds/open.mp3", 1, new Handler(this, this.onComplete));
                    break;
                case 6:
                    if (this.buttonSound)
                        SoundManager.playSound("res/sounds/answerRight.mp3", 1, new Handler(this, this.onComplete));
                    break;
                case 7:
                    if (this.buttonSound)
                        SoundManager.playSound("res/sounds/gainPrize.mp3", 1, new Handler(this, this.onComplete));
                    break;
                case 8:
                    if (this.buttonSound)
                        SoundManager.playSound("res/sounds/noPrize.mp3", 1, new Handler(this, this.onComplete));
                    break;
            }
        };
        GamePage.prototype.onComplete = function () {
            console.log("播放完成");
        };
        GamePage.prototype.onCreatedRole = function () {
            this.ui.updateUserInfo();
            this.ui.userTree();
            this.ui.bbmShow();
            this.ui.notic();
            this.ui.redPoint();
            this.ui.PlayTreeAni();
        };
        return GamePage;
    }(Laya.Sprite));
    view.GamePage = GamePage;
})(view || (view = {}));
//# sourceMappingURL=GamePage.js.map