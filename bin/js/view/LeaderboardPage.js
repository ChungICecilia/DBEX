var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**Created by the LayaAirIDE*/
var view;
(function (view) {
    var Event = laya.events.Event;
    var Box = laya.ui.Box;
    var LeaderboardPage = /** @class */ (function (_super) {
        __extends(LeaderboardPage, _super);
        function LeaderboardPage(game) {
            var _this = _super.call(this) || this;
            _this.data = [];
            _this.listData = [];
            _this.topThree = [];
            _this.playerRank = [];
            _this.game = game;
            _this.list_leaderBoard.array = [];
            _this.btn_close.on(Laya.Event.CLICK, _this, _this.onClickClose);
            //this.btn_tool.on(Laya.Event.CLICK, this, this.onClickTool);
            _this.commonlyUsed = new CommonlyUsed;
            return _this;
        }
        LeaderboardPage.prototype.onClickClose = function () {
            this.listData = [];
            this.topThree = [];
            this.playerRank = [];
            this.list_leaderBoard.renderHandler = new Laya.Handler(this, this.onRenderDisabled);
            this.close();
        };
        LeaderboardPage.prototype.onOpened = function () {
            var _this = this;
            rest.friend.bbmCalcPoint(0, 10, function (data) {
                _this.data = data.data.topRank;
                _this.text_myRanking.style.align = "center";
                // this.text_myRanking._getCSSStyle().valign = "middle";
                if (data.data.playerRank == null) {
                    _this.text_myRanking.innerHTML =
                        "<font  style='fontSize:22' color='#666666' 'bold = true'>" + "抱歉您不在排行榜内" + "</font>";
                }
                else {
                    _this.playerRank = data.data.playerRank.playerRank;
                    var html = "<span style='fontSize:22;vertical-align:middle;color:#666666;'>" + "我的排名:" + "</span>";
                    html += "<span style='fontSize:24;color:#6958fb'  >" + _this.playerRank + "</span>";
                    _this.text_myRanking.innerHTML = "" + html;
                    // "<font  style='fontSize:22' color='#666666' >" + "我的排名:" + "<font  style='fontSize:26' color='#6958fb' >" + this.playerRank + "</font></font>";
                }
                for (var i = 0; i < 3; i++) {
                    _this.topThree.push(_this.data[i]);
                }
                ;
                for (var i = 3; i < _this.data.length; i++) {
                    _this.listData.push(_this.data[i]);
                }
                ;
                console.log(_this.topThree);
                console.log(_this.listData);
                _this.commonlyUsed.avatar(_this.data[0].playerHeadIcon, _this.img_num1);
                _this.text_num1Name.text = _this.topThree[0].playerNickName;
                _this.text_num1Quantity.text = _this.topThree[0].playerBbmCalcPoint + '';
                _this.commonlyUsed.avatar(_this.data[1].playerHeadIcon, _this.img_num2);
                _this.text_num2Name.text = _this.topThree[1].playerNickName;
                _this.text_num2Quantity.text = _this.topThree[1].playerBbmCalcPoint + '';
                _this.commonlyUsed.avatar(_this.data[2].playerHeadIcon, _this.img_num3);
                _this.text_num3Name.text = _this.topThree[2].playerNickName;
                _this.text_num3Quantity.text = _this.topThree[2].playerBbmCalcPoint + '';
                _this.list_leaderBoard.array = _this.listData;
                _this.list_leaderBoard.renderHandler = new Laya.Handler(_this, _this.onRender);
                _this.list_leaderBoard.mouseHandler = new Laya.Handler(_this, _this.onMouse);
            });
        };
        LeaderboardPage.prototype.onRender = function (cell, index) {
            if (index >= this.data.length) {
                return;
            }
            var text_friendName = cell.getChildByName("text_friendName");
            var lab_fame = cell.getChildByName("lab_fame");
            var img_avatar = cell.getChildByName("img_avatar");
            var text_quantity = cell.getChildByName("text_quantity");
            var text_rankNum = cell.getChildByName("text_rankNum");
            text_friendName.text = this.listData[index].playerNickName;
            text_rankNum.text = this.listData[index].playerRank + '';
            this.commonlyUsed.avatar(this.listData[index].playerHeadIcon, img_avatar);
            text_quantity.text = this.listData[index].playerBbmCalcPoint + '';
        };
        LeaderboardPage.prototype.onRenderDisabled = function (cell, index) {
        };
        LeaderboardPage.prototype.onMouse = function (e, index) {
            if (e.type == Event.CLICK) {
                if ((e.target) instanceof Box) {
                    //console.log(index)
                }
            }
        };
        return LeaderboardPage;
    }(ui.LeaderboardPageUI));
    view.LeaderboardPage = LeaderboardPage;
})(view || (view = {}));
//# sourceMappingURL=LeaderboardPage.js.map