var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**Created by the LayaAirIDE*/
var view;
(function (view) {
    var Browser = Laya.Browser;
    var CalcPointPage = /** @class */ (function (_super) {
        __extends(CalcPointPage, _super);
        function CalcPointPage(game) {
            var _this = _super.call(this) || this;
            _this.rcord = [];
            _this.game = game;
            _this.btn_close.on(Laya.Event.CLICK, _this, _this.onClickClose);
            _this.btn_wallet.on(Laya.Event.CLICK, _this, _this.onClickWallet);
            return _this;
        }
        CalcPointPage.prototype.onClickClose = function () {
            this.close();
        };
        CalcPointPage.prototype.onClickWallet = function () {
            Browser.window.location.href = "http://www.bbb-home.com/appDownload.html";
        };
        CalcPointPage.prototype.onOpened = function () {
            //持有bbm数量
            // Rest.USERINFO.marketBbm != null ? this.text_bbm.text = (Rest.USERINFO.marketBbm).toFixed(4) : this.text_bbm.text = '0';
            if (Rest.USERINFO.marketBbm > 0) {
                this.img_noBBM.visible = false;
                this.text_noBBM.visible = false;
                this.zero_dbexImg.visible = false;
                // this.text_bbm.visible = true;
                this.hold_dbexHtml.visible = true;
                this.hold_dbexHtml.style.align = "center";
                var html = "<span style='fontSize:30;vertical-align:top;color:#713a0e;'>" + "持有" + "</span>";
                html += "<img src='calcPoint/01.png'></img>";
                html += "<span style='fontSize:30;vertical-align:top;color:#713a0e;'  >" + "DBEX:" + "</span>";
                html += "<span style='fontSize:30;color:#713a0e'  >" + (Rest.USERINFO.marketBbm).toFixed(4) + "</span>";
                this.hold_dbexHtml.innerHTML = html + "";
                this.img_haveBBM.visible = true;
                this.html_calcPoint.visible = true;
                this.html_calcPoint.style.align = "center";
                this.html_calcPoint.width = 492;
                this.html_calcPoint.innerHTML =
                    "<font style='fontSize:30' color='#713a0e'>" + "增加了" + Rest.USERINFO.marketCalcPoint + "算力" + "<br/></font>";
            }
            else {
                this.img_noBBM.visible = true;
                this.text_noBBM.visible = true;
            }
        };
        return CalcPointPage;
    }(ui.CalcPointPageUI));
    view.CalcPointPage = CalcPointPage;
})(view || (view = {}));
//# sourceMappingURL=CalcPointPage.js.map