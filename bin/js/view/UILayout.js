var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**Created by the LayaAirIDE*/
var view;
(function (view) {
    var Browser = Laya.Browser;
    var UILayout = /** @class */ (function (_super) {
        __extends(UILayout, _super);
        function UILayout(game) {
            var _this = _super.call(this) || this;
            _this.data = [];
            _this.notices = [];
            _this.playSource = 0;
            _this.game = game;
            _this.errorStepBack = false;
            _this.activityTime = [];
            // this.img_bg.skin = "res/singleImages/bg.jpg";
            //加载图集资源 "res/atlas/desktop.atlas" ,"res/atlas/tree.atlas", 
            Laya.loader.load(["res/atlas/my.atlas", "res/atlas/news.atlas", "res/atlas/qiandao.atlas",
                "res/atlas/task.atlas", "res/atlas/share.atlas", "res/atlas/leaderBoard.atlas", "res/atlas/animal/guid.atlas",
                "res/atlas/question.atlas", "res/atlas/answer.atlas", "res/atlas/chong.atlas", "res/atlas/drawPrize.atlas"], new Laya.Handler(_this, _this.init));
            _this.dbexNum = new ScrollNum();
            _this.powerNum = new ScrollNum();
            _this.dbexNum.x = 589;
            _this.dbexNum.y = 24;
            _this.powerNum.x = 589;
            _this.powerNum.y = 78;
            _this.addChild(_this.dbexNum);
            _this.addChild(_this.powerNum);
            return _this;
        }
        UILayout.prototype.init = function () {
            var _this = this;
            console.log(Rest.OPTION);
            //this.ani_tree.play(0, true, "ani1");
            //排行榜 x:642 y:296
            //问答 643 400
            //冲顶大会btn_chong  text_chong 643 505
            //this.box_b
            rest.chong.info(function (data) {
                //btn_chong
                if (data.status != "error" || data.msg == "已经完成问答") {
                    var str = data.data.previewTime;
                    var preview = DateUtil.Timestamp(str);
                    var s = data.data.endTime;
                    var end = DateUtil.Timestamp(s);
                    var date1 = new Date();
                    var now = date1.getTime();
                    if (preview <= now || end < now) {
                        _this.btn_chong.visible = true;
                        var s_1 = data.data.startTime;
                        var startTime = DateUtil.Timestamp(s_1);
                        var e = data.data.endTime;
                        var endTime = DateUtil.Timestamp(e);
                        _this.activityTime[0] = { startTime: startTime, endTime: endTime };
                        Laya.timer.loop(1000, _this, _this.onLoop);
                        Laya.timer.loop(100, _this, _this.countdownEnds);
                        _this.box_b.x = 643;
                        _this.box_b.y = 400;
                    }
                }
                else {
                    _this.btn_chong.visible = false;
                    _this.box_b.x = 643;
                    _this.box_b.y = 296;
                }
                rest.answer.getQuestion(function (data) {
                    if (data.data != null) {
                        var s = data.data.stateTime;
                        var startTime = DateUtil.Timestamp(s);
                        var e = data.data.endTime;
                        var endTime = DateUtil.Timestamp(e);
                        var d = new Date();
                        var now = d.getTime();
                        if (startTime <= now && endTime > now) {
                            _this.btn_answer.visible = true;
                        }
                        else {
                            _this.btn_answer.visible = false;
                        }
                    }
                });
            });
            this.tips = 0;
            this.out = 0;
            this.js = 0;
            this.bbm = 0;
            UILayout.stepNum = 0;
            this.commonlyUsed = new CommonlyUsed;
            this.redPoint();
            this.upAni = new Laya.Animation();
            this.upAni.loadAnimation("shengji.ani", Laya.Handler.create(this, this.onLoaded), null);
            this.gold = new Laya.Animation();
            this.gold.loadAnimation("feijinbi.ani", Laya.Handler.create(this, this.onLoaded), null);
            this.changBuildAni = new Laya.Animation();
            this.changBuildAni.loadAnimation("huanjianzhu.ani", Laya.Handler.create(this, this.onLoaded), null);
            this.guid = new Laya.Animation();
            this.guid.loadAnimation("guid.ani", Laya.Handler.create(this, this.onLoaded), null);
            this.signIn = new Laya.Animation();
            this.signIn.loadAnimation("qiandao.ani", Laya.Handler.create(this, this.onLoaded), null);
            this.element1.on(Laya.Event.CLICK, this, this.onbbm1);
            this.element2.on(Laya.Event.CLICK, this, this.onbbm2);
            this.element3.on(Laya.Event.CLICK, this, this.onbbm3);
            this.btn_realName.on(Laya.Event.CLICK, this, this.onClickRealName);
            this.btn_icon.on(Laya.Event.CLICK, this, this.onClickIcon);
            this.btn_desktop.on(Laya.Event.CLICK, this, this.onClickDesktop);
            this.btn_shareIt.on(Laya.Event.CLICK, this, this.onClickShareIt);
            this.sp_wallet.on(Laya.Event.CLICK, this, this.onClickWallet);
            this.btn_news.on(Laya.Event.CLICK, this, this.onClickNews);
            this.btn_task.on(Laya.Event.CLICK, this, this.onClickTask);
            this.btn_leaderboard.on(Laya.Event.CLICK, this, this.onClickLeaderboard);
            this.btn_water.on(Laya.Event.CLICK, this, this.onClickWater);
            this.btn_friend.on(Laya.Event.CLICK, this, this.onClickFriend);
            this.btn_receive.on(Laya.Event.CLICK, this, this.onClickReceive);
            this.sp_tree.on(Laya.Event.CLICK, this, this.onClickTree);
            //this.btn_qxj.on(Laya.Event.CLICK, this, this.onQXJ);
            this.btn_answer.on(Laya.Event.CLICK, this, this.onAnswer);
            this.btn_chong.on(Laya.Event.CLICK, this, this.onChongDing);
            this.btn_draw.on(Laya.Event.CLICK, this, this.onDrawPrize);
            // Laya.timer.loop(5000, this, this.PlayTreeAni);
            this.btn_skip.on(Laya.Event.CLICK, this, this.onSkipHandler);
            this.btn_skip.visible = Rest.SKIP;
            this.notic();
            if (Rest.USERINFO.idcardStatus == 0) {
                this.btn_realName.visible = true;
            }
            else {
                this.btn_realName.visible = false;
            }
            rest.user.getInviteCode(function (data) {
                Rest.INVITECODE = data.data;
            });
            if (!this.game.first) {
                switch (Rest.USERINFO.guidMoveSteps) { //
                    case 0:
                        this.guid.pos(this.element1.x + (this.element1.width * 0.5), this.element1.y - 40);
                        this.addChild(this.guid);
                }
                this.guid.play(0, true, "ani1");
            }
            else {
                if (this.guid) {
                    this.guid.removeSelf();
                }
            }
            view.EventCenter.add("refreshUserInfo", this, this.updateUserInfo);
            view.EventCenter.add("moreCoins", this, this.updateBBM);
        };
        UILayout.prototype.onLoop = function () {
            var date = new Date();
            var now = date.getTime();
            var startTime = this.activityTime[0].startTime;
            var endTime = this.activityTime[0].endTime;
            if (startTime > now) {
                this.text_chong.text = this.activityTime[0].startTime;
                this.text_chong.text = DateUtil.second2String((startTime - now) / 1000);
            }
        };
        UILayout.prototype.countdownEnds = function () {
            var date = new Date();
            var now = date.getTime();
            var startTime = this.activityTime[0].startTime;
            var endTime = this.activityTime[0].endTime;
            if (startTime <= now) {
                Laya.timer.clear(this, this.onLoop);
                Laya.timer.clear(this, this.countdownEnds);
                this.text_chong.text = "活动开始";
                this.activityTime = [];
            }
        };
        UILayout.prototype.redPoint = function () {
            var _this = this;
            rest.user.redPoint(function (data) {
                if (data.status == "success") {
                    if (0 != data.data.didNotread) {
                        _this.img_newsTips.visible = true;
                    }
                    else {
                        _this.img_newsTips.visible = false;
                    }
                    if (0 != data.data.didNotReceive) {
                        _this.img_taskTips.visible = true;
                    }
                    else {
                        _this.img_taskTips.visible = false;
                    }
                }
            });
        };
        UILayout.prototype.notic = function () {
            var _this = this;
            //通知滚动
            rest.notice.list(function (data) {
                if (data.data !== null) {
                    _this.loadNotice();
                }
            });
        };
        UILayout.prototype.onStep = function (num) {
            UILayout.stepNum = num;
            rest.user.moveGuidingSteps({ step: num }, function (data) {
            });
        };
        UILayout.prototype.updateUserInfo = function () {
            console.log(Rest.USERINFO);
            if (Rest.USERINFO && Rest.USERINFO.id > 0) {
                //用户信息
                this.lab_nickname.text = Rest.USERINFO.nickName || '';
                this.dbexNum.setNum((Rest.USERINFO.baseBbm + Rest.USERINFO.starterBbm).toFixed(4) || '0');
                //this.lab_bbm.text = (Rest.USERINFO.baseBbm + Rest.USERINFO.starterBbm).toFixed(4) || '0';
                //this.lab_quantity.text = Rest.USERINFO.bbmCalcPoint + Rest.USERINFO.marketCalcPoint + Rest.USERINFO.newsCalcPoint || '0';
                this.powerNum.setNum(Rest.USERINFO.bbmCalcPoint + Rest.USERINFO.marketCalcPoint + Rest.USERINFO.newsCalcPoint);
                this.commonlyUsed.avatar(Rest.USERINFO.headIcon, this.btn_icon);
                console.log(Rest.GALAXY.galaxyName);
                if (Rest.GALAXY.galaxyName != null) {
                    this.user_galaxy.text = Rest.GALAXY.galaxyName;
                }
                switch (Rest.GALAXY.rankLevel) {
                    case 1:
                        this.user_galaxyImg.skin = "changeGalaxy/xingqiu_high.png";
                        break;
                    case 2:
                        this.user_galaxyImg.skin = "changeGalaxy/xingqiu_mid.png";
                        break;
                    case 3:
                        this.user_galaxyImg.skin = "changeGalaxy/xingqiu_low.png";
                        break;
                }
            }
        };
        UILayout.prototype.userTree = function () {
            //浇水按钮
            if (Rest.USERINFO.upgradeToNextStatus == 1) {
                this.btn_receive.visible = true;
                this.btn_water.visible = false;
                this.text_tips.visible = false;
                // this.ani_proBar.visible = true;
                // this.ani_proBar.play(0, true, "ani1");
            }
            else {
                if (Rest.USERINFO.selfWateringTimes == 1) {
                    this.btn_water.visible = false;
                    this.text_tips.visible = true;
                }
                else {
                    this.btn_water.visible = true;
                    this.text_tips.visible = false;
                }
                // this.ani_proBar.visible = false;
                // this.ani_proBar.stop();
                this.btn_receive.visible = false;
            }
            if (Rest.USERINFO.yqsCurrentExps >= Rest.USERINFO.yqsNextGradeExps) {
                this.btn_receive.visible = true;
            }
            //楼的等级
            this.text_lv.text = "LV:" + Rest.USERINFO.yqsGrade;
            this.text_lv.stroke = 3;
            this.text_lv.strokeColor = "#005a62";
            this.text_probText.text = Rest.USERINFO.yqsCurrentExps + "/" + Rest.USERINFO.yqsNextGradeExps;
            this.proBar_TreeLV.value = Rest.USERINFO.yqsCurrentExps / Rest.USERINFO.yqsNextGradeExps;
        };
        // 点击头像按钮
        UILayout.prototype.onClickIcon = function () {
            this.event('Sounds', 1);
            if (this.MyPage == null) {
                this.MyPage = new view.MyPage(this.game);
            }
            this.MyPage.popup(true, true);
        };
        //点击钱包
        UILayout.prototype.onClickWallet = function () {
            if (9 == UILayout.stepNum) {
                this.guid.visible = true;
                UILayout.stepNum = 10;
                this.onStep(10);
            }
            if (this.guid) {
                this.guid.removeSelf();
            }
            this.event('Sounds', 1);
            //Browser.window.open("http://www.bbb-home.com/appDownload.html", '_blank');
            //Browser.window.location.href = "http://www.bbb-home.com/appDownload.html";
            if (this.walletPage == null) {
                this.walletPage = new view.WalletPage();
            }
            this.walletPage.popup(true, true);
        };
        // 点击放快捷方式到桌面按钮
        UILayout.prototype.onClickDesktop = function () {
            this.event('Sounds', 1);
            if (this.desktopPage == null) {
                this.desktopPage = new view.DesktopPage();
            }
            this.desktopPage.popup(true, true);
        };
        //点击实名
        UILayout.prototype.onClickRealName = function () {
            this.event('Sounds', 1);
            if (this.RealName == null) {
                this.RealName = new view.RealName();
            }
            this.RealName.on("HIDE", this, this.realNameHidden);
            this.addChild(this.RealName);
        };
        UILayout.prototype.realNameHidden = function () {
            this.btn_realName.visible = false;
            this.signIn.pos(Laya.stage.width / 2, Laya.stage.height / 2);
            this.signIn.play(0, false, "ani1");
            this.addChild(this.signIn);
            //this.lab_quantity.text = Rest.USERINFO.bbmCalcPoint + Rest.USERINFO.marketCalcPoint + Rest.USERINFO.newsCalcPoint;
            //this.powerNum.setNum(Rest.USERINFO.bbmCalcPoint + Rest.USERINFO.marketCalcPoint + Rest.USERINFO.newsCalcPoint);
        };
        //点击分享
        UILayout.prototype.onClickShareIt = function () {
            if (UILayout.stepNum == 8) { //Rest.USERINFO.guidMoveSteps
                this.game.first = true;
                UILayout.stepNum = 9;
                this.onStep(9);
                this.guid.pos(this.sp_wallet.x + (this.sp_wallet.width >> 1) + 15, this.sp_wallet.y - (this.sp_wallet.height * 0.7));
            }
            this.event('Sounds', 1);
            if (this.shareItPage == null) {
                this.shareItPage = new view.ShareItPage(this.game);
            }
            this.shareItPage.popup(true, true);
        };
        // 点击新闻按钮
        UILayout.prototype.onClickNews = function () {
            this.event('Sounds', 1);
            if (this.newsPage == null) {
                this.newsPage = new view.NewsPage(this.game);
            }
            this.newsPage.on("redPoint", this, this.redPoint);
            this.newsPage.popup(true, true);
        };
        //点击楼
        UILayout.prototype.onClickTree = function () {
            this.event('Sounds', 1);
            if (this.treePage == null) {
                this.treePage = new view.TreePage(this.game);
            }
            this.treePage.popup(true, true);
        };
        //区世界公众号
        UILayout.prototype.onQXJ = function () {
            this.event('Sounds', 1);
            if (this.QSJPage == null) {
                this.QSJPage = new view.QSJPage(this.game);
            }
            this.QSJPage.popup(true, true);
        };
        //问答
        UILayout.prototype.onAnswer = function () {
            this.event('Sounds', 1);
            if (this.answerPage == null) {
                this.answerPage = new view.AnswerPage(this.game);
            }
            //this.answerPage.viewStack.selectedIndex = 0;
            this.answerPage.popup(true, true);
        };
        //冲顶大会
        UILayout.prototype.onChongDing = function () {
            this.event('Sounds', 1);
            if (this.chongPage == null) {
                this.chongPage = new view.ChongPage(this.game);
            }
            this.chongPage.popup(true, true);
        };
        //抽奖
        UILayout.prototype.onDrawPrize = function () {
            this.event('Sounds', 1);
            if (this.drawPage == null) {
                this.drawPage = new view.DrawPage(this.game);
            }
            this.drawPage.popup(true, true);
        };
        //连接跳转
        UILayout.prototype.onSkipHandler = function () {
            // Browser.window.location.href = "http://world.game.bcrealm.com";
            var name = "Token";
            var token = localStorage.getItem("Token");
            Laya.Browser.window.setCookie(name, token);
            Browser.window.location.href = "http://world.game.bcrealm.com";
        };
        // 点击浇水按钮
        UILayout.prototype.onClickWater = function () {
            var _this = this;
            if (UILayout.stepNum == 3) { //Rest.USERINFO.guidMoveSteps
                this.guid.pos(this.btn_receive.x + (this.btn_receive.width >> 1), this.btn_receive.y - this.btn_receive.height * 1.5);
                UILayout.stepNum = 4;
                this.onStep(4);
                console.log(Rest.USERINFO);
            }
            rest.work.wateringTree(function (data) {
                if (data.status == "error") {
                    _this.tips = 3;
                    _this.showErrorTips(_this.img_msg, _this.text_msg, data.msg);
                    Laya.timer.loop(1000, _this, _this.tipsLoop);
                }
                else {
                    Rest.USERINFO = data.data;
                    _this.userTree();
                }
            });
        };
        //提示
        UILayout.prototype.showErrorTips = function (img, tips, msg) {
            img.visible = true;
            tips.text = msg;
            img.width = msg.length * 30;
            tips.width = msg.length * 30;
            img.centerX = 1;
        };
        UILayout.prototype.tipsLoop = function () {
            this.tips--;
            if (this.tips <= 0) {
                this.img_msg.visible = false;
                Laya.timer.clear(this, this.tipsLoop);
            }
        };
        //点击任务
        UILayout.prototype.onClickTask = function () {
            if (5 == UILayout.stepNum) { //Rest.USERINFO.guidMoveSteps
                UILayout.stepNum = 6;
                this.guid.visible = false;
                this.onStep(6);
            }
            this.event('Sounds', 1);
            if (this.taskPage == null) {
                this.taskPage = new view.TaskPage(this.game);
            }
            this.taskPage.on("redPoint", this, this.redPoint);
            this.taskPage.popup(true, true);
        };
        //点击排行榜
        UILayout.prototype.onClickLeaderboard = function () {
            this.event('Sounds', 1);
            if (this.leaderBoardPage == null) {
                this.leaderBoardPage = new view.LeaderboardPage3(this.game);
            }
            this.leaderBoardPage.popup(true, true);
        };
        // 点击好友按钮
        UILayout.prototype.onClickFriend = function () {
            this.event('Sounds', 1);
            if (this.friendPage == null) {
                this.friendPage = new view.FriendPage(this.game);
            }
            this.friendPage.popup(true, true);
        };
        //点击领取树升级奖励
        UILayout.prototype.onClickReceive = function () {
            var _this = this;
            rest.work.upgradeTree(function (data) {
                _this.event('Sounds', 2);
                Rest.USERINFO = data.data;
                _this.addChild(_this.gold);
                _this.gold.pos(((Laya.stage.width - _this.round.width) >> 1) + 77, ((Laya.stage.height - _this.round.height) >> 1) - 200);
                _this.gold.play(0, false, "ani1");
                var newBBM = Rest.USERINFO.baseBbm + Rest.USERINFO.starterBbm;
                _this.userTree();
                var once = true;
                _this.PlayTreeAni(once);
                _this.updateUserInfo();
            });
            if (UILayout.stepNum == 4) { //Rest.USERINFO.guidMoveSteps
                this.guid.pos(this.btn_task.x + (this.btn_task.width >> 1), this.btn_task.y - this.btn_task.height);
                UILayout.stepNum = 5;
                this.onStep(5);
            }
        };
        // 动画加载完成
        UILayout.prototype.onLoaded = function () {
            this.js++;
            this.upAni.on(Laya.Event.COMPLETE, this, this.AniComplete);
            // this.round = this.ani_tree.getBounds();
            this.ani_tree.on(Laya.Event.COMPLETE, this, this.changSource);
            this.guid.on(Laya.Event.COMPLETE, this, null);
            this.signIn.on(Laya.Event.COMPLETE, this, this.sign);
            this.gold.on(Laya.Event.COMPLETE, this, null);
            this.changBuildAni.on(Laya.Event.COMPLETE, this, this.louPlay);
            this.round = this.gold.getBounds();
            if (this.js == 5) { //== 4
                this.updateUserInfo();
                this.userTree();
                this.PlayTreeAni();
                this.bbmShow();
            }
        };
        UILayout.prototype.changSource = function () {
            switch (Rest.USERINFO.yqsGrade % 5) {
                case 1:
                    this.ani_tree.source = "lou.ani";
                    this.ani_tree.play(0, true, "ani1");
                    break;
                case 0:
                    this.ani_tree.visible = false;
                    break;
            }
        };
        UILayout.prototype.sign = function () {
            this.signIn.visible = false;
            this.removeChild(this.signIn);
        };
        UILayout.prototype.louPlay = function () {
            this.ani_tree.visible = true;
            this.ani_tree.play(0, true, "ani1");
            this.removeChild(this.changBuildAni);
        };
        UILayout.prototype.onQSJLoaded = function () {
            if (this.QSJPage == null) {
                this.QSJPage = new view.QSJPage(this.game);
            }
            this.QSJPage.popup(true, true);
        };
        UILayout.prototype.onComplete = function () {
            // console.log("播放完成");
        };
        // 5的倍数级动画播放完毕回调
        UILayout.prototype.AniComplete = function () {
            this.upAni.stop();
            this.removeChild(this.upAni);
            this.userTree();
        };
        //升级动画播放完回调
        UILayout.prototype.TreeAniComplete = function () {
            this.ani_tree.stop();
            Laya.timer.loop(5000, this, this.PlayTreeAni);
        };
        //树每个状态下的动画
        UILayout.prototype.PlayTreeAni = function (once) {
            var point = this.ani_tree.localToGlobal(new Laya.Point(0, 0));
            // switch (Rest.USERINFO.yqsGrade) {
            // 	case 1:
            // 		// this.addChild(this.changBuildAni);
            // 		// this.changBuildAni.zOrder = -1;
            // 		// this.changBuildAni.pos(point.x , point.y);
            // 		// this.changBuildAni.play(0, false, "ani1");
            // 		this.ani_tree.source = "huanjianzhu.ani";
            // 		this.ani_tree.visible = true;
            // 		this.ani_tree.play(0, false, "ani1");
            // 		this.buildingImg.alpha = 0.2;
            // 		this.buildingImg.skin = "floor/01.png";
            // 		break;
            // 	case 2:
            // 		// this.ani_tree.visible = false;
            // 		// this.addChild(this.upAni);
            // 		// this.upAni.zOrder = -1;
            // 		// this.upAni.pos((Laya.stage.width  >> 1) - 3 , ((Laya.stage.height - this.round.height) >> 1) + 84);
            // 		// // this.upAni.pos(point.x , point.y);
            // 		// this.upAni.play(0, false, "ani1");
            // 		this.ani_tree.source = "shengji.ani";
            // 		this.ani_tree.visible = true;
            // 		// this.ani_tree.x = point.x + 1;
            // 		this.ani_tree.play(0, false, "ani1");
            // 		this.buildingImg.alpha = 1;
            // 		this.buildingImg.skin = "floor/01.png";
            // 	case 3:
            // 	case 4:
            // 	case 5:
            // 		break;
            // }
            if (Rest.USERINFO.yqsGrade <= 5) {
                this.buildingImg.skin = "floor/01.png";
            }
            if (5 < Rest.USERINFO.yqsGrade && Rest.USERINFO.yqsGrade <= 10) {
                this.buildingImg.skin = "floor/02.png";
            }
            if (10 < Rest.USERINFO.yqsGrade && Rest.USERINFO.yqsGrade <= 15) {
                this.buildingImg.skin = "floor/03.png";
            }
            if (15 < Rest.USERINFO.yqsGrade && Rest.USERINFO.yqsGrade <= 20) {
                this.buildingImg.skin = "floor/04.png";
            }
            if (20 < Rest.USERINFO.yqsGrade && Rest.USERINFO.yqsGrade <= 25) {
                this.buildingImg.skin = "floor/05.png";
            }
            if (25 < Rest.USERINFO.yqsGrade && Rest.USERINFO.yqsGrade <= 30) {
                this.buildingImg.skin = "floor/06.png";
            }
            if (30 < Rest.USERINFO.yqsGrade && Rest.USERINFO.yqsGrade <= 35) {
                this.buildingImg.skin = "floor/07.png";
            }
            if (35 < Rest.USERINFO.yqsGrade && Rest.USERINFO.yqsGrade <= 40) {
                this.buildingImg.skin = "floor/08.png";
            }
            if (40 < Rest.USERINFO.yqsGrade && Rest.USERINFO.yqsGrade <= 45) {
                this.buildingImg.skin = "floor/09.png";
            }
            if (45 < Rest.USERINFO.yqsGrade && Rest.USERINFO.yqsGrade <= 50) {
                this.buildingImg.skin = "floor/10.png";
            }
            switch (Rest.USERINFO.yqsGrade % 5) {
                case 1:
                    if (once) {
                        this.ani_tree.source = "huanjianzhu.ani";
                        this.ani_tree.visible = true;
                        this.ani_tree.play(0, false, "ani1");
                    }
                    else {
                        this.ani_tree.visible = true;
                        this.ani_tree.play(0, true, "ani1");
                    }
                    this.buildingImg.alpha = 0.2;
                    break;
                case 2:
                    this.ani_tree.visible = true;
                    this.ani_tree.play(0, true, "ani1");
                    this.buildingImg.alpha = 0.4;
                    break;
                case 3:
                    this.ani_tree.visible = true;
                    this.ani_tree.play(0, true, "ani1");
                    this.buildingImg.alpha = 0.6;
                    break;
                case 4:
                    this.ani_tree.visible = true;
                    this.ani_tree.play(0, true, "ani1");
                    this.buildingImg.alpha = 0.8;
                    break;
                case 0:
                    if (once) {
                        this.ani_tree.source = "shengji.ani";
                        this.ani_tree.visible = true;
                        this.ani_tree.play(0, false, "ani1");
                        this.event('Sounds', 5);
                    }
                    else {
                        this.ani_tree.visible = false;
                    }
                    this.buildingImg.alpha = 1;
                    break;
            }
        };
        UILayout.prototype.bbmShow = function () {
            this.creatElement();
        };
        /**
         *
         * 创建coins
         * */
        UILayout.prototype.creatElement = function () {
            /**最多显示10个*/
            var leg = Math.min(Rest.COINSLIST.length, 10);
            for (var i = 0; i < leg; i++) {
                var obj = Rest.COINSLIST[i];
                var ele = new view.JinbiElement(this["element" + i]);
                ele.info = obj;
            }
        };
        UILayout.prototype.updateBBM = function () {
            // var leg = Rest.COINSLIST.length;
            // let showNum = Rest.USERINFO.pickableCoins - this.box_bbm._childs.length;
            // let showNum = leg - this.box_bbm._childs.length;
            // if (showNum > 0) {
            // 	bbm.visible = true;
            // 	let singleGold = bbm.getChildByName("jb") as Laya.Animation;
            // 	singleGold.play(0, true, "jinbi");
            this.game.first = true;
            // }
        };
        UILayout.prototype.onbbm1 = function () {
            if (UILayout.stepNum == 0) { //Rest.USERINFO.guidMoveSteps
                this.guid.pos(this.element2.x + (this.element2.width * 0.5), this.element2.y - 40);
                UILayout.stepNum = 1;
                if (!this.game.first) {
                    this.onStep(1);
                }
            }
        };
        UILayout.prototype.onbbm2 = function () {
            if (UILayout.stepNum == 1) { //Rest.USERINFO.guidMoveSteps
                this.guid.pos(this.element3.x + (this.element3.width * 0.5), this.element3.y - 40);
                UILayout.stepNum = 2;
                if (!this.game.first) {
                    this.onStep(2);
                }
            }
        };
        UILayout.prototype.onbbm3 = function () {
            if (UILayout.stepNum == 2) { //Rest.USERINFO.guidMoveSteps
                this.guid.pos(this.btn_water.x + (this.btn_water.width >> 1), this.btn_water.y - this.btn_water.height * 1.5);
                UILayout.stepNum = 3;
                if (!this.game.first) {
                    this.onStep(3);
                }
            }
        };
        //滚动通知
        UILayout.prototype.loadNotice = function () {
            var _this = this;
            rest.notice.list(function (data) {
                if (!data.data || !(data.data instanceof Array)) {
                    return;
                }
                if (data.data.length == 0) {
                    return;
                }
                var i = 0;
                var w = 470;
                for (var _i = 0, _a = data.data; _i < _a.length; _i++) {
                    var d = _a[_i];
                    //let lab = new Laya.Label(`${++i} ${d.content || ''}     `)
                    var lab = new Laya.Label((d.content || '') + "     ");
                    lab.color = '#54d1e8';
                    lab.fontSize = 24;
                    lab.x = w;
                    lab.y = 4;
                    _this.notices.push(lab);
                    _this.sp_notice.addChild(lab);
                }
                Laya.timer.loop(0.000001, _this, _this.rollNotice);
                //Laya.timer.loop(1, this, this.rollNotice);
            });
        };
        UILayout.prototype.timeOut = function () {
            this.out--;
            if (this.out == 0) {
                this.timer.clear(this, this.timeOut);
                Laya.timer.loop(0.000001, this, this.rollNotice);
                //Laya.timer.loop(1, this, this.rollNotice);
            }
        };
        UILayout.prototype.rollNotice = function () {
            if (this.notices.length == 0) {
                return;
            }
            this.notices[0].x -= 2;
            // let string = this.notices[0]._tf.text;
            // if (this.notices[0]._tf.text.replace(/[^0-9]/ig, "")) {
            // 	var s = this.notices[0]._tf.text.replace(/[^0-9]/ig, "");
            // 	s.fontcolor = "#c32523";
            // 	var reg = new RegExp("(" + s + ")", "ig");
            // 	// var str = "停车坐爱枫亭晚";
            // 	var newstr = this.notices[0]._tf.text.replace(reg, "<span style='color: red;'>$&</span>");
            // 	// this.notices[0].color = "#c32523";
            // 	// document.write(newstr + "<br />");
            // 	this.notices[0]._tf.text = newstr;
            // 	this.notices[0]._tf.text.color = "#c32523";
            // 	console.log("");
            // }
            var eleX = (parseInt((470 - this.notices[0].width) / 2 + ''));
            if (this.notices[0].x == (eleX % 2 == 0 ? eleX : eleX + 1)) {
                this.out = 5;
                Laya.timer.clear(this, this.rollNotice);
                this.timerLoop(1000, this, this.timeOut);
            }
            if (this.notices[0].x < -470) {
                this.notices[0].x = 470;
                var ele = this.notices.shift();
                this.notices.push(ele);
            }
        };
        return UILayout;
    }(ui.UILayoutUI));
    view.UILayout = UILayout;
})(view || (view = {}));
//# sourceMappingURL=UILayout.js.map