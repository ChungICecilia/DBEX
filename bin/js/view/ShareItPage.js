/**Created by the LayaAirIDE*/
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var view;
(function (view) {
    var Browser = Laya.Browser;
    var ShareItPage = /** @class */ (function (_super) {
        __extends(ShareItPage, _super);
        function ShareItPage(game) {
            var _this = _super.call(this) || this;
            _this.shareLink = '';
            _this.game = game;
            var strings = Browser.window.location.href.split("?");
            _this.shareLink = strings[0] + '?userCode=' + Rest.INVITECODE;
            _this.inviteHref = Laya.Browser.window.document.getElementById("bar");
            _this.copyDiv = Laya.Browser.window.document.getElementById("copy_btn");
            _this.btn_close.on(Laya.Event.CLICK, _this, _this.onClickClose);
            return _this;
        }
        ShareItPage.prototype.onClickClose = function () {
            this.hiddenCopyBtn();
            this.close();
        };
        //<div style=" background: rgba(255, 255, 255, 0); position: absolute; bottom:26%; text-align: center;  width: 100%; visibility: hidden"id="copy_div">
        ShareItPage.prototype.onOpened = function () {
            //  Rest.OPTION
            this.html_tips.style.lineHeight = 24;
            this.html_tips.style.align = "center";
            this.html_tips.innerHTML =
                "<font  style='fontSize:26' color='#333333'>" + '可分享给好友,好友成功注册增加算力' +
                    "<font style='fontSize:30' color='red'>" + '5' +
                    "<font style='fontSize:26' color='#333333'>" + '点,' +
                    "<font style='fontSize:30' color='red'>" + '1' +
                    "<font style='fontSize:26' color='#333333'>" + '张复活卷,' +
                    "<font style='fontSize:30' color='red'>" + '2' +
                    "<font style='fontSize:26' color='#333333'>" + '积分!' +
                    "<br/></font>" + "<br/></font>" + "<br/></font>" + "<br/></font>" + "<br/></font>" + "<br/></font>" + "<br/></font> ";
            this.showCopyBtn();
            console.log(Rest.INVITECODE);
        };
        ShareItPage.prototype.hiddenCopyBtn = function () {
            if (this.inviteHref && this.copyDiv) {
                this.copyDiv.style.visibility = "hidden";
                this.inviteHref.style.visibility = "hidden";
            }
        };
        ShareItPage.prototype.showCopyBtn = function () {
            if (this.inviteHref && this.copyDiv) {
                this.copyDiv.style.visibility = "visible";
                this.inviteHref.style.visibility = "visible";
                this.inviteHref.innerHTML = this.shareLink;
                console.log(this.inviteHref.text);
            }
        };
        return ShareItPage;
    }(ui.ShareItPageUI));
    view.ShareItPage = ShareItPage;
})(view || (view = {}));
//# sourceMappingURL=ShareItPage.js.map