var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var view;
(function (view) {
    var WalletPage = /** @class */ (function (_super) {
        __extends(WalletPage, _super);
        function WalletPage() {
            var _this = _super.call(this) || this;
            _this.btn_close.on(Laya.Event.CLICK, _this, _this.onClickClose);
            _this.btn_wallet.on(Laya.Event.CLICK, _this, _this.onClickWallet);
            return _this;
        }
        WalletPage.prototype.onClickClose = function () {
            this.text_bbm.text = '';
            this.text_rmb.text = '';
            this.close();
        };
        WalletPage.prototype.onOpened = function () {
            console.log(Rest.OPTION.estimated_value_ratio);
            var num = Rest.USERINFO.baseBbm + Rest.USERINFO.starterBbm;
            console.log(num);
            this.text_bbm.text = "持有DBEX:" + num.toFixed(4) || "持有DBEX：0";
            // Rest.OPTION.  transform_rate
            var y = num * Rest.OPTION.estimated_value_ratio + '';
            if ("NaN" == y) {
                this.text_rmb.text = "预估值0元";
            }
            else {
                this.text_rmb.text = "预估价值:" + (num * Rest.OPTION.estimated_value_ratio).toFixed(4) + "元";
            }
        };
        WalletPage.prototype.onClickWallet = function () {
            // Browser.window.location.href = "http://www.bbb-home.com/appDownload.html";
            this.showErrorTips("迪拜交易所正在搭建中，敬请期待哦！");
        };
        WalletPage.prototype.showErrorTips = function (msg) {
            this.tips = 3;
            this.img_createPlayer.visible = true;
            this.img_createPlayer.width = msg.length * 30;
            this.text_createPlayer.width = msg.length * 30;
            this.img_createPlayer.centerX = 1;
            this.text_createPlayer.text = msg;
            Laya.timer.loop(1000, this, this.tipsLoop);
        };
        WalletPage.prototype.tipsLoop = function () {
            this.tips--;
            if (this.tips <= 0) {
                if (this.disabled) {
                    this.disabled = false;
                }
                this.img_createPlayer.visible = false;
                Laya.timer.clear(this, this.tipsLoop);
            }
        };
        return WalletPage;
    }(ui.WalletPageUI));
    view.WalletPage = WalletPage;
})(view || (view = {}));
//# sourceMappingURL=WalletPage.js.map