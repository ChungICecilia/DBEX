var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**Created by the LayaAirIDE*/
var view;
(function (view) {
    var RegistPage = /** @class */ (function (_super) {
        __extends(RegistPage, _super);
        function RegistPage(phoneNum, verCode, usercode) {
            var _this = _super.call(this) || this;
            _this.phoneNum = phoneNum;
            _this.verCode = verCode;
            _this.usercode = usercode;
            _this.registBtn.on(Laya.Event.CLICK, _this, _this.regist);
            _this.loginClick.on(Laya.Event.CLICK, _this, _this.onClickClose);
            _this.text_text.text = "注册";
            _this.loginClick.visible = true;
            return _this;
        }
        RegistPage.prototype.regist = function () {
            var _this = this;
            if (this.psw_again.text == this.pswText.text) {
                var phone = this.phoneNum;
                var passWord = this.psw_again.text;
                var code = this.verCode;
                var userCode = this.usercode;
                var userId = null;
                Http.DATA_TYPE = 'JSON';
                rest.user.register({ phone: phone, passWord: passWord, code: code, userCode: userCode, userId: userId }, (function (data) {
                    if (data.status == "error") {
                        _this.tips = 3;
                        _this.img_singn.visible = true;
                        _this.img_singn.width = data.msg.length * 30;
                        _this.img_singn.centerX = 1;
                        _this.text_tips.width = data.msg.length * 30;
                        _this.text_tips.text = data.msg;
                        Laya.timer.loop(1000, _this, _this.tipsLoop);
                    }
                    else {
                        _this.onClickClose();
                    }
                    Http.DATA_TYPE = 'FORMDATA';
                }));
            }
            else {
                this.tips = 3;
                this.img_singn.visible = true;
                this.text_tips.text = "请输入相同的密码";
                this.text_tips.width = 330;
                this.img_singn.width = 330;
                this.img_singn.centerX = 1;
                Laya.timer.loop(1000, this, this.tipsLoop);
            }
        };
        RegistPage.prototype.onClickClose = function () {
            if (this.parent != null) {
                this.event('CLOSE');
                this.parent.removeChild(this);
                this.img_singn.visible = false;
                Laya.timer.clear(this, this.tipsLoop);
            }
        };
        RegistPage.prototype.tipsLoop = function () {
            this.tips--;
            if (this.tips <= 0) {
                this.img_singn.visible = false;
                Laya.timer.clear(this, this.tipsLoop);
            }
        };
        return RegistPage;
    }(ui.RegistPageUI));
    view.RegistPage = RegistPage;
})(view || (view = {}));
//# sourceMappingURL=RegistPage.js.map