var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
* name s
*/
var view;
(function (view) {
    var ForgetPasswordPages = /** @class */ (function (_super) {
        __extends(ForgetPasswordPages, _super);
        function ForgetPasswordPages() {
            var _this = _super.call(this) || this;
            _this.mobile.text = "";
            _this.code.text = "";
            _this.num = 60;
            _this.tips = 0;
            _this.commonlyUsed = new CommonlyUsed;
            _this.submitBtn.on(Laya.Event.CLICK, _this, _this.onSubmit);
            _this.lab_num.on(Laya.Event.CLICK, _this, _this.oncode);
            _this.img_singn.on(Laya.Event.CLICK, _this, _this.onClosePrompt);
            _this.btn_back.visible = false;
            return _this;
        }
        ForgetPasswordPages.prototype.onBack = function () {
            this.removeSelf();
        };
        ForgetPasswordPages.prototype.onClosePrompt = function () {
            this.img_singn.visible = false;
            Laya.timer.clear(this, this.tipsLoop);
        };
        ForgetPasswordPages.prototype.oncode = function () {
            var _this = this;
            if (this.mobile.text != "" && this.mobile.text.length == 11) {
                this.num = 60;
                this.lab_num.text = "重新发送" + this.num + "秒";
                var phone = this.mobile.text;
                var type = 1;
                Http.DATA_TYPE = 'JSON';
                rest.user.getMSBack({ phone: phone, type: type }, (function (data) {
                    if (data.status == "error") {
                        _this.tips = 3;
                        _this.img_singn.visible = true;
                        _this.text_tips.width = data.msg.length * 30;
                        _this.img_singn.width = data.msg.length * 30;
                        _this.img_singn.centerX = 1;
                        _this.text_tips.text = data.msg;
                        Laya.timer.loop(1000, _this, _this.tipsLoop);
                    }
                    Http.DATA_TYPE = 'FORMDATA';
                }));
                this.lab_num.visible = true;
                this.submitBtn.disabled = false;
                Laya.timer.loop(1000, this, this.onLoop);
            }
            else {
                this.tips = 3;
                this.img_singn.visible = true;
                this.text_tips.text = "请检查您的手机号码!";
                this.text_tips.width = 330;
                this.img_singn.width = 330;
                this.img_singn.centerX = 1;
                Laya.timer.loop(1000, this, this.tipsLoop);
            }
        };
        ForgetPasswordPages.prototype.onSubmit = function () {
            var _this = this;
            var phone = this.mobile.text || '';
            var userCode = this.commonlyUsed.UrlSearch();
            var code = this.code.text || '';
            if (this.mobile.text == "" || this.mobile.text.length != 11 || this.code.text == "") {
                this.submitBtn.disabled = true;
                this.tips = 2;
                this.img_singn.visible = true;
                this.text_tips.text = "请输入正确的手机号码和验证码!";
                this.text_tips.width = 400;
                this.img_singn.width = 400;
                this.img_singn.centerX = 1;
                Laya.timer.loop(1000, this, this.tipsLoop);
            }
            else {
                Http.DATA_TYPE = 'JSON';
                rest.user.sMsg({ code: code, phone: phone }, (function (data) {
                    if (data.code == 1) {
                        _this.confirmChangePasswordPage = new view.ConfirmChangePasswordPage(phone, code, userCode);
                        _this.confirmChangePasswordPage.on("CLOSE", _this, _this.onClickClose);
                        Laya.stage.addChild(_this.confirmChangePasswordPage);
                    }
                    else {
                        _this.submitBtn.disabled = true;
                        _this.tips = 2;
                        _this.img_singn.visible = true;
                        _this.text_tips.text = data.msg;
                        _this.text_tips.width = data.msg.length * 36;
                        _this.img_singn.width = data.msg.length * 36;
                        _this.img_singn.centerX = 1;
                        Laya.timer.loop(1000, _this, _this.tipsLoop);
                    }
                    Http.DATA_TYPE = 'FORMDATA';
                }));
            }
        };
        ForgetPasswordPages.prototype.onClickClose = function () {
            if (this.parent != null) {
                this.parent.removeChild(this);
                this.img_singn.visible = false;
                Laya.timer.clear(this, this.tipsLoop);
            }
        };
        ForgetPasswordPages.prototype.onLoop = function () {
            this.num--;
            this.lab_num.text = "重新发送" + this.num + "秒";
            if (this.num <= 0) {
                this.lab_num.text = "获取验证码";
                this.lab_num.align = "center";
                Laya.timer.clear(this, this.onLoop);
            }
        };
        ForgetPasswordPages.prototype.tipsLoop = function () {
            this.num = 0;
            this.tips--;
            if (this.tips <= 0) {
                this.img_singn.visible = false;
                if (this.submitBtn.disabled) {
                    this.submitBtn.disabled = false;
                }
                Laya.timer.clear(this, this.tipsLoop);
            }
        };
        return ForgetPasswordPages;
    }(ui.SignUpPageUI));
    view.ForgetPasswordPages = ForgetPasswordPages;
})(view || (view = {}));
//# sourceMappingURL=ForgetPasswordPage.js.map