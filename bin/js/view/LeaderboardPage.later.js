// /**Created by the LayaAirIDE*/
// module view {
// 	import Loader = laya.net.Loader;
// 	import Handler = laya.utils.Handler;
// 	import Button = laya.ui.Button;
// 	import Label = laya.ui.Label;
// 	import Event = laya.events.Event;
// 	import CheckBox = laya.ui.CheckBox;
// 	import Box = laya.ui.Box;
// 	export class LeaderboardPage extends ui.LeaderboardPageUI {
// 		private commonlyUsed: CommonlyUsed;
// 		private dailyData = [
// 			{ playerHeadIcon: "1", playerNickName: "qe", playerBbmCalcPoint: 234, playerRank: 1 },
// 			{ playerHeadIcon: "2", playerNickName: "qe", playerBbmCalcPoint: 234, playerRank: 2 },
// 			{ playerHeadIcon: "3", playerNickName: "qe", playerBbmCalcPoint: 234, playerRank: 3 },
// 			{ playerHeadIcon: "4", playerNickName: "qe", playerBbmCalcPoint: 234, playerRank: 4 },
// 		];
// 		private weeklyData = [
// 			{ playerHeadIcon: "1", playerNickName: "周", playerBbmCalcPoint: 234, playerRank: 1 },
// 			{ playerHeadIcon: "2", playerNickName: "周", playerBbmCalcPoint: 234, playerRank: 2 },
// 			{ playerHeadIcon: "3", playerNickName: "周", playerBbmCalcPoint: 234, playerRank: 3 },
// 			{ playerHeadIcon: "4", playerNickName: "周", playerBbmCalcPoint: 234, playerRank: 4 },
// 		];
// 		private monthData = [
// 			{ playerHeadIcon: "1", playerNickName: "月", playerBbmCalcPoint: 234, playerRank: 1 },
// 			{ playerHeadIcon: "2", playerNickName: "月", playerBbmCalcPoint: 234, playerRank: 2 },
// 			{ playerHeadIcon: "3", playerNickName: "月", playerBbmCalcPoint: 234, playerRank: 3 },
// 			{ playerHeadIcon: "4", playerNickName: "月", playerBbmCalcPoint: 234, playerRank: 4 },
// 		];
// 		private dailyListData = [];
// 		private dailyTop = [];
// 		private dailyPlayerRank = [];
// 		private weeklyListData = [];
// 		private weeklyTop = [];
// 		private weeklyPlayerRank = [];
// 		private monthListData = [];
// 		private monthTop = [];
// 		private monthPlayerRank = [];
// 		private game: GamePage;
// 		constructor(game: GamePage) {
// 			super();
// 			this.game = game;
// 			this.list_leaderBoard.array = [];
// 			this.btn_close.on(Laya.Event.CLICK, this, this.onClickClose);
// 			this.text_day.on(Laya.Event.CLICK, this, this.onDaily);
// 			this.text_week.on(Laya.Event.CLICK, this, this.onWeekly);
// 			this.text_month.on(Laya.Event.CLICK, this, this.onMonth);
// 			this.commonlyUsed = new CommonlyUsed;
// 		}
// 		private onClickClose(): void {
// 			this.close();
// 			this.dailyListData = [];
// 			this.dailyTop = [];
// 			this.dailyPlayerRank = [];
// 			this.weeklyListData = [];
// 			this.weeklyTop = [];
// 			this.weeklyPlayerRank = [];
// 			this.monthListData = [];
// 			this.monthTop = [];
// 			this.monthPlayerRank = [];
// 		}
// 		public onOpened(): void {
// 			// rest.friend.bbmCalcPoint(0, 10, (data) => {
// 			// 	this.dailyData = data.data.topRank;
// 			// 	if (data.data.playerRank == null) {
// 			// 		this.text_myRanking.text = "抱歉您不在排行榜内";
// 			// 	} else {
// 			// 		this.dailyPlayerRank = data.data.playerRank.playerRank;
// 			// 		this.text_myRanking.text = "我的排名:" + this.dailyPlayerRank;
// 			// 	}
// 			// 	for (var i = 0; i < 3; i++) {
// 			// 		this.dailyTop.push(this.dailyData[i]);
// 			// 	};
// 			// 	for (var i = 3; i < this.dailyData.length; i++) {
// 			// 		this.dailyListData.push(this.dailyData[i]);
// 			// 	};
// 			// })
// 			for (var i = 0; i < 3; i++) {
// 				this.dailyTop.push(this.dailyData[i]);
// 			};
// 			for (var i = 3; i < this.dailyData.length; i++) {
// 				this.dailyListData.push(this.dailyData[i]);
// 			};
// 			for (var i = 0; i < 3; i++) {
// 				this.weeklyTop.push(this.weeklyData[i]);
// 			};
// 			for (var i = 3; i < this.weeklyData.length; i++) {
// 				this.weeklyListData.push(this.weeklyData[i]);
// 			};
// 			for (var i = 0; i < 3; i++) {
// 				this.monthTop.push(this.monthData[i]);
// 			};
// 			for (var i = 3; i < this.monthData.length; i++) {
// 				this.monthListData.push(this.monthData[i]);
// 			};
// 			this.onDaily();
// 		}
// 		private onDaily(): void {
// 			this.img_day.visible = true;
// 			this.img_week.visible = false;
// 			this.img_month.visible = false;
// 			if (this.dailyPlayerRank == null) {
// 				this.text_myRanking.text = "抱歉您不在排行榜内";
// 			} else {
// 				this.text_myRanking.text = this.dailyPlayerRank + '';
// 			}
// 			this.commonlyUsed.avatar(this.dailyTop[0].playerHeadIcon, this.img_num1);
// 			this.text_num1Name.text = this.dailyTop[0].playerNickName;
// 			this.text_num1Quantity.text = this.dailyTop[0].playerBbmCalcPoint + '';
// 			this.commonlyUsed.avatar(this.dailyTop[1].playerHeadIcon, this.img_num2);
// 			this.text_num2Name.text = this.dailyTop[1].playerNickName;
// 			this.text_num2Quantity.text = this.dailyTop[1].playerBbmCalcPoint + '';
// 			this.commonlyUsed.avatar(this.dailyTop[2].playerHeadIcon, this.img_num3);
// 			this.text_num3Name.text = this.dailyTop[2].playerNickName;
// 			this.text_num3Quantity.text = this.dailyTop[2].playerBbmCalcPoint + '';
// 			this.list_leaderBoard.array = this.dailyListData;
// 			this.list_leaderBoard.renderHandler = new Laya.Handler(this, this.onRenderDaily);
// 		}
// 		private onWeekly(): void {
// 			this.img_day.visible = false;
// 			this.img_week.visible = true;
// 			this.img_month.visible = false;
// 			if (this.monthPlayerRank == null) {
// 				this.text_myRanking.text = "抱歉您不在排行榜内";
// 			} else {
// 				this.text_myRanking.text = this.monthPlayerRank + '';
// 			}
// 			this.commonlyUsed.avatar(this.weeklyTop[0].playerHeadIcon, this.img_num1);
// 			this.text_num1Name.text = this.weeklyTop[0].playerNickName;
// 			this.text_num1Quantity.text = this.weeklyTop[0].playerBbmCalcPoint + '';
// 			this.commonlyUsed.avatar(this.weeklyTop[1].playerHeadIcon, this.img_num2);
// 			this.text_num2Name.text = this.weeklyTop[1].playerNickName;
// 			this.text_num2Quantity.text = this.weeklyTop[1].playerBbmCalcPoint + '';
// 			this.commonlyUsed.avatar(this.weeklyTop[2].playerHeadIcon, this.img_num3);
// 			this.text_num3Name.text = this.weeklyTop[2].playerNickName;
// 			this.text_num3Quantity.text = this.weeklyTop[2].playerBbmCalcPoint + '';
// 			this.list_leaderBoard.array = this.dailyListData;
// 			this.list_leaderBoard.renderHandler = new Laya.Handler(this, this.onRenderWeekly);
// 		}
// 		private onMonth(): void {
// 			this.img_day.visible = false;
// 			this.img_week.visible = false;
// 			this.img_month.visible = true;
// 			if (this.monthPlayerRank == null) {
// 				this.text_myRanking.text = "抱歉您不在排行榜内";
// 			} else {
// 				this.text_myRanking.text = this.monthPlayerRank + '';
// 			}
// 			this.commonlyUsed.avatar(this.monthTop[0].playerHeadIcon, this.img_num1);
// 			this.text_num1Name.text = this.monthTop[0].playerNickName;
// 			this.text_num1Quantity.text = this.monthTop[0].playerBbmCalcPoint + '';
// 			this.commonlyUsed.avatar(this.monthTop[1].playerHeadIcon, this.img_num2);
// 			this.text_num2Name.text = this.monthTop[1].playerNickName;
// 			this.text_num2Quantity.text = this.monthTop[1].playerBbmCalcPoint + '';
// 			this.commonlyUsed.avatar(this.monthTop[2].playerHeadIcon, this.img_num3);
// 			this.text_num3Name.text = this.monthTop[2].playerNickName;
// 			this.text_num3Quantity.text = this.monthTop[2].playerBbmCalcPoint + '';
// 			this.list_leaderBoard.array = this.monthListData;
// 			this.list_leaderBoard.renderHandler = new Laya.Handler(this, this.onRenderPerMonth);//
// 		}
// 		private onRenderDaily(cell, index): void {
// 			if (index >= this.dailyListData.length) {
// 				return;
// 			}
// 			var text_friendName: Laya.Label = cell.getChildByName("text_friendName") as Laya.Label;
// 			var lab_fame: Laya.Label = cell.getChildByName("lab_fame") as Laya.Label;
// 			var img_avatar: Laya.Image = cell.getChildByName("img_avatar") as Laya.Image;
// 			var text_quantity: Laya.Text = cell.getChildByName("text_quantity") as Laya.Text;
// 			var text_rankNum: Laya.Text = cell.getChildByName("text_rankNum") as Laya.Text;
// 			text_friendName.text = this.dailyListData[index].playerNickName;
// 			text_rankNum.text = this.dailyListData[index].playerRank + '';
// 			this.commonlyUsed.avatar(this.dailyListData[index].playerHeadIcon, img_avatar)
// 			text_quantity.text = this.dailyListData[index].playerBbmCalcPoint + '';
// 		}
// 		private onRenderWeekly(cell, index): void {
// 			if (index >= this.dailyListData.length) {
// 				return;
// 			}
// 			var text_friendName: Laya.Label = cell.getChildByName("text_friendName") as Laya.Label;
// 			var lab_fame: Laya.Label = cell.getChildByName("lab_fame") as Laya.Label;
// 			var img_avatar: Laya.Image = cell.getChildByName("img_avatar") as Laya.Image;
// 			var text_quantity: Laya.Text = cell.getChildByName("text_quantity") as Laya.Text;
// 			var text_rankNum: Laya.Text = cell.getChildByName("text_rankNum") as Laya.Text;
// 			text_friendName.text = this.dailyListData[index].playerNickName;
// 			text_rankNum.text = this.dailyListData[index].playerRank + '';
// 			this.commonlyUsed.avatar(this.dailyListData[index].playerHeadIcon, img_avatar)
// 			text_quantity.text = this.dailyListData[index].playerBbmCalcPoint + '';
// 		}
// 		private onRenderPerMonth(cell, index): void {
// 			if (index >= this.monthListData.length) {
// 				return;
// 			}
// 			var text_friendName: Laya.Label = cell.getChildByName("text_friendName") as Laya.Label;
// 			var lab_fame: Laya.Label = cell.getChildByName("lab_fame") as Laya.Label;
// 			var img_avatar: Laya.Image = cell.getChildByName("img_avatar") as Laya.Image;
// 			var text_quantity: Laya.Text = cell.getChildByName("text_quantity") as Laya.Text;
// 			var text_rankNum: Laya.Text = cell.getChildByName("text_rankNum") as Laya.Text;
// 			text_friendName.text = this.monthListData[index].playerNickName;
// 			text_rankNum.text = this.monthListData[index].playerRank + '';
// 			this.commonlyUsed.avatar(this.monthListData[index].playerHeadIcon, img_avatar)
// 			text_quantity.text = this.monthListData[index].playerBbmCalcPoint + '';
// 		}
// 		private onRender(cell, index, array): void {
// 			if (index >= this.monthListData.length) {
// 				return;
// 			}
// 			var text_friendName: Laya.Label = cell.getChildByName("text_friendName") as Laya.Label;
// 			var lab_fame: Laya.Label = cell.getChildByName("lab_fame") as Laya.Label;
// 			var img_avatar: Laya.Image = cell.getChildByName("img_avatar") as Laya.Image;
// 			var text_quantity: Laya.Text = cell.getChildByName("text_quantity") as Laya.Text;
// 			var text_rankNum: Laya.Text = cell.getChildByName("text_rankNum") as Laya.Text;
// 			text_friendName.text = array[index].playerNickName;
// 			text_rankNum.text = array[index].playerRank + '';
// 			this.commonlyUsed.avatar(array[index].playerHeadIcon, img_avatar)
// 			text_quantity.text = array[index].playerBbmCalcPoint + '';
// 		}
// 		private onMouse(e: Event, index: number): void {
// 			if (e.type == Event.CLICK) {
// 				if ((e.target) instanceof Box) {
// 					//console.log(index)
// 				}
// 			}
// 		}
// 	}
// }
//# sourceMappingURL=LeaderboardPage.later.js.map