var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var view;
(function (view) {
    var AnswerPage = /** @class */ (function (_super) {
        __extends(AnswerPage, _super);
        function AnswerPage(game) {
            var _this = _super.call(this) || this;
            _this.questionId = 0;
            _this.topics = [];
            _this.cur = 0;
            _this.answer = [];
            _this.sn = 0;
            _this.power = 0; //每答对一题所加战力
            _this.submitObj = {};
            _this.curErr = 0;
            _this.curErrTopic = [];
            _this.rewardState = 0;
            _this.susNumber = 0; //答对的题目数
            _this.errorNumber = 0; //答错的题目数
            _this.addForce = 0; //增加的战力
            _this.prizeForce = 0; //每答对一题奖励算力
            _this.rightNum = 0; //答对多少题后
            _this.optionPrompt = new OptionPrompt();
            _this.recTopics = [];
            _this.tinx = 0;
            //  Laya.loader.load(["res/atlas/answer.atlas"],// "res/atlas/game.atlas", 
            //     Laya.Handler.create(this, this.onLoaded), null, Laya.Loader.ATLAS);
            _this.game = game;
            return _this;
        }
        ;
        AnswerPage.prototype.initialize = function () {
            _super.prototype.initialize.call(this);
            this.btn_close.on(Laya.Event.CLICK, this, this.onClickClose);
            this.btn_next.on(Laya.Event.CLICK, this, this.onNextClick);
            this.btn_start.on(Laya.Event.CLICK, this, this.onStartClick);
            this.list_answer.renderHandler = new Laya.Handler(this, this.onListRender);
            this.list_errAnswer.renderHandler = new Laya.Handler(this, this.onErrListRender);
            this.btn_getForce.on(Laya.Event.CLICK, this, this.onForceClick);
            this.btn_check.on(Laya.Event.CLICK, this, this.onCheckClick);
            this.btn_nextErr.on(Laya.Event.CLICK, this, this.onErrNextClick);
            this.list_answer.selectEnable = true;
            this.list_answer.selectHandler = new Laya.Handler(this, this.listSelectHandler);
            this.txt_add.style.bold = true;
            this.txt_correct.style.align = "center";
            //this.list_answer.
        };
        AnswerPage.prototype.onLoaded = function () {
            var i = 3;
        };
        AnswerPage.prototype.onClickClose = function (e) {
            this.close();
        };
        //private curSn:number = 0;
        AnswerPage.prototype.onNextClick = function (e) {
            var _this = this;
            //this.getTopic(this.questionId, this.topics[this.sn]);
            Laya.timer.clear(this, this.countTime);
            view.LoadingPage.loading();
            //this.btn_next.mouseEnabled = false;
            rest.answer.setAnswerTime(this.questionId, this.topics[this.sn], function (data) {
                //console.log(name + "__" + password)
                //console.log(data)
                if (data.status == "error") {
                    // this.ani_loginLoading.clear();
                    // this.showErrorTips(data.msg);
                    _this.optionPrompt.play(data.msg, [Laya.stage.width / 2, Laya.stage.height / 2]);
                }
                else {
                    //this.event('LoginOK');
                    // this.ani_loginLoading.clear();
                    var ans = {};
                    ans["topicId"] = _this.topics[_this.sn];
                    ans["answerId"] = _this.getAnswer();
                    _this.submitObj["answerLogDTOList"].push(ans);
                    _this.sn++;
                    if (_this.sn >= _this.topics.length) {
                        _this.submit();
                    }
                    else {
                        _this.getTopic(_this.questionId, _this.topics[_this.sn]);
                    }
                }
                Http.DATA_TYPE = 'FORMDATA';
                // this.setQuestion(datas);
            });
        };
        AnswerPage.prototype.onErrNextClick = function () {
            this.curErr++;
            if (this.curErr >= this.curErrTopic.length) {
                //返回
                this.viewStack.selectedIndex = 3;
                return;
            }
            this.selectErr(this.curErr);
        };
        AnswerPage.prototype.onForceClick = function () {
            var _this = this;
            rest.answer.force(this.questionId, function (data) {
                if (data.code == 1) {
                    // this.ani_loginLoading.clear();
                    // this.showErrorTips(data.msg);
                    _this.optionPrompt.play("领取成功", [Laya.stage.width / 2, Laya.stage.height / 2]);
                    var inviteCode = void 0;
                    rest.user.userInfo(inviteCode, function (data) {
                        Rest.USERINFO = data.data.player;
                        Rest.GALAXY = data.data.byDbexGalaxy;
                        _this.game.onCreatedRole();
                    });
                }
                else {
                    _this.optionPrompt.play(data.msg, [Laya.stage.width / 2, Laya.stage.height / 2]);
                }
            });
        };
        AnswerPage.prototype.onCheckClick = function () {
            this.errPage();
        };
        AnswerPage.prototype.getAnswer = function () {
            var ids = [];
            var sprites = this.list_answer.cells;
            var spri;
            for (var i = 0; i < sprites.length; i++) {
                spri = sprites[i];
                var cheBox = spri.getChildByName("check");
                if (cheBox.selected) {
                    var ansId = this.list_answer.getItem(i).answerId;
                    ids.push(ansId);
                }
            }
            return ids;
        };
        AnswerPage.prototype.onStartClick = function () {
            this.viewStack.selectedIndex = 1;
            this.sn = 0;
            view.LoadingPage.loading();
            this.getTopic(this.questionId, this.topics[this.sn]);
        };
        AnswerPage.prototype.submit = function () {
            var _this = this;
            Http.DATA_TYPE = 'JSON';
            rest.answer.submitAnswers(this.submitObj, function (data) {
                console.log(data);
                Http.DATA_TYPE = 'FORMDATA';
                view.LoadingPage.dispose();
                if (data.status == "error") {
                    // this.ani_loginLoading.clear();
                    // this.showErrorTips(data.msg);
                    _this.optionPrompt.play(data.msg, [Laya.stage.width / 2, Laya.stage.height / 2]);
                }
                else {
                    _this.clearList();
                    //this.event('LoginOK');
                    // this.ani_loginLoading.clear();
                    console.log("哈利要" + data);
                    var datas = data.data;
                    _this.errorNumber = datas.errNumber;
                    _this.susNumber = datas.successNumber;
                    _this.addForce = datas.rewardIsForce;
                    _this.resultPage(datas);
                }
            });
        };
        AnswerPage.prototype.resultPage = function (datas) {
            this.viewStack.selectedIndex = 3;
            if (this.addForce <= 0) {
                this.pic_fail.visible = true;
                this.pic_victory.visible = false;
                this.btn_getForce.visible = false;
                console.log("saddasdasd");
                this.game.event('Sounds', 8);
                console.log("!!!!!");
            }
            else {
                console.log("!!2222!");
                this.pic_fail.visible = false;
                this.pic_victory.visible = true;
                this.btn_getForce.visible = true;
                this.game.event('Sounds', 7);
                //btn_getForce
            }
            var str = Font.getSizeColor("#5d3500", 32, "您答对") + Font.getSizeColor("#593cce", 32, String(this.susNumber)) + Font.getSizeColor("#5d3500", 32, "题");
            this.txt_correct.innerHTML = str;
            str = Font.getSizeColor("#5d3500", 32, "增加了") + Font.getSizeColor("#593cce", 32, String(this.addForce)) + Font.getSizeColor("#5d3500", 32, "算力");
            this.txt_add.innerHTML = str;
            str = Font.getSizeColor("#5d3500", 28, "答错") + Font.getSizeColor("#ff1434", 28, String(this.errorNumber)) + Font.getSizeColor("#5d3500", 28, "题");
            this.txt_error.innerHTML = str;
            this.btn_check.visible = this.errorNumber > 0;
            this.txt_error.visible = this.errorNumber > 0;
            ;
            this.txt_error.style.align = "center";
            this.txt_correct.style.align = "center";
        };
        AnswerPage.prototype.errPage = function () {
            var _this = this;
            this.curErr = 0;
            this.viewStack.selectedIndex = 2;
            view.LoadingPage.loading();
            rest.answer.getWrongList(this.questionId, function (data) {
                view.LoadingPage.dispose();
                if (data.status == "error") {
                    // this.ani_loginLoading.clear();
                    // this.showErrorTips(data.msg);
                    _this.optionPrompt.play(data.msg, [Laya.stage.width / 2, Laya.stage.height / 2]);
                }
                else {
                    _this.curErrTopic = data.data;
                    _this.selectErr(_this.curErr);
                }
            });
        };
        AnswerPage.prototype.selectErr = function (inx) {
            this.txt_errTitle.text = "错题" + (inx + 1) + "/" + this.curErrTopic.length;
            this.txt_errWenti.wordWrap = true;
            this.txt_errWenti.text = this.curErrTopic[inx].question.title;
            this.list_errAnswer.array = this.curErrTopic[inx].questionAnswers;
            if (inx >= (this.curErrTopic.length - 1)) {
                this.txt_nextErr.text = "返回";
            }
            else {
                this.txt_nextErr.text = "下一题";
            }
        };
        AnswerPage.prototype.onListRender = function (cell, index) {
            var checkBox = cell.getChildByName("check");
            var text = cell.getChildByName("txt_answer");
            text.text = this.list_answer.array[index].answer;
            text.y = (55 - text.textHeight) / 2;
        };
        AnswerPage.prototype.listSelectHandler = function (inx) {
            this.chooseList(inx);
        };
        AnswerPage.prototype.onErrListRender = function (cell, index) {
            var data = this.list_errAnswer.getItem(index);
            var text = cell.getChildByName("txt_answer");
            var checkBox = cell.getChildByName("check");
            text.text = data.answer;
            if (data.isRightAnswer > 0) {
                checkBox.selected = true;
            }
            else {
                checkBox.selected = false;
            }
            var ans = this.curErrTopic[this.curErr].playerAnswers;
            cell.getChildByName("pic_choose").visible = false;
            for (var i = 0; i < ans.length; i++) {
                if (ans[i].answerId == data.answerId) {
                    cell.getChildByName("pic_choose").visible = true;
                }
                else {
                    cell.getChildByName("pic_choose").visible = false;
                }
            }
            text.y = (55 - text.textHeight) / 2;
        };
        AnswerPage.prototype.onOpened = function () {
            var _this = this;
            _super.prototype.onOpened.call(this);
            //this.errPage();
            view.LoadingPage.loading();
            rest.answer.getQuestion(function (data) {
                //1
                //
                view.LoadingPage.dispose();
                var datas = data.data;
                if (data.code == 1 || data.code == 10025 || data.code == 10026) {
                    _this.questionId = datas.questionId;
                    _this.topics = datas.topicId;
                    _this.power = datas.rewardIsForce;
                    _this.errorNumber = datas.errNumber;
                    _this.susNumber = datas.topicNumber - _this.errorNumber;
                    _this.addForce = datas.rewardIsForceCount;
                    _this.prizeForce = datas.rewardIsForce;
                }
                if (data.code == 1) {
                    _this.viewStack.selectedIndex = 0;
                }
                else if (data.code == 10025 || data.code == 10026) {
                    _this.questionId = data.data.questionId;
                    data.data.successNumber = data.data.topicNumber - data.data.errNumber;
                    _this.resultPage(data.data);
                    //成功
                    // if(data.data.errNumber>0)
                    // {
                    //     this.resultPage(data.data);
                    // }
                    // else
                    // {
                    // }
                }
                else {
                    _this.optionPrompt.play(data.msg, [Laya.stage.width / 2, Laya.stage.height / 2]);
                }
                if (datas) {
                    _this.txt_total.text = "\u4E00\u5171" + datas.topicNumber + "\u9898";
                    var all = datas.topicNumber * _this.prizeForce;
                    if (datas.rewardQuestionNumber == 1) {
                        _this.txt_reward.text = "\u6BCF\u7B54\u5BF9\u4E00\u9898+" + _this.prizeForce + "\u7B97\u529B";
                    }
                    else if (datas.rewardQuestionNumber > 1 && datas.rewardQuestionNumber < datas.topicNumber) {
                        _this.txt_reward.text = "\u56DE\u7B54\u6B63\u786E" + datas.rewardQuestionNumber + "\u9898\u540E\uFF0C\u6BCF\u7B54\u5BF9\u4E00\u9898+" + _this.prizeForce + "\u7B97\u529B";
                    }
                    else {
                        _this.txt_reward.text = "\u5168\u90E8\u56DE\u7B54\u6B63\u786E+" + _this.prizeForce + "\u7B97\u529B";
                    }
                    _this.txt_power1.text = _this.txt_reward.text;
                    _this.txt_power.text = _this.txt_reward.text;
                }
                Http.DATA_TYPE = 'FORMDATA';
                _this.submitObj["questionId"] = _this.questionId;
                _this.submitObj["answerLogDTOList"] = [];
                // this.setQuestion(datas);
            });
        };
        AnswerPage.prototype.getTopic = function (questionId, topicId) {
            var _this = this;
            var Token = localStorage.getItem("Token");
            rest.answer.getTopic(questionId, topicId, Token, function (data) {
                view.LoadingPage.dispose();
                if (data.msg == "error") {
                    console.log(data.msg);
                }
                else {
                    var datas = data.data;
                    var inx = _this.topics.indexOf(_this.topics[_this.sn]);
                    _this.txt_wenti.innerHTML = Font.getSizeColor("#5a3ace", 24, (inx + 1) + ".") + Font.getSizeColor("#5d3500", 24, datas.title);
                    _this.clearList();
                    _this.list_answer.array = datas.answerList;
                    if (_this.sn >= _this.topics.length - 1) {
                        _this.txt_next.text = "提交";
                    }
                    else {
                        _this.txt_next.text = "下一题";
                    }
                    _this.tinx = 30000;
                    _this.txt_sec.text = String(_this.tinx / 1000);
                    Laya.timer.loop(1000, _this, _this.countTime);
                }
            });
        };
        AnswerPage.prototype.countTime = function () {
            this.tinx -= 1000;
            this.txt_sec.text = String(this.tinx / 1000);
            if (this.tinx <= 0) {
                Laya.timer.clear(this, this.countTime);
                this.onNextClick(null);
            }
        };
        AnswerPage.prototype.onClosed = function () {
            Laya.timer.clear(this, this.time);
        };
        AnswerPage.prototype.beginTime = function () {
            this.count = 0;
            this.txt_sec.text = String(this.count);
            Laya.timer.loop(1000, this, this.time);
        };
        AnswerPage.prototype.endTime = function () {
            Laya.timer.clear(this, this.time);
        };
        AnswerPage.prototype.time = function () {
            this.count++;
            this.txt_sec.text = String(this.count);
            if (this.count == 30) {
                this.endTime();
            }
        };
        AnswerPage.prototype.clearList = function () {
            var len = this.list_answer.cells.length;
            for (var i = 0; i < len; i++) {
                var checkBox = this.list_answer.getCell(i).getChildByName("check");
                checkBox.selected = false;
            }
        };
        AnswerPage.prototype.chooseList = function (inx) {
            var checkBox = this.list_answer.getCell(inx).getChildByName("check");
            var len = this.list_answer.cells.length;
            checkBox.selected = true;
            for (var i = 0; i < len; i++) {
                if (inx != i) {
                    checkBox = this.list_answer.getCell(i).getChildByName("check");
                    checkBox.selected = false;
                }
            }
        };
        return AnswerPage;
    }(ui.AnswerPageUI));
    view.AnswerPage = AnswerPage;
})(view || (view = {}));
//# sourceMappingURL=AnswerPage.js.map