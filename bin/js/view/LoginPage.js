var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**Created by the LayaAirIDE*/
var view;
(function (view) {
    var LoginPage = /** @class */ (function (_super) {
        __extends(LoginPage, _super);
        function LoginPage() {
            var _this = _super.call(this) || this;
            _this.tips = 0;
            _this.mobile.text = "";
            _this.password.text = "";
            _this.commonlyUsed = new CommonlyUsed();
            _this.submit.on(Laya.Event.CLICK, _this, _this.onSubmit);
            _this.sign.on(Laya.Event.CLICK, _this, _this.onSign);
            _this.text_forgetPassword.on(Laya.Event.CLICK, _this, _this.onClickForgetPassword);
            //加载图集资源
            Laya.loader.load(["res/atlas/changeGalaxy.atlas", "res/atlas/public.atlas"]);
            return _this;
        }
        LoginPage.prototype.onClosePrompt = function () {
            Laya.timer.clear(this, this.tipsLoop);
        };
        LoginPage.prototype.AniComplete = function () {
        };
        LoginPage.prototype.onSubmit = function () {
            var _this = this;
            //  this.event('LoginOK');
            this.ani_loginLoading.visible = true;
            // if(this.ani_loginLoading.visible){
            this.submit.disabled = true;
            this.sign.mouseEnabled = false;
            // }
            this.ani_loginLoading.play(0, true, "ani1");
            var name = this.mobile.text || '';
            var password = this.password.text || '';
            var invite_code = this.commonlyUsed.UrlSearch();
            if (name == "" || password == "") {
                this.ani_loginLoading.clear();
                this.showErrorTips("手机号码或密码不能为空!");
                return;
            }
            else {
                Http.DATA_TYPE = 'JSON';
                rest.user.login({ name: name, password: password, invite_code: invite_code }, (function (data) {
                    //console.log(name + "__" + password)
                    //console.log(data)
                    if (data.status == "error") {
                        _this.ani_loginLoading.clear();
                        _this.showErrorTips(data.msg);
                    }
                    else {
                        _this.event('LoginOK');
                        // this.ani_loginLoading.clear();
                    }
                    Http.DATA_TYPE = 'FORMDATA';
                }));
            }
        };
        LoginPage.prototype.showErrorTips = function (msg) {
            this.tips = 2;
            this.img_loin.visible = true;
            this.img_loin.width = msg.length * 30;
            this.text_loginTips.width = msg.length * 30;
            this.img_loin.centerX = 1;
            this.text_loginTips.text = msg;
            Laya.timer.loop(1000, this, this.tipsLoop);
        };
        LoginPage.prototype.tipsLoop = function () {
            this.tips--;
            if (this.tips <= 0) {
                this.img_loin.visible = false;
                if (this.submit.disabled) {
                    this.submit.disabled = false;
                    this.sign.mouseEnabled = true;
                }
                Laya.timer.clear(this, this.tipsLoop);
            }
        };
        LoginPage.prototype.onSign = function () {
            // this.event("SignUpOK");
            //跳转注册页面
            this.signUpPage = new view.SignUpPage();
            Laya.stage.addChild(this.signUpPage);
        };
        LoginPage.prototype.onClickForgetPassword = function () {
            this.forgetPasswordPages = new view.ForgetPasswordPages();
            Laya.stage.addChild(this.forgetPasswordPages);
        };
        // 关闭登录
        LoginPage.prototype.close = function () {
            if (this.parent != null) {
                this.parent.removeChild(this);
            }
        };
        return LoginPage;
    }(ui.LoginPageUI));
    view.LoginPage = LoginPage;
})(view || (view = {}));
//# sourceMappingURL=LoginPage.js.map