var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**Created by the LayaAirIDE*/
var view;
(function (view) {
    var ChooseHead = /** @class */ (function (_super) {
        __extends(ChooseHead, _super);
        function ChooseHead(game) {
            var _this = _super.call(this) || this;
            _this.game = game;
            _this.nickname.text = "";
            _this.head = 1;
            _this.tips = 0;
            _this.commonlyUsed = new CommonlyUsed;
            _this.btn_confirm.on(Laya.Event.CLICK, _this, _this.onCreateRole);
            _this.img_head1.on(Laya.Event.CLICK, _this, _this.onPickHead1);
            _this.img_head2.on(Laya.Event.CLICK, _this, _this.onPickHead2);
            _this.img_head3.on(Laya.Event.CLICK, _this, _this.onPickHead3);
            _this.img_head4.on(Laya.Event.CLICK, _this, _this.onPickHead4);
            return _this;
        }
        ChooseHead.prototype.onPickHead1 = function () {
            this.onPickHead(1);
        };
        ChooseHead.prototype.onPickHead2 = function () {
            this.onPickHead(2);
        };
        ChooseHead.prototype.onPickHead3 = function () {
            this.onPickHead(3);
        };
        ChooseHead.prototype.onPickHead4 = function () {
            this.onPickHead(4);
        };
        ChooseHead.prototype.onPickHead = function (num) {
            switch (num) {
                case 1:
                    this.head = 1;
                    this.img_pick1.visible = true;
                    this.img_pick2.visible = false;
                    this.img_pick3.visible = false;
                    this.img_pick4.visible = false;
                    break;
                case 2:
                    this.head = 2;
                    this.img_pick1.visible = false;
                    this.img_pick2.visible = true;
                    this.img_pick3.visible = false;
                    this.img_pick4.visible = false;
                    break;
                case 3:
                    this.head = 3;
                    this.img_pick1.visible = false;
                    this.img_pick2.visible = false;
                    this.img_pick3.visible = true;
                    this.img_pick4.visible = false;
                    break;
                case 4:
                    this.head = 4;
                    this.img_pick1.visible = false;
                    this.img_pick2.visible = false;
                    this.img_pick3.visible = false;
                    this.img_pick4.visible = true;
                    break;
            }
        };
        ChooseHead.prototype.onCreateRole = function () {
            var _this = this;
            //var nickname = Laya.Browser.window._.trim(this.nickname.text);
            if (!this.nickname.text || this.nickname.text.length == 0) {
                return;
            }
            rest.user.createRole({
                userName: this.nickname.text,
                headIcon: this.head,
                invite_code: this.commonlyUsed.UrlSearch(),
            }, function (data) {
                //console.log(data)
                if (data.status == "error") {
                    _this.showErrorTips(data.msg);
                }
                else {
                    Rest.USERINFO = data.data;
                    //console.log(Rest.USERINFO);
                    _this.addGalaxy();
                    _this.close();
                }
                rest.user.coinsList(function (param) {
                    console.log(param);
                    if (param.status == "error") {
                        console.log("拉取coins信息失败");
                    }
                    else {
                        Rest.COINSLIST = param.data;
                    }
                });
            });
        };
        ChooseHead.prototype.addGalaxy = function () {
            if (this.ChangeGalaxyPage == null) {
                this.ChangeGalaxyPage = new view.ChangeGalaxyPage(this);
            }
            Laya.stage.addChild(this.ChangeGalaxyPage);
        };
        ChooseHead.prototype.showErrorTips = function (msg) {
            this.tips = 3;
            this.img_createPlayer.visible = true;
            this.img_createPlayer.width = msg.length * 30;
            this.text_createPlayer.width = msg.length * 30;
            this.img_createPlayer.centerX = 1;
            this.text_createPlayer.text = msg;
            Laya.timer.loop(1000, this, this.tipsLoop);
        };
        ChooseHead.prototype.tipsLoop = function () {
            this.tips--;
            if (this.tips <= 0) {
                this.img_createPlayer.visible = false;
                Laya.timer.clear(this, this.tipsLoop);
            }
        };
        return ChooseHead;
    }(ui.ChooseHeadUI));
    view.ChooseHead = ChooseHead;
})(view || (view = {}));
//# sourceMappingURL=ChooseHead.js.map