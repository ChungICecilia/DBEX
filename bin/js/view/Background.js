var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var view;
(function (view) {
    var Background = /** @class */ (function (_super) {
        __extends(Background, _super);
        function Background(game) {
            var _this = _super.call(this) || this;
            _this.game = game;
            Laya.loader.load(["res/atlas/friend.atlas"]);
            _this.init();
            return _this;
        }
        Background.prototype.init = function () {
            this.bg = new Laya.Sprite();
            this.bg.loadImage("res/singleImages/bg.jpg", 0, 0, Laya.stage.width, Laya.stage.height);
            this.addChild(this.bg);
            this.bgAni = new Laya.Animation();
            this.bgAni.loadAnimation("bg.ani", Laya.Handler.create(this, this.onLoaded), null);
            this.bgAni.pos(375, 220);
            this.addChild(this.bgAni);
            this.bgAni.play(0, true, "ani1"); //
        };
        Background.prototype.onLoaded = function () {
            this.bgAni.on(Laya.Event.COMPLETE, this, null);
        };
        return Background;
    }(Laya.Sprite));
    view.Background = Background;
})(view || (view = {}));
//# sourceMappingURL=Background.js.map