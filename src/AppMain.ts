﻿/*
* name;
*/
class AppMain {
    private loginPage: view.LoginPage;
    public gamePage: view.GamePage;
    private EnterTheGamePage: view.EnterTheGamePage;
    private commonlyUsed: CommonlyUsed;
    constructor() {
        this.commonlyUsed = new CommonlyUsed;
        //初始化引擎，设置游戏设计宽高，并且打开WebGL模式
        Laya.init(750, 1207, Laya.WebGL);
        if(Browser.onPC){
            //laya.utils.Stat.show(0,0);  
        }
        
        //加载图集资源
        Laya.loader.load(["res/atlas/animal/loading.atlas", "res/atlas/login.atlas", "res/atlas/changeGalaxy.atlas", "res/atlas/public.atlas"],// "res/atlas/game.atlas", 
            Laya.Handler.create(this, this.onLoaded), null, Laya.Loader.ATLAS);
        Laya.stage.scaleMode = Laya.Stage.SCALE_EXACTFIT;
        //设置居中对齐
        Laya.stage.alignH = Laya.Stage.ALIGN_CENTER;
        Laya.stage.alignV = Laya.Stage.ALIGN_CENTER;
        //设置横竖屏
        Laya.stage.screenMode = "none";

        //显示FPS
        // Laya.Stat.show(100, 300);
        view.UILayout.stepNum = 0;
    }

    private onLoaded(): void {
        /**判断是是否平台入口的用户*/
        var name:string = "Token";
        var pToken:string = Laya.Browser.window.getCookie(name);
        var Token:string;
        if(pToken)
        {
            Token = pToken;
            localStorage.setItem('Token', Token);
            Rest.HEADERS = Rest.CONTENT_TYPE_JSON.concat(['Authorization', Token]);
            rest.user.onHandoverGame(param => {
				console.log(param);
				if(param.status == "error")
				{    
                    
				}else
				{
			        rest.user.userInfo(inviteCode, (data => {
                        console.log(data)
                        if (data.status == "error") {
                             this.onGame();
                        } else {
                            Rest.USERINFO = data.data.player;
                            Rest.SKIP = data.data.isSkip;
                            this.onGame();
                            // this.ani_EnterTheGameLoading.clear();
                            rest.user.coinsList(param => {
                                console.log(param);
                                if(param.status == "error")
                                {
                                    console.log("拉取coins信息失败");
                                }else
                                {
                                    Rest.COINSLIST = param.data;
                                }
                            });
                        }
                    }))
                    return;
				}
			});
        }else
        {
            Token = localStorage.getItem("Token");  
            this.loginPage = new view.LoginPage();
            this.EnterTheGamePage = new view.EnterTheGamePage();
            this.loginPage.on("LoginOK", this, this.onGame);
            this.EnterTheGamePage.on("GO", this, this.onGame);
            this.EnterTheGamePage.on("ONLOGIN", this, this.onLogin);
            if (this.gamePage != null) {
                this.gamePage.on("CLOSE", this, this.onLoginOK);
            }
            if (Token == null || Token == "undefined") {
                this.onLogin();
            } else {
                var clickOnNewsTime = localStorage.getItem("clickOnNewsTime");
                var data: Date = new Date();
                var openAppTime = data.getTime();
                var inviteCode = this.commonlyUsed.UrlSearch();
                if (openAppTime - parseInt(clickOnNewsTime) < 20000) {
                    Rest.HEADERS = Rest.CONTENT_TYPE_JSON.concat(['Authorization', Token]);
                    
                    rest.user.userInfo(inviteCode, (data => {
                        rest.user.option(data => {
                            Rest.OPTION = data;
                        })
                        if (data.status == "error") {
                            this.onLogin();
                        } else {
                            Rest.USERINFO = data.data.player;
                            this.onGame();
                        }
                    }))
                } else {
                    Rest.HEADERS = Rest.CONTENT_TYPE_JSON.concat(['Authorization', Token])

                    rest.user.option(data => {
                        Rest.OPTION = data;
                    })
                    this.onEnterTheGame();
                }
            }
        }
    }

    private onGame(): void {
        this.gamePage = new view.GamePage();
        Laya.stage.addChild(this.gamePage);
    }

    // 登录成功
    private onLoginOK(): void {
        //关闭登录页面
        this.loginPage.close();
        this.EnterTheGamePage.close();
    }

    //进入游戏页面
    private onEnterTheGame(): void {
        Laya.stage.addChild(this.EnterTheGamePage);
    }



    //跳登录页面
    private onLogin(): void {
        Laya.stage.addChild(this.loginPage);
    }
}

var app = new AppMain()