/*
* name;
*/
module rest {

    class Draw{
        constructor(){

        }

        public list(callback):void
		{
			
			
			let headers:any = Rest.HEADERS.concat();
			headers[1] = localStorage.getItem("Token");
			Http.get(`${Rest.DHOSTS}/dbex/rewardSector/list`,Rest.RESPONSE_TYPE_JSON,headers, (data) => {
				callback && callback(data);
				console.log(data);
			})
		}

		public drawPrize(type:number,callback):void
		{
			let headers:any = Rest.HEADERS.concat();
			headers[1] = localStorage.getItem("Token");
			Http.get(`${Rest.DHOSTS}/dbex/rewardSector/randomDraw/${type}`,Rest.RESPONSE_TYPE_JSON,headers, (data) => {
				callback && callback(data);
				console.log(data);
			})
		}
    }
    
    export const draw = new Draw();
}