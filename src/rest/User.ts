/**
* name 
*/
module rest {
	class User extends Rest {
		constructor() {
			super()
		}

		// 登录
		// public login({name, password}, callback): void {

		// 	Http.post(`${Rest.HOST}/admin/login`, JSON.stringify({
		// 		name: name,
		// 		password: password
		// 	}), Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {

		// 		// 设置token
		// 		super._login(data.token)

		// 		// 同步登录用户信息
		// 		this.userInfo((data) => {

		// 			if (data.stauts == 'success') {
		// 				super._setUserInfo(data.data)
		// 			} else {
		// 				console.log(data.description)
		// 			}

		// 			callback && callback(data)
		// 		})
		// 	})
		// }

		// // 注册
		// public register({name, password, code}, callback): void {

		// 	Http.post(`${Rest.HOST}/admin/register`, JSON.stringify({
		// 		name: name,
		// 		password: password,
		// 		code: code,
		// 	}), Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {

		// 		callback && callback(data)
		// 	})
		// }

		// // 登出
		// public logout(callback): void {
		// 	Http.post(`${Rest.HOST}/admin/logout`,
		// 		'', Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {

		// 			super._logout()
		// 			callback && callback(data)
		// 		})
		// }

		/**
		 * 注册 http://192.168.31.62:8070/user/
		 * @param param0 
		 * @param callback 
		 */
		public register({phone, code, passWord, userCode, userId}, callback): void {
			//http://api.test.bcrealm.com:8080/user/register
			Http.post(`${Rest.HOSTS}/user/register`, {
				code: code,
				fromChannel: 1,
				fromType: 3,
				password: passWord,
				phone: phone,
				userCode: userCode,
				userId: "",//新增不填写,修改必填
				userLogoImg: "",
			}, Rest.RESPONSE_TYPE_JSON,
				Rest.HEADERS.concat(["Content-Type", "application/json; charset=utf-8"]),
				(data) => {

					callback && callback(data);
				})
		}

		/**
		 *确认修改密码 
		 * @param param0 
		 * @param callback 
		 */
		public changePassword({ code, passWord, phone, repeatPassword }, callback): void {
			//http://api.test.bcrealm.com:8080/user/register
			Http.post(`${Rest.HOSTS}/user/back/pwd`, {
				code: code,
				password: passWord,
				phone: phone,
				repeatPassword: repeatPassword,
			}, Rest.RESPONSE_TYPE_JSON,
				Rest.HEADERS.concat(["Content-Type", "application/json; charset=utf-8"]),
				(data) => {

					callback && callback(data);
				})
		}

		//获取注册验证码  /user/back/ /user/back/msg
		public getSMS({phone, type}, callback): void {
			Http.post(`${Rest.HOSTS}/user/send/msg?phone=` + phone, {
				phone: phone,
			}, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {

				callback && callback(data);
			})

		}

		/**
		 *修改密码获取短信 
		 * @param param0 
		 * @param callback 
		 */
		public getMSBack({phone, type}, callback): void {
			Http.post(`${Rest.HOSTS}/user/back/msg?phone=` + phone, {
				phone: phone,
			}, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {

				callback && callback(data);
			})

		}
		// 登录
		public login({name, password, invite_code}, callback): void {

			Http.post(`${Rest.HOSTS}/user/login`, {//`${Rest.HOST}/jwtToken`
				userName: name,
				password: password,
				fromChannel: 1,
				fromType: 3,
				invite_code: invite_code
			}, Rest.RESPONSE_TYPE_JSON,
				Rest.HEADERS.concat(["Content-Type", "application/json; charset=utf-8"]),
				(data) => {

					console.log(data);
					if (data.code == 1) {
						if (data.data.token != null) {
							// 设置token
							super._login(data.data.token, '')

							this.userInfo({ invite_code }, (userInfo) => {
								//console.log(userInfo)
								if (userInfo.status != "error") {
									super._setUserInfo(userInfo);
									Rest.SKIP = data.data.isSkip;
									rest.user.coinsList(param => {
										console.log(param);
										if(param.status == "error")
										{
											console.log("拉取coins信息失败");
										}else
										{
											Rest.COINSLIST = param.data;
										}
									});
								}
								callback(data);///
								// this.option((optionInfo) => {
								// 	super._setOption(optionInfo);
								// 	callback(data);
								// });
							})
						}
					}
					else {
						callback(data);
					}
				})
		}
		// 注册
		// public register({phone, code, passWord, userCode,userId}, callback): void {

		// 	Http.post(`${Rest.HOST}/v1/user/register`, {
		// 		phone: phone,
		// 		passWord: passWord,
		// 		code: code,
		// 		userCode: userCode,
		// 	}, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {

		// 		callback && callback(data);
		// 	})
		// }

		// //获取验证码
		// public getSMS({phone, type}, callback): void {
		// 	Http.post(`${Rest.HOST}/v1/user/getSMS`, {
		// 		phone: phone,
		// 		type: type,
		// 	}, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {

		// 		callback && callback(data);
		// 	})

		// }
		// // 登录
		// public login({name, password, invite_code}, callback): void {

		// 	Http.post(`${Rest.HOST}/jwtToken`, {//`${Rest.HOST}/jwtToken`
		// 		username: name,
		// 		password: password,
		// 		grant_type: 'password',
		// 		invite_code: invite_code
		// 	}, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS.concat(['Authorization', 'Basic YWRtaW46YWRtaW4=']), (data) => {//'Authorization', 'Basic YWRtaW46YWRtaW4='

		// 		console.log(data);
		// 		if (data.token != null) {
		// 			// 设置token
		// 			super._login(data.token, 'Bearer')
		// 			var inviteCode: '';
		// 			this.userInfo(inviteCode, (userInfo) => {
		// 				//console.log(userInfo)
		// 				if (userInfo.status != "error") {
		// 					super._setUserInfo(userInfo);
		// 				}
		// 				callback(data);///
		// 				// this.option((optionInfo) => {
		// 				// 	super._setOption(optionInfo);
		// 				// 	callback(data);
		// 				// });
		// 			})
		// 		} else {
		// 			callback(data);
		// 		}
		// 	})
		// }
		/*
	 * 获取邀请码 
	 */
		public getInviteCode(callback): void {
			Http.get(`${Rest.HOST}/v1/user/getInviteCode`, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				callback && callback(data);
			})
		}

		/**
		 * 查询注册验证码是否正确
		 * @param param0 
		 * @param callback 
		 */
		public Msg({ code, phone}, callback): void {

			Http.post(`${Rest.HOSTS}/user/Msg/`, {
				phone: phone,
				code: code
			}, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS.concat(["Content-Type", "application/json; charset=utf-8"]),
				(data) => {
					callback && callback(data);
					console.log(data);
				})
		}

		/**
		 * 查询更改密码验证码是否正确
		 * @param param0 
		 * @param callback 
		 */
		public sMsg({ code, phone}, callback): void {

			Http.post(`${Rest.HOSTS}/user/back/`, {
				phone: phone,
				code: code
			}, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS.concat(["Content-Type", "application/json; charset=utf-8"]),
				(data) => {
					callback && callback(data);
					console.log(data);
				})
		}

		/**
		 * 用户信息
		 * @param callback 
		 */
		public userInfo(inviteCode, callback): void {
			Http.post(`${Rest.HOST}/player/details`, { inviteCode: inviteCode }, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				callback && callback(data);
				console.log(data);
			})
		}
		/**
		 * 平台入口来的用户需发送
		 * @param callback 
		 */
		public onHandoverGame(callback): void {
			Http.get(`${Rest.HOSTS}/user/handoverGame`, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				callback && callback(data);
			})
		}
		/**
		 * 请求coinsList信息
		 * 
		 */
		public coinsList(callback): void{
			Http.get(`${Rest.DHOSTS}/dbex/coins/list`, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (param) => {
				callback && callback(param);
			})
		}

		

		// 配置列表
		public option(callback): void {
			Http.get(`${Rest.HOST}/option/list`, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				callback && callback(data);
			})
		}


		//实名认证
		public realName({name, card}, callback): void {
			Http.post(`${Rest.HOST}/idcardAuth`, {
				idcardName: name,
				idcardNo: card,
			}, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {

				callback && callback(data);
			})

		}
		//红点提示
		public redPoint(callback): void {
			Http.get(`${Rest.HOST}/unfinished`, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				console.log(data);
				callback && callback(data);
			})

		}


		/**
		 * 创建玩家角色
		 * @param param0 
		 * @param callback 
		 */
		public createRole({userName, headIcon, invite_code }, callback): void {
			Http.post(`${Rest.HOST}/player/createPlayer`, {
				userName: userName,
				headIcon: headIcon,
				invite_code: invite_code,
			}, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				callback && callback(data);
				//console.log(data);
			})
		}

		/**
		 * 设置音乐音效
		 * @param type 
		 * @param status 
		 * @param callback 
		 */

		public setMusicStatus({type, status}, callback): void {
			Http.post(`${Rest.HOST}/player/setMusicStatus`, {
				type: type,
				status: status,
			}, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				callback && callback(data);
			})
		}

		/**
		 *更新新手指引步骤 
		 * @param param0 
		 * @param callback 
		 */
		public moveGuidingSteps({step}, callback): void {
			Http.post(`${Rest.HOST}/player/moveGuidingSteps`, {
				step: step,
			}, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				callback && callback(data);
				console.log(data);
			})
		}
	}

	export const user = new User()
}