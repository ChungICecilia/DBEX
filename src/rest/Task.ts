module rest {
    class Task extends Rest {
        constructor() {
            super();
        }
        /**
         * 获取任务列表
         * @param callback 
         */
        public tasks(callback): void {
            Http.get(`${Rest.HOST}/tasks`, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
                callback && callback(data);
            })
        }


        /**
         * 领取任务奖励
         * @param param0 
         * @param callback 
         */
        public askRewards(taskId, callback): void {
            Http.post(`${Rest.HOST}/askRewards`, {
                taskId: taskId
            }, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
                callback && callback(data);
            })
        }
    }
    export const task = new Task()
}