/**
* name 
*/
module rest{
	export class Chong extends Rest{
		constructor(){
			super();
		}
		
		/**
		 * 获取问答题库
		 */
		public info(callback):void
		{
			let headers:any = Rest.HEADERS.concat();
			headers[1] = localStorage.getItem("Token");
			
			Http.post(`${Rest.DHOSTS}/dbex/activity/`,JSON.stringify({}), Rest.RESPONSE_TYPE_JSON, headers, (data) => {
				callback && callback(data);
				console.log(data);
			})
		}

		public getTopicId(callback):void
		{
			let headers:any = Rest.HEADERS.concat();
			headers[1] = localStorage.getItem("Token");
			Http.post(`${Rest.DHOSTS}/dbex/activity/topic`,JSON.stringify({}), Rest.RESPONSE_TYPE_JSON, headers, (data) => {
				callback && callback(data);
				console.log(data);
			})
		}

		public getTopic(topicId:number,callback):void
		{
			let headers:any = Rest.HEADERS.concat();
			Http.get(`${Rest.DHOSTS}/dbex/topic/${topicId}`, Rest.RESPONSE_TYPE_JSON, headers, (data) => {
				callback && callback(data);
				console.log(data);
			})
		}

		public answer(answers:Object,callback):void
		{
			let headers:any = Rest.HEADERS.concat();
			headers[1] = localStorage.getItem("Token");
			headers = headers.concat(["Content-Type", "application/json; charset=utf-8"]);
			Http.post(`${Rest.DHOSTS}/dbex/activity/answer`,answers,Rest.RESPONSE_TYPE_JSON,headers,(data) => {
				callback && callback(data);
				console.log(data);
			})
		}

		public live(activityId:number,callback):void
		{
			let headers:any = Rest.HEADERS.concat();
			headers[1] = localStorage.getItem("Token");
			headers = headers.concat(["Content-Type", "application/json; charset=utf-8"]);
			Http.post(`${Rest.DHOSTS}/dbex/activity/revive/${activityId}`,JSON.stringify({}),Rest.RESPONSE_TYPE_JSON,headers,(data) => {
				callback && callback(data);
				console.log(data);
			})
		}

		public reward(activityId:number,callback):void
		{
			let headers:any = Rest.HEADERS.concat();
			headers[1] = localStorage.getItem("Token");
			Http.get(`${Rest.DHOSTS}/dbex/activity/getActivityRewards?activityId=${activityId}`, Rest.RESPONSE_TYPE_JSON, headers, (data) => {
				callback && callback(data);
				console.log(data);
			})
		}

		public getPrizeNotice(callback):void
		{
			let headers:any = Rest.HEADERS.concat();
			headers[1] = localStorage.getItem("Token");
			Http.get(`${Rest.DHOSTS}/dbex/activity/getNotReceivedActivityList`, Rest.RESPONSE_TYPE_JSON, headers, (data) => {
				callback && callback(data);
				console.log(data);
			})

		}


	}

	
	export const chong = new Chong();
	
}