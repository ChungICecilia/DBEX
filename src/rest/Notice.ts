/**
* name 
*/
module rest {
    class Notice extends Rest {
        constructor() {
            super()
        }

        public list(callback): void {

            Http.get(`${Rest.DHOSTS}/dbex/notice/list`, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
                // console.log(data)
                callback && callback(data);
            })
        }



    }

    export const notice = new Notice()
}