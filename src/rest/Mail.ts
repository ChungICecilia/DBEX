/**
* name 
*/
module rest {
	class Mail extends Rest {
		constructor() {
			super()
		}

		/**
		 * 获取邮件列表
		 * @param page 
		 * @param pageSize 
		 * @param callback 
		 */
		public list(page: number, pageSize: number, callback): void {

			Http.get(`${Rest.HOST}/api/mail`, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				//console.log(data);
				callback && callback(data);
			})
		}

		/**
		 * 获取指定邮件的详情
		 * @param id 
		 * @param callback 
		 */
		public info(id: String, callback): void {

			Http.get(`${Rest.HOST}/api/mail/${id}`, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				//console.log(data);
				callback && callback(data);
			})
		}

		/**
		 * 阅读一封邮件
		 * @param id 
		 * @param callback 
		 */
		public read(id: String, callback): void {

			Http.put(`${Rest.HOST}/api/mail/read/${id}`, '', Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				//console.log(data);
				callback && callback(data);
			})
		}

		/**
		 * 阅读所有的邮件
		 * @param ids 
		 * @param callback 
		 */
		public readAll(callback): void {

			Http.put(`${Rest.HOST}/api/mail/read`, JSON.stringify({
			}), Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				//console.log(data);
				callback && callback(data);
			})
		}

		/**
		 * 删除所有的邮件
		 * @param ids 
		 * @param callback 
		 */
		public deleteAll(callback): void {

			Http.delete(`${Rest.HOST}/api/mail/deleteAll`,
				Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
					//console.log(data);
					callback && callback(data);
				})
		}
	}

	export const mail = new Mail()
}