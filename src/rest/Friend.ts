/**
* name 
*/
module rest {
	class Friend extends Rest {
		constructor() {
			super()
		}

		// 好友列表
		public list(page: number, pageSize: number, callback): void {
			
			Http.get(`${Rest.HOST}/friends/list`, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				callback && callback(data);
			})
		}
		
		/**
		 * 添加好友
		 * @param nickName 
		 * @param callback 
		 */
		public addFriend({nickName}, callback): void {
			Http.post(`${Rest.HOST}/friends/add`, {
				nickName: nickName,
			}, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				callback && callback(data);
			})
		}

		/**
		 *好友浇水记录 
		 * @param page 
		 * @param pageSize 
		 * @param callback 
		 */
		public wateringRecord(callback): void {

			Http.get(`${Rest.HOST}/wateringRecord`, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				callback && callback(data);
			})
		}
		
		/**
		 * 算力排行榜
		 * @param page 
		 * @param pageSize 
		 * @param callback 
		 */
		public bbmCalcPoint(page: number, pageSize: number, callback): void {

			Http.get(`${Rest.DHOSTS}/dbex/force/week`, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				callback && callback(data);
			})

			// Http.get(`${Rest.HOST}/rank/bbmCalcPoint`, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
			// 	callback && callback(data);
			// })
		}

		public getAnking(page: number, pageSize: number, callback): void {

			Http.get(`${Rest.HOST}/dbex/getAnking`, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				callback && callback(data);
			})
		}
		
	}

	export const friend = new Friend()
}