/**
* name 
*/
module rest {
	class Answer extends Rest {
		
		constructor() {
			super();
			
		}
		
		/**
		 * 获取问答题库
		 */
		public getQuestion(callback):void
		{
				
			let headers:any = Rest.HEADERS.concat();
			headers[1] = localStorage.getItem("Token");
			
			Http.post(`${Rest.DHOSTS}/dbex/question/topic`,JSON.stringify({}), Rest.RESPONSE_TYPE_JSON, headers, (data) => {
				callback && callback(data);
				console.log(data);
			})
		}

		public getTopic(questionId:number,topicId:number,token:any,callback):void
		{
			
			
			let headers:any = Rest.HEADERS.concat();
			headers[1] = localStorage.getItem("Token");
			Http.get(`${Rest.DHOSTS}/dbex/topic/user/${topicId}`,Rest.RESPONSE_TYPE_JSON,headers.concat(["questionId",questionId]), (data) => {
				callback && callback(data);
				console.log(data);
			})
		}

		public setAnswerTime(questionId:number,topicId:number,callback):void
		{
			let headers:any = Rest.HEADERS.concat();
			headers[1] = localStorage.getItem("Token");
			Http.get(`${Rest.DHOSTS}/dbex/topic/user/answer/${topicId}`,Rest.RESPONSE_TYPE_JSON,headers.concat(["questionId",questionId]), (data) => {
				callback && callback(data);
				console.log(data);
			})
		}

		public submitAnswers(answers:Object,callback):void
		{
			let headers:any = Rest.HEADERS.concat();
			headers[1] = localStorage.getItem("Token");
			headers = headers.concat(["Content-Type", "application/json; charset=utf-8"]);
			Http.post(`${Rest.DHOSTS}/dbex/questionLog/`,answers,Rest.RESPONSE_TYPE_JSON,headers,(data) => {
				callback && callback(data);
				console.log(data);
			})
			
			
		}

		public force(questionId:number,callback):void
		{
			let headers:any = Rest.HEADERS.concat();
			headers[1] = localStorage.getItem("Token");
			Http.post(`${Rest.DHOSTS}/dbex/questionLog/receive/force?questionId=`+questionId,JSON.stringify({}),Rest.RESPONSE_TYPE_JSON,headers,(data) => {
				callback && callback(data);
				console.log(data);
			})

		}

		public getWrongList(questionId:number,callback):void
		{
			let headers:any = Rest.HEADERS.concat();
			headers[1] = localStorage.getItem("Token");
		
			Http.get(`${Rest.DHOSTS}/dbex/questionLog/getWrongAnsweredList?questionId=${questionId}`,Rest.RESPONSE_TYPE_JSON,headers,(data) => {
				callback && callback(data);
				
			})
		}
	}
	export const answer = new Answer();
}