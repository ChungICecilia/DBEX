/**
* name 
*/
module rest {
	class Money extends Rest {
		constructor() {
			super()
		}

		public total(page: number, pageSize: number, callback): void {

			Http.get(`${Rest.HOST}/api/money/total`, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				//console.log(data);
				callback && callback(data);
			})
		}

		public add(num: String, callback): void {

			Http.post(`${Rest.HOST}/api/money/add`, JSON.stringify({
				num: num
			}), Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				//console.log(data);
				callback && callback(data);
			})
		}
	}

	export const money = new Money()
}