/**
* name 
*/
module rest {
	class Work extends Rest {
		constructor() {
			super()
		}

		/**
		 * 给自己浇水
		 * @param callback 
		 */
		public wateringTree(callback): void {

			Http.get(`${Rest.HOST}/wateringTree`, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				//console.log(data);
				callback && callback(data);
			})
		}

		/**
		 * 升级树
		 * @param callback 
		 */
		public upgradeTree(callback): void {

			Http.get(`${Rest.HOST}/upgradeTree`, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				callback && callback(data);
			})
		}

		/**
		 * 给好友浇水
		 * @param friendId 
		 * @param callback 
		 */
		public wateringFriendsTree({friendId}, callback): void {

			Http.post(`${Rest.HOST}/wateringFriendsTree`, {
				friendId: friendId,
			}, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				callback && callback(data);
				//console.log(data)
			})
		}

		/**
		 * 领取每天刷新的金币
		 * @param callback 
		 */
		// public pickCoin(callback): void {

		// 	Http.get(`${Rest.HOST}/pickCoin`, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
		// 		callback && callback(data);
		// 	})
		// }
		/**
		 * 新的领取每天刷新的金币
		 * @param callback 
		 */
		public pickCoin({id},callback): void {
			Http.post(`${Rest.DHOSTS}/dbex/coins/gainCoinsBbm`,{id:id}, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (param) => {
				callback && callback(param);
			});
		}
	}

	export const work = new Work()
}