/**
* name 
*/
module rest {
	class News extends Rest {
		constructor() {
			super()
		}

		public list(page: number, pageSize: number, callback): void {

			Http.get(`${Rest.HOST}/news/list`, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				console.log(data)
				callback && callback(data);
			})
		}

		public info(id: String, callback): void {

			Http.get(`${Rest.HOST}/api/news/${id}`, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				//console.log(data);
				callback && callback(data);
			})
		}

		public read(id: String, callback): void {

			Http.put(`${Rest.HOST}/api/news/read/${id}`, JSON.stringify({}),
				Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
					//console.log(data);
					callback && callback(data);
				})
		}
	}

	export const news = new News()
}