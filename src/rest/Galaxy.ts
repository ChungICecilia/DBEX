/**
* name 
*/
module rest {
	class Galaxy extends Rest {
		constructor() {
			super()
		}
		
		/**
		 * 查询推荐星系节点
		 * @param callback 
		 */
		public groomGalaxy(callback): void {

			Http.get(`${Rest.HOST}/galaxy/getRecs`, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				//console.log(data);
				callback && callback(data);
			})
		}
		//星球大战前景色
		/**
		 * 查询全部星系节点
		 * @param callback 
		 */
		public allGalaxy(callback): void {

			Http.get(`${Rest.HOST}/galaxy/listAll`, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				callback && callback(data);
			})
		}
		/**
		 * 设置星系
		 * @param galaxyld 
		 */
		public setGalaxy({galaxyld}, callback): void {
			Http.post(`${Rest.HOST}/player/chooseGalaxy`, {
				galaxyId: galaxyld,
			}, Rest.RESPONSE_TYPE_JSON, Rest.HEADERS, (data) => {
				callback && callback(data);
				//console.log(data);
			})
		}

		

	}

	export const galaxy = new Galaxy()
}