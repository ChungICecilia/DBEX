

class Http1 {
	constructor() {

	}

	private static json2Params(json): String {
		return Object.keys(json).map((key)=>{
			return encodeURIComponent(key) + "=" + encodeURIComponent(json[key]);
		}).join("&");
	}
	
	/**
	 * Get 查询资源
	 * @param url 
	 * @param responseType 
	 * @param headers 
	 * @param complete 
	 */
	public static get(url, responseType, headers, complete): void {

		let xhr: Laya.HttpRequest = new Laya.HttpRequest()
		xhr.http.timeout = 10000;//设置超时时间；
		xhr.once(Laya.Event.COMPLETE, this, complete);

		xhr.send(url, "", "get", responseType, headers);
	}

	/**
	 * Post 提交资源
	 * @param url 
	 * @param data 
	 * @param responseType 
	 * @param headers 
	 * @param complete 
	 */
	public static post(url, data, responseType, headers, complete): void {

		let xhr: Laya.HttpRequest = new Laya.HttpRequest()
		xhr.http.timeout = 10000;//设置超时时间；
		xhr.once(Laya.Event.COMPLETE, this, complete);


		xhr.send(url, data, "post", responseType, headers);
	}

	/**
	 * Put 资源进行新增或者修改
	 * @param url 
	 * @param data 
	 * @param responseType 
	 * @param headers 
	 * @param complete 
	 */
	public static put(url, data, responseType, headers, complete): void {

		let xhr: Laya.HttpRequest = new Laya.HttpRequest()
		xhr.http.timeout = 10000;//设置超时时间；
		xhr.once(Laya.Event.COMPLETE, this, complete);

		xhr.send(url, data, "put", responseType, headers);
	}

	/**
	 * Patch 对已知资源进行局部更新
	 * @param url 
	 * @param responseType 
	 * @param headers 
	 * @param complete 
	 */
	public static patch(url, responseType, headers, complete): void {

		let xhr: Laya.HttpRequest = new Laya.HttpRequest()
		xhr.http.timeout = 10000;//设置超时时间；
		xhr.once(Laya.Event.COMPLETE, this, complete);

		xhr.send(url, "", "patch", responseType, headers);
	}

	/**
	 * Delete 删除
	 * @param url 
	 * @param responseType 
	 * @param headers 
	 * @param complete 
	 */
	public static delete(url, responseType, headers, complete): void {

		let xhr: Laya.HttpRequest = new Laya.HttpRequest()
		xhr.http.timeout = 10000;//设置超时时间；
		xhr.once(Laya.Event.COMPLETE, this, complete);

		xhr.send(url, "", "delete", responseType, headers);
	}

	
}