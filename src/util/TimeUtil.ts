/*
* name;
*/
class TimeUtil{
    constructor(){

    }

    /**
		 * 毫秒转时间格式(00:00:00)
		 * @param msec
		 * @return
		 *
		 */
		public static msec2THMS3(msec:number, isContainsSec:Boolean=true):string
		{
			var sec:number=Math.floor(msec / 1000);
			var hours:number=Math.floor(sec / 3600);
			var minutes:number=Math.floor(Math.floor(sec % 3600) / 60);
			var seconds:number=Math.floor(Math.floor(sec % 3600) % 60);
			if (hours == 0)
			{
				if (isContainsSec)
				{
					return minutes + ":" + seconds;
				}
				else
				{
					return minutes + "分";
				}
			}
			else
			{
				if (isContainsSec)
				{
					return hours + ":" + minutes + ":" + seconds;
				}
				else
				{
					return hours + ":" + minutes;
				}
			}
		}
}