/*
* name;
*/
class CommonlyUsed {

    public avatar(head, avatar): any {
        switch (head) {
            case '1':
                avatar.skin = 'chooseHead/photo_01.png';
                break;
            case '2':
                avatar.skin = 'chooseHead/photo_02.png';
                break;
            case '3':
                avatar.skin = 'chooseHead/photo_03.png';
                break;
            case '4':
                avatar.skin = 'chooseHead/photo_04.png';
                break;
        }
    }

    public selectGalaxyIcon(icon, avatar): any {
        switch (icon) {
            case 0:
                avatar.skin = 'leaderBoard/photo_03.png';
                break;
            case 1:
                avatar.skin = 'leaderBoard/photo_01.png';
                break;
            case 2:
                avatar.skin = 'leaderBoard/photo_02.png';
                break;
            case 3:
                avatar.skin = 'leaderBoard/photo_03.png';
                break;
        }
    }

    //拆url
    public UrlSearch(): string {
        var name, value;
        var str = window.location.href;
        var num = str.indexOf("?")
        str = str.substr(num + 1);

        var arr = str.split("&");
        for (var i = 0; i < arr.length; i++) {
            num = arr[i].indexOf("=");
            if (num > 0) {
                name = arr[i].substring(0, num);
                value = arr[i].substr(num + 1);
                this[name] = value;
            }
        }
        return this[name];
    }
}