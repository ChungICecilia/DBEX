import Texture = laya.resource.Texture;
import NetLoader = laya.net.Loader;

/**
 * 数字滚动组件
 */
class ScrollNum extends Laya.Sprite {

    private adds: string[];
    private masks: Laya.Sprite = new Laya.Sprite();
    public cols: Array<Laya.Sprite> = [];
    private value:number=0;
    constructor() {
        super();
        

    }
    
    private resLoaded(num1: number): void {
        this.adds = NetLoader.getAtlas("res/atlas/num.atlas");
        
        this.makeCol(this.value, num1);
        this.value = num1;
    }

    private makeCol(num: number, num1: number): void {
        this.removeChildren(0,this.numChildren);
        

        var le: number = String(num1).length - String(num).length;
        let numStr: string = String(num);
        let numStr1: string = String(num1);
        if (le > 0) {
            while (le > 0) {
                numStr += "0";
                le--;
            }

        }
        
        let len: number = numStr.length;
        let txt: Texture;
        
        while (this.cols.length < len) {
            var sm: number = this.cols.length;
            let max: number = len - this.cols.length;
            let col: Laya.Sprite;

            for (let k: number = 0; k < max; k++) {

                let offY: number = 0;
                col = new Laya.Sprite();
                for (let i: number = 0; i < 30; i++) {
                    let j: number = 0;
                    if (i > 9 && i <= 19) {
                        j = i - 10;
                    } else if (i > 19) {
                        j = i - 20;
                    } else {
                        j = i;
                    }

                    txt = NetLoader.getRes(this.adds[j]);
                    col.graphics.drawTexture(txt, 0, offY, 15, 20);
                    offY += 20;



                }


            }
            this.cols.push(col);
            
        }
        let offX: number = 0;
        var dfsd: number = this.cols.length;
      
        for (let i: number = 0; i < len; i++) {
            
            let n: number = 0;
            var ss: string = numStr.substr(i, 1);
            if(ss == ".")
                n = 0;
            else
                n = parseFloat(ss);
            this.addChild(this.cols[i]);
            this.cols[i].y = -n * 20;
            this.cols[i].x = offX;
            
            
            //var anys:Laya.Sprite[] = ScrollNum.cols;
            
            
            offX += 15;
        }
        this.masks.graphics.clear();
        this.masks.graphics.drawRect(0, 0, 1500, 20, "#FFFFFF");//
        this.mask = this.masks;

       

        var le: number = String(num1).length;

        let loi: boolean = false;
        let po:Laya.Sprite;
        for (let i: number = 0; i < le; i++) {
            
            if(numStr1.substr(i, 1)==".")
            {
                
                let col:Laya.Sprite = this.cols[i];
                po = new Laya.Sprite();
                let txt:Texture = NetLoader.getRes(this.adds[10]);
                po.graphics.drawTexture(txt,0,0,15,20);
                this.removeChild(col);
                this.addChild(po);
                
                po.x = col.x;
                po.y = 0;
                continue;
            }
            var l: number = parseFloat(numStr.substr(i, 1));
            if(!l)
                l = 0;
            var l1: number = parseFloat(numStr1.substr(i, 1));

            if (l == l1) {
                if (!loi)
                    continue;
            }
            
            loi = true;
            var ge: number = 0;
            if (l1 > l) {
                ge = l1 - l;
            }
            else {
                ge = 10 - l + l1;
            }
            let col: Laya.Sprite = this.cols[i];
            Laya.Tween.to(col, { y: col.y - 20 * 10 - ge * 20 }, 1000, Laya.Ease.linearIn, null);
        }
        //
        var dd: number = this.cols.length;
        if (le < dd) {
            while (dd > le) {
                this.removeChild(this.cols[dd - 1]);
                dd--;
            }
        }

    }




    public setNum(num1: number): void {

        Laya.loader.load(["res/atlas/num.atlas"],
            new Laya.Handler(this, this.resLoaded, [num1]));
    }
}