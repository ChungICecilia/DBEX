
/*
* name;
*/
// import Texture = laya.resource.Texture;
// import NetLoader = laya.net.Loader;
class NumSprite extends Laya.Sprite{

    public totalWidth:number;
    private num:number;
    private gap:number;
    private url:string;
    private adds: string[];
    
    private txt:Texture;
    private row:Laya.Sprite = new Laya.Sprite();
    
    constructor(){
        super();
        
    }

    public setNumber(num:number,url:string,gap:number=0):void
    {
        this.num = num;
        this.gap = gap;
        this.url = url;
        this.loadRes();
    }

    //res/atlas/num.atlas
    private loadRes():void
    {
        Laya.loader.load([this.url],
            new Laya.Handler(this, this.resLoaded));
    }

    private resLoaded():void
    {
        this.adds = NetLoader.getAtlas(this.url);
        
        this.removeChildren(0,this.numChildren);
        this.make();
    }
    
    private make():void
    {
        this.row.graphics.clear();
        let str:string = this.num.toString();
        let offX:number = 0;
        let i:number;
        for(i=0;i<str.length;i++)
        {
            var n:number = Number(str.charAt(i));
            this.txt = NetLoader.getRes(this.adds[n]);
            
            this.row.graphics.drawTexture(this.txt, offX, 0, this.txt.width,this.txt.height);
            offX += (this.txt.width+this.gap);
        }
        this.addChild(this.row);
        this.totalWidth = (this.txt.width+this.gap)*(i-1)+this.txt.width;
        this.event("LoadOk");

    }

}