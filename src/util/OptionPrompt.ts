/*
* name;
*/
class OptionPrompt {
    private imgurl: string = "";
    constructor() {
        this.imgurl = "res/singleImages/tips.png";
    }

    /**
    * 单例 
    */
    private static _ins:OptionPrompt;
    public static get ins(): OptionPrompt {
        return OptionPrompt._ins ? OptionPrompt._ins:new OptionPrompt();
      
    }

    private imgCache: any[] = [];

    public play(msg: String, pos: number[] = [Laya.stage.width/2,Laya.stage.height/2]): void {
        var prompt: Laya.Image;
        if (msg == null) {
            return;
        }
        if (this.imgCache.length > 0) {
            prompt = this.imgCache.pop();
            prompt.alpha = 1;
        }
        else {
            prompt = new Laya.Image();
            prompt.sizeGrid = "21,73,28,56";
            prompt.skin = this.imgurl;

            prompt.mouseThrough = true;
            prompt.alpha = 1;
            //prompt.graphics.drawTexture(imgurl,0,0,imgurl.width,imgurl.height);
            var htmlText: Laya.Text = new Laya.Text;
            htmlText.fontSize = 24;
            htmlText.color = "#ffffff";
            htmlText.align = "center";
            //htmlText.wordWrap = true;


            prompt["txt"] = htmlText;
            prompt.addChild(htmlText);
            prompt.zOrder = 10001;
            prompt.height = 59;


        }





        prompt["txt"].text = msg;
        prompt["txt"].x = 20;
        prompt.width = prompt["txt"].textWidth + 40;

        prompt.x = pos[0] - prompt.width / 2;
        prompt.y = pos[1] - prompt.height / 2;
        Laya.stage.addChild(prompt);
        Laya.timer.once(1000, this, this.clearMsg, [prompt], false);
        //166 59


        prompt["txt"].y = (prompt.height - prompt["txt"].textHeight) / 2;









        //this.piao(prompt);




    }
    private clearMsg(msg: Laya.Image): void {
        Laya.Tween.to(msg, { alpha: 0 }, 1500, null, Laya.Handler.create(this, this.remove, [msg]));
    }

    private remove(msg: Laya.Image): void {
        msg.removeSelf();
        msg.alpha = 1;
        this.imgCache.push(msg);
    }

    private isPlay: Boolean = false;
    /**piao动画*/
    private piao(prompt: Laya.Image): void {

        Laya.Tween.to(prompt, { y: prompt.y - 300 }, 800);
        

    }
}