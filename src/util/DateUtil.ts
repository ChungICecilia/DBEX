class DateUtil {

    //返回yy:mm:dd格式时间
    public static second2String(second: number): string {

        var hour: number = Math.floor(second / 3600)
        var minute: number = Math.floor(second / 60) % 60
        var second: number = Math.floor(second % 60)

        var str: string;
        var h = hour < 10 ? ('0' + hour) : hour;
        var m = minute < 10 ? ('0' + minute) : minute;
        var s = second < 10 ? ('0' + second) : second;
            return `${h}:${m}:${s}`;
    }

    //    public static second2String(second: number): string {

    //     var hour: number = Math.floor(second / 3600)
    //     var minute: number = Math.floor(second / 60) % 60
    //     var second: number = Math.floor(second % 60)

    //     var str: string;
    //     if (hour > 0) {
    //         return `${hour}:${minute}:${second}`
    //     } else if (minute > 0) {
    //         return `${minute}:${second}`
    //     } else {
    //         return `${second}`
    //     }

    // }

    // 转成成简单的时间格式
    public static second2SimpleStr(second: number): string {

        var hour: number = Math.floor(second / 3600)
        if (hour > 0) {
            return `${hour}小时`;
        }
        var minute: number = Math.floor(second / 60) % 60
        if (minute > 0) {
            return `${minute}分钟`;
        }

        var second: number = Math.floor(second % 60)
        if (second > 0) {
            return '1分钟';
        }

        return '0分钟';

    }

    public static Timestamp(time: string): number {
        var str = time.replace(/-/g, '/');
        var date = new Date(str);
        var t = date.getTime();
        return t;
    }
}