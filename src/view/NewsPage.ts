/**Created by the LayaAirIDE*/
module view {
	import Loader = laya.net.Loader;
	import Handler = laya.utils.Handler;
	import Box = laya.ui.Box;
	import Label = laya.ui.Label;
	import Event = laya.events.Event;
	import CheckBox = laya.ui.CheckBox;
	export class NewsPage extends ui.NewsPageUI {

		private data = [];

		private game: GamePage;
		constructor(game: GamePage) {
			super();
			this.game = game;

			this.list_news.array = []
			this.btn_close.on(Laya.Event.CLICK, this, this.onClickClose);
			//this.game.on("READNID", this, this.isRead)
			console.log(Browser.window.location.href);
		}
		
		private onClickClose(): void {
			this.event("redPoint");
			this.close();
		}

		public onOpened(): void {
			this.text_total.style.align = "center";
			// this.text_total._getCSSStyle().valign = "middle";
			var html: String = "<span style='fontSize:22;vertical-align:middle;color:#666666;'>" + "已增加算力:" + "</span>";
			html += "<span style='fontSize:24;color:#6958fb'  >" + Rest.USERINFO.newsCalcPoint + "</span>";

			this.text_total.innerHTML = "" + html;
			// "<font  style='fontSize:22' color='#666666' >" + "已增加算力:" + "<font  style='fontSize:24' color='#6958fb' valign='bottom' >"
			//  + Rest.USERINFO.newsCalcPoint + "</font></font>";

			rest.news.list(0, 1, (data) => {
				this.data = data.data;
				this.list_news.array = this.data;
				console.log(data.data);
			})

			this.list_news.renderHandler = new Laya.Handler(this, this.onRender);
			this.list_news.mouseHandler = new Laya.Handler(this, this.onMouse);
		}


		private onRender(cell, index): void {
			if (index >= this.data.length) {
				return;
			}
			//根据子节点的名字title，获取子节点对象。 
			var lab_title: Laya.Text = cell.getChildByName("lab_title") as Laya.Text;
			var lab_time: Laya.Text = cell.getChildByName("lab_time") as Laya.Text;
			var img_read: Laya.Image = cell.getChildByName("img_read") as Laya.Image;
			var img_fhaveRead: Laya.Image = cell.getChildByName("img_fhaveRead") as Laya.Image;
			//label渲染列表文本（序号）
			// lab_title.text = this.data[index].title;
			// img_read.visible = false;
			// img_fhaveRead.visible = false;
			lab_title.text = this.data[index].newsTitle;
			if (this.data[index].playerReadStatus == 1) {
				img_read.visible = true;
				img_fhaveRead.visible = false;
				lab_title.color = "#333333";
			} else {
				lab_title.color = "#aeb4bb";
				img_fhaveRead.visible = true;
				img_read.visible = false;
			}
			//时间戳转换
			var date: Date = new Date();
			date.setTime(this.data[index].newsCreateTimestamp * 1000);
			lab_time.text = date.toLocaleDateString();
			lab_title.wordWrap = true;
		}

		private onMouse(e: Event, index: number): void {
			var data: Date = new Date();
			var clickOnNewsTime = data.getTime();
			localStorage.setItem('clickOnNewsTime', clickOnNewsTime + '');
			var timer = localStorage.getItem("clickOnNewsTime");
			console.log(timer);
			//鼠标单击事件触发
			if (e.type == Event.CLICK) {
				//console.log("1111")
				//判断点击事件类型,如果点中的是Box组件执行
				if ((e.target) instanceof Box) {
					// if (this.news == null) {
					// 	this.news = new view.News(this.game);
					// }
					// this.news.setNews(this.data[index])
					// this.news.popup(false, true)
					//http://39.108.117.15/news.html?newsId=7

					let strings: String[] = Browser.window.location.href.split("?");
					// this.shareLink = strings[0] + '?userCode=' + Rest.INVITECODE;
					Browser.window.location.href = strings[0] + "news.html?newsId=" + this.data[index].newsId;
					//Browser.window.open(strings[0] + "news.html?newsId=" + this.data[index].id, '_blank');
					var cell: Laya.Box = this.list_news.getCell(index) as Laya.Box;
					var lab_title: Laya.Label = cell.getChildByName("lab_title") as Laya.Label;
					lab_title.color = "#aaaaaa";
					this.list_news.refresh();
				}
			}
		}
	}
}