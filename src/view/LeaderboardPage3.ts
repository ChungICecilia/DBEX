/**Created by the LayaAirIDE*/
module view {

	import Loader = laya.net.Loader;
	import Handler = laya.utils.Handler;
	import Button = laya.ui.Button;
	import Label = laya.ui.Label;
	import Event = laya.events.Event;
	import CheckBox = laya.ui.CheckBox;
	import Box = laya.ui.Box;
	export class LeaderboardPage3 extends ui.LeaderboardPageUI {

		private commonlyUsed: CommonlyUsed;
		private rankData = [];
		private calcPointData = [];
		private nodeData = [];



		private rankListData = [];
		private rankTop = [];
		private rankPlayerRank = [];

		private calcPointListData = [];
		private calcPointTop = [];
		private calcPointPlayerRank:any;

		private nodeListData = [];
		private nodeTop = [];
		private nodePlayerRank = [];


		private game: GamePage;
		constructor(game: GamePage) {
			super();
			this.game = game;
			this.list_leaderBoard.array = [];
			this.sp_rank.visible = true;
			this.sp_calcPoint.visible = true;
			this.sp_node.visible = true;
			this.btn_close.on(Laya.Event.CLICK, this, this.onClickClose);
			this.sp_rank.on(Laya.Event.CLICK, this, this.onRank);
			this.sp_calcPoint.on(Laya.Event.CLICK, this, this.onCalcPoint);
			this.sp_node.on(Laya.Event.CLICK, this, this.onNode);
			this.commonlyUsed = new CommonlyUsed;
		}

		private onClickClose(): void {

			this.rankData = [];
			this.calcPointData = [];
			this.nodeData = [];

			this.rankListData = [];
			this.rankTop = [];
			this.rankPlayerRank = [];

			this.calcPointListData = [];
			this.calcPointTop = [];
			this.calcPointPlayerRank = -1;

			this.nodeListData = [];
			this.nodeTop = [];
			this.nodePlayerRank = [];
			this.list_leaderBoard.renderHandler = new Laya.Handler(this, this.onRenderDisabled);
			this.close();
		}

		private onRenderDisabled(cell, index): void {
		}
		public onOpened(): void {

			rest.friend.getAnking(0, 10, (data) => {
				this.rankData = data.data.yqsPlayerDTOS;
				for (var i = 0; i < 3; i++) {
					this.rankTop.push(this.rankData[i]);
				};
				for (var i = 3; i < this.rankData.length; i++) {
					this.rankListData.push(this.rankData[i]);
				};
				if (data.data.yqsPlayerRanking.ranking) {
					this.rankPlayerRank = data.data.yqsPlayerRanking.ranking;
				}
				//this.onRank();
			})

			rest.friend.bbmCalcPoint(0, 10, (data) => {
				this.calcPointData = data.data.powerWeekRankingDTOS;
				for (let i = 0; i < 3; i++) {
					this.calcPointTop.push(this.calcPointData[i]);
				};
				for (let i = 3; i < this.calcPointData.length; i++) {
					this.calcPointListData.push(this.calcPointData[i]);
				};
				this.calcPointPlayerRank = data.data.ranking;
				this.onCalcPoint();
			})


			rest.galaxy.allGalaxy((data) => {
				this.nodeData = data.data;
				for (let i = 0; i < 3; i++) {
					this.nodeTop.push(this.nodeData[i]);
				};
				for (let i = 3; i < this.nodeData.length; i++) {
					this.nodeListData.push(this.nodeData[i]);
				};
				//this.onNode();
			})

		}


		private onRank(): void {
			if (this.rankListData.length == 0 || this.rankTop.length == 0) {
				return;
			}
			this.img_rank.visible = true;
			this.img_calcPoint.visible = false;
			this.img_node.visible = false;

			this.text_myRanking.style.align = "center";
			if (this.rankPlayerRank.length == 0) {
				this.text_myRanking.innerHTML =
					"<span style='fontSize:22;vertical-align:middle;color:#666666;'>" + "抱歉您不在排行榜内:" + "</span>";
			} else {
				var html: String = "<span style='fontSize:22;vertical-align:middle;color:#666666;'>" + "我的排名:" + "</span>";
				html += "<span style='fontSize:24;color:#6958fb'  >" + this.rankPlayerRank + "</span>";
				this.text_myRanking.innerHTML = "" + html;
				// "<font  style='fontSize:22' color='#666666' >" + "我的排名:" + "<font  style='fontSize:26' color='#6958fb' >" + this.playerRank + "</font></font>";
			}

			this.commonlyUsed.avatar(this.rankTop[0].headIcon, this.img_num1);
			this.text_num1Name.text = this.rankTop[0].nickName;
			this.text_num1Quantity.text = this.rankTop[0].yqsGrade + '';
			this.img_n1.skin = "leaderBoard/suanli_icon-3.png";

			this.commonlyUsed.avatar(this.rankTop[1].headIcon, this.img_num2);
			this.text_num2Name.text = this.rankTop[1].nickName;
			this.text_num2Quantity.text = this.rankTop[1].yqsGrade + '';
			this.img_n2.skin = "leaderBoard/suanli_icon-3.png";

			this.commonlyUsed.avatar(this.rankTop[2].headIcon, this.img_num3);
			this.text_num3Name.text = this.rankTop[2].nickName;
			this.text_num3Quantity.text = this.rankTop[2].yqsGrade + '';
			this.img_n3.skin = "leaderBoard/suanli_icon-3.png";

			this.list_leaderBoard.array = this.rankListData;
			this.list_leaderBoard.renderHandler = new Laya.Handler(this, this.onRenderRank);
		}

		private onCalcPoint(): void {
			if (this.calcPointTop.length == 0) {
				return;
			}

			this.img_rank.visible = false;
			this.img_calcPoint.visible = true;
			this.img_node.visible = false;

			this.text_myRanking.style.align = "center";
			if (this.calcPointPlayerRank == null || this.calcPointPlayerRank==-1) {
				this.text_myRanking.innerHTML =
					"<span style='fontSize:22;vertical-align:middle;color:#666666;'>" + "抱歉您不在排行榜内:" + "</span>";
			} else {
				var html: String = "<span style='fontSize:22;vertical-align:middle;color:#666666;'>" + "我的排名:" + "</span>";
				html += "<span style='fontSize:24;color:#6958fb'  >" + this.calcPointPlayerRank.ranking + "</span>";
				this.text_myRanking.innerHTML = "" + html;
			}
			if(this.calcPointTop.length>0)
			{
				this.commonlyUsed.avatar(this.calcPointTop[0].headIcon, this.img_num1);
				this.text_num1Name.text = this.calcPointTop[0].nickName;
				this.text_num1Quantity.text = this.calcPointTop[0].addValue + '';
				this.img_n1.skin = "leaderBoard/suanli_icon.png";
			}
			
			if(this.calcPointTop.length>1)
			{
				this.commonlyUsed.avatar(this.calcPointTop[1].headIcon, this.img_num2);
				this.text_num2Name.text = this.calcPointTop[1].nickName;
				this.text_num2Quantity.text = this.calcPointTop[1].addValue + '';
				this.img_n2.skin = "leaderBoard/suanli_icon.png";
			}

			if(this.calcPointTop.length>2)
			{
				this.commonlyUsed.avatar(this.calcPointTop[2].headIcon, this.img_num3);
				this.text_num3Name.text = this.calcPointTop[2].nickName;
				this.text_num3Quantity.text = this.calcPointTop[2].addValue + '';
				this.img_n3.skin = "leaderBoard/suanli_icon.png";
			}
			
			
			this.list_leaderBoard.array = this.calcPointListData;
			this.list_leaderBoard.renderHandler = new Laya.Handler(this, this.onRenderCalcPoint);
		}

		private onNode(): void {
			if (this.nodeListData.length == 0 || this.nodeTop.length == 0) {
				return;
			}

			this.img_rank.visible = false;
			this.img_calcPoint.visible = false;
			this.img_node.visible = true;
			this.text_myRanking.innerHTML =
				"<span style='fontSize:22;vertical-align:middle;color:#666666;'>" + "" + "</span>";
			// this.text_myRanking.style.align = "center";
			// if (this.calcPointPlayerRank == null) {
			// 	this.text_myRanking.innerHTML =
			// 		"<span style='fontSize:22;vertical-align:middle;color:#666666;'>" + "抱歉您所在的节点不在排行榜内:" + "</span>";
			// } else {
			// 	var html: String = "<span style='fontSize:22;vertical-align:middle;color:#666666;'>" + "您所在的节点排名:" + "</span>";
			// 	html += "<span style='fontSize:24;color:#6958fb'  >" + this.nodePlayerRank + "</span>";
			// 	this.text_myRanking.innerHTML = "" + html;
			// }

			this.commonlyUsed.selectGalaxyIcon(this.nodeTop[0].rankLevel, this.img_num1);
			this.text_num1Name.text = this.nodeTop[0].galaxyName;
			this.text_num1Quantity.text = this.nodeTop[0].galaxyPower + '';
			this.img_n1.skin = "leaderBoard/suanli_icon.png";

			this.commonlyUsed.selectGalaxyIcon(this.nodeTop[1].rankLevel, this.img_num2);
			this.text_num2Name.text = this.nodeTop[1].galaxyName;
			this.text_num2Quantity.text = this.nodeTop[1].galaxyPower + '';
			this.img_n2.skin = "leaderBoard/suanli_icon.png";

			this.commonlyUsed.selectGalaxyIcon(this.nodeTop[2].rankLevel, this.img_num3);
			this.text_num3Name.text = this.nodeTop[2].galaxyName;
			this.text_num3Quantity.text = this.nodeTop[2].galaxyPower + '';
			this.img_n3.skin = "leaderBoard/suanli_icon.png";

			this.list_leaderBoard.array = this.nodeListData;
			this.list_leaderBoard.renderHandler = new Laya.Handler(this, this.onRenderPerNode);
		}



		private onRenderRank(cell, index): void {
			if (index >= this.rankListData.length) {
				return;
			}
			var text_friendName: Laya.Label = cell.getChildByName("text_friendName") as Laya.Label;
			var lab_fame: Laya.Label = cell.getChildByName("lab_fame") as Laya.Label;
			var img_avatar: Laya.Image = cell.getChildByName("img_avatar") as Laya.Image;
			var text_quantity: Laya.Text = cell.getChildByName("text_quantity") as Laya.Text;
			var text_rankNum: Laya.Text = cell.getChildByName("text_rankNum") as Laya.Text;
			var text_t: Laya.Text = cell.getChildByName("text_t") as Laya.Text;
			var img_icon: Laya.Image = cell.getChildByName("img_icon") as Laya.Image;

			text_friendName.text = this.rankListData[index].nickName;
			text_rankNum.text = this.rankListData[index].ranking + '';
			this.commonlyUsed.avatar(this.rankListData[index].headIcon, img_avatar);
			text_quantity.text = this.rankListData[index].yqsGrade + '';
			text_t.text = "等级";
			img_icon.skin = "leaderBoard/suanli_icon-3.png";
		}

		private onRenderCalcPoint(cell, index): void {
			if (index >= this.calcPointListData.length) {
				return;
			}
			var text_friendName: Laya.Label = cell.getChildByName("text_friendName") as Laya.Label;
			var lab_fame: Laya.Label = cell.getChildByName("lab_fame") as Laya.Label;
			var img_avatar: Laya.Image = cell.getChildByName("img_avatar") as Laya.Image;
			var text_quantity: Laya.Text = cell.getChildByName("text_quantity") as Laya.Text;
			var text_rankNum: Laya.Text = cell.getChildByName("text_rankNum") as Laya.Text;
			var text_t: Laya.Text = cell.getChildByName("text_t") as Laya.Text;
			var img_icon: Laya.Image = cell.getChildByName("img_icon") as Laya.Image;

			text_friendName.text = this.calcPointListData[index].nickName;
			text_rankNum.text = this.calcPointListData[index].ranking + '';
			this.commonlyUsed.avatar(this.calcPointListData[index].headIcon, img_avatar);
			text_quantity.text = this.calcPointListData[index].addValue + '';
			text_t.text = "算力";
			img_icon.skin = "leaderBoard/suanli_icon.png";
		}

		private onRenderPerNode(cell, index): void {
			if (index >= this.nodeListData.length) {
				return;
			}
			var text_friendName: Laya.Label = cell.getChildByName("text_friendName") as Laya.Label;
			var lab_fame: Laya.Label = cell.getChildByName("lab_fame") as Laya.Label;
			var img_avatar: Laya.Image = cell.getChildByName("img_avatar") as Laya.Image;
			var text_quantity: Laya.Text = cell.getChildByName("text_quantity") as Laya.Text;
			var text_rankNum: Laya.Text = cell.getChildByName("text_rankNum") as Laya.Text;
			var text_t: Laya.Text = cell.getChildByName("text_t") as Laya.Text;
			var img_icon: Laya.Image = cell.getChildByName("img_icon") as Laya.Image;
			text_friendName.text = this.nodeListData[index].galaxyName;
			text_rankNum.text = this.nodeListData[index].rankPosi + '';
			this.commonlyUsed.selectGalaxyIcon(this.nodeListData[index].rankLevel, img_avatar);
			text_quantity.text = this.nodeListData[index].galaxyPower + '';
			text_t.text = "算力";
			img_icon.skin = "leaderBoard/suanli_icon.png";
		}
		private onMouse(e: Event, index: number): void {
			if (e.type == Event.CLICK) {

				if ((e.target) instanceof Box) {
					//console.log(index)
				}
			}
		}
	}
}
