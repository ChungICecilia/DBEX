/**Created by the LayaAirIDE*/
module view {
	export class EnterTheGamePage extends ui.EnterTheGamePageUI {

		private commonlyUsed: CommonlyUsed;
		constructor() {
			super();
			this.commonlyUsed = new CommonlyUsed;
			this.btn_login.on(Laya.Event.CLICK, this, this.onSubmit)
		}

		private onSubmit(): void {
			this.ani_EnterTheGameLoading.visible = true;
			if(this.ani_EnterTheGameLoading.visible){
				this.btn_login.disabled = true;
			}
			this.ani_EnterTheGameLoading.play(0, true, "ani1");
			var inviteCode = this.commonlyUsed.UrlSearch();
			this.btn_login.disabled =true;
			rest.user.userInfo(inviteCode, (data => {
				console.log(data)
				if (data.status == "error") {
					this.event('ONLOGIN');
				} else {
					Rest.USERINFO = data.data.player;
					Rest.SKIP = data.data.isSkip;
					this.event('GO')
					// this.ani_EnterTheGameLoading.clear();
					rest.user.coinsList(param => {
						console.log(param);
						if(param.status == "error")
						{
							console.log("拉取coins信息失败");
						}else
						{
							Rest.COINSLIST = param.data;
						}
					});
				}
			}))
		}

		// 关闭登录
		public close(): void {

			if (this.parent != null) {
				this.parent.removeChild(this);
			}
		}
	}
}