/**Created by the LayaAirIDE*/
module view {

	import Loader = laya.net.Loader;
	import Handler = laya.utils.Handler;
	import Button = laya.ui.Button;
	import Label = laya.ui.Label;
	import Event = laya.events.Event;
	import CheckBox = laya.ui.CheckBox;
	import Box = laya.ui.Box;
	export class FriendPage extends ui.FriendPageUI {

		private commonlyUsed: CommonlyUsed;
		private data = [];
		private tips: number;

		private shareLink = '';
		private inviteHref;
		private copyDiv;
		private copy;

		private game: GamePage;
		constructor(game: GamePage) {
			super();
			this.game = game;
			
			let strings: String[] = Browser.window.location.href.split("?");
			this.shareLink = strings[0] + '?userCode=' + Rest.INVITECODE;
			this.inviteHref = Laya.Browser.window.document.getElementById("bar");
			this.copyDiv = Laya.Browser.window.document.getElementById("addFriend_btn");
			this.copy = Laya.Browser.window.document.getElementById("copy1_div");
			this.btn_close.on(Laya.Event.CLICK, this, this.onClickClose);
			this.list_friends.array = [];
			this.tips = 0;
			this.commonlyUsed = new CommonlyUsed;
		}
		private onClickClose(): void {
			this.hiddenCopyBtn();
			this.close();
		}
		public onOpened(): void {
			this.showCopyBtn();
			this.text_wateringFriendsTimes.text = Rest.USERINFO.wateringFriendsTimes + '/50';
			rest.friend.list(0, 10, (data) => {
				if (data.data.pageResult) {
					this.data = data.data.pageResult;
					this.list_friends.array = this.data;
				}
				//console.log(data);
			})
			this.list_friends.renderHandler = new Laya.Handler(this, this.onRender);
			this.list_friends.mouseHandler = new Laya.Handler(this, this.onMouse);
		}

		private showCopyBtn() {
			if (this.inviteHref && this.copyDiv) {
				this.copyDiv.style.visibility = "visible";
				this.inviteHref.style.visibility = "visible";
				this.inviteHref.innerHTML = this.shareLink;
				console.log(this.inviteHref.text);
			}
		}
		private hiddenCopyBtn() {
			if (this.inviteHref && this.copyDiv) {
				this.copyDiv.style.visibility = "hidden";
				this.inviteHref.style.visibility = "hidden";
			}
		}

		private onRender(cell, index): void {
			if (index >= this.data.length) {
				return;
			}

			var lab_friendName: Laya.Label = cell.getChildByName("lab_friendName") as Laya.Label;
			var lab_fame: Laya.Label = cell.getChildByName("lab_fame") as Laya.Label;
			var img_avatar: Laya.Image = cell.getChildByName("img_avatar") as Laya.Image;
			var btn_water: Laya.Button = cell.getChildByName("btn_water") as Laya.Button;
			var btn_waterOK: Laya.Button = cell.getChildByName("btn_waterOK") as Laya.Button;
			var img_water: Laya.Image = btn_water.getChildByName("img_water") as Laya.Image;
			var text_addQuantity: Laya.Text = cell.getChildByName("text_addQuantity") as Laya.Text;
			var lab_bbm: Laya.Label = cell.getChildByName("lab_bbm") as Laya.Label;
			var lab_bbmPointTotal: Laya.Label = cell.getChildByName("lab_bbmPointTotal") as Laya.Label;

			lab_friendName.text = this.data[index].nickName;
			lab_bbm.text = this.data[index].bbmTotal + '';
			lab_bbmPointTotal.text = this.data[index].calcPointTotal + '';
			if (this.data[index].wateringCount == 0) {
				btn_water.visible = true;
				btn_waterOK.visible = false;
				text_addQuantity.visible = false;
			} else {
				btn_water.visible = false;
				btn_waterOK.visible = true;
				text_addQuantity.visible = true;
			}
			this.commonlyUsed.avatar(this.data[index].headIcon, img_avatar);
		}

		private onMouse(e: Event, index: number): void {
			if (e.type == Event.CLICK) {
				let cell = e.target;
				if (cell instanceof Button) {

					var friendId = this.data[index].id
					rest.work.wateringFriendsTree({ friendId }, (data => {
						if (data.status == "error") {
							this.showErrorTips(data.msg);
						} else {
							Rest.USERINFO = data.data;
							this.text_wateringFriendsTimes.text = Rest.USERINFO.wateringFriendsTimes + '/50';
							this.data[index].wateringCount = 1;
							this.list_friends.array = this.data;
							this.game.event('UPDATEUSER');
						}
					}))
				}
			}
		}

		private showErrorTips(msg: string): void {
			this.tips = 3;
			this.img_friendTips.visible = true;
			this.img_friendTips.width = msg.length * 30;
			this.text_friendTips.width = msg.length * 30;
			this.img_friendTips.centerX = 1;
			this.text_friendTips.text = msg;
			Laya.timer.loop(1000, this, this.tipsLoop);
		}


		private tipsLoop(): void {
			this.tips--;
			if (this.tips <= 0) {
				this.img_friendTips.visible = false;
				Laya.timer.clear(this, this.tipsLoop);
			}
		}
	}
}
