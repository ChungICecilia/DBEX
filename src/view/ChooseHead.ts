/**Created by the LayaAirIDE*/
module view {
	export class ChooseHead extends ui.ChooseHeadUI {

		public head: number;
		private game: GamePage;
		private tips: number;
		private commonlyUsed: CommonlyUsed;
		private ChangeGalaxyPage: view.ChangeGalaxyPage;
		constructor(game: GamePage) {
			super();
			this.game = game;
			this.nickname.text = "";
			this.head = 1;
			this.tips = 0;
			this.commonlyUsed = new CommonlyUsed;
			this.btn_confirm.on(Laya.Event.CLICK, this, this.onCreateRole)

			this.img_head1.on(Laya.Event.CLICK, this, this.onPickHead1);
			this.img_head2.on(Laya.Event.CLICK, this, this.onPickHead2);
			this.img_head3.on(Laya.Event.CLICK, this, this.onPickHead3);
			this.img_head4.on(Laya.Event.CLICK, this, this.onPickHead4);
		}


		private onPickHead1(): void {
			this.onPickHead(1);
		}

		private onPickHead2(): void {
			this.onPickHead(2);
		}

		private onPickHead3(): void {
			this.onPickHead(3);
		}

		private onPickHead4(): void {
			this.onPickHead(4);
		}

		private onPickHead(num): void {
			switch (num) {
				case 1:
					this.head = 1;
					this.img_pick1.visible = true;
					this.img_pick2.visible = false;
					this.img_pick3.visible = false;
					this.img_pick4.visible = false;
					break;
				case 2:
					this.head = 2;
					this.img_pick1.visible = false;
					this.img_pick2.visible = true;
					this.img_pick3.visible = false;
					this.img_pick4.visible = false;
					break;
				case 3:
					this.head = 3;
					this.img_pick1.visible = false;
					this.img_pick2.visible = false;
					this.img_pick3.visible = true;
					this.img_pick4.visible = false;
					break;
				case 4:
					this.head = 4;
					this.img_pick1.visible = false;
					this.img_pick2.visible = false;
					this.img_pick3.visible = false;
					this.img_pick4.visible = true;
					break;
			}


		}

		private onCreateRole(): void {

			//var nickname = Laya.Browser.window._.trim(this.nickname.text);
			if (!this.nickname.text || this.nickname.text.length == 0) {
				return;
			}
			rest.user.createRole({
				userName: this.nickname.text,
				headIcon: this.head,
				invite_code: this.commonlyUsed.UrlSearch(),
			}, data => {
				//console.log(data)
				if (data.status == "error") {
					this.showErrorTips(data.msg);
				} else {
					Rest.USERINFO = data.data;
					//console.log(Rest.USERINFO);
					this.addGalaxy();
					this.close();
				}

				rest.user.coinsList(param => {
					console.log(param);
					if(param.status == "error")
					{
						console.log("拉取coins信息失败");
					}else
					{
						Rest.COINSLIST = param.data;
					}
				})
			})

		}
		private addGalaxy() {
			if (this.ChangeGalaxyPage == null) {
				this.ChangeGalaxyPage = new view.ChangeGalaxyPage(this);
			}
			Laya.stage.addChild(this.ChangeGalaxyPage);
		}

		private showErrorTips(msg: string): void {
			this.tips = 3;
			this.img_createPlayer.visible = true;
			this.img_createPlayer.width = msg.length * 30;
			this.text_createPlayer.width = msg.length * 30;
			this.img_createPlayer.centerX = 1;
			this.text_createPlayer.text = msg;
			Laya.timer.loop(1000, this, this.tipsLoop);
		}

		private tipsLoop(): void {
			this.tips--;
			if (this.tips <= 0) {
				this.img_createPlayer.visible = false;
				Laya.timer.clear(this, this.tipsLoop);
			}
		}

	}
}