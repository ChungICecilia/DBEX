/**Created by the LayaAirIDE*/
module view {

	import Loader = laya.net.Loader;
	import Handler = laya.utils.Handler;
	import Button = laya.ui.Button;
	import Label = laya.ui.Label;
	import Event = laya.events.Event;
	import CheckBox = laya.ui.CheckBox;
	import Box = laya.ui.Box;
	export class LeaderboardPage extends ui.LeaderboardPageUI {

		private commonlyUsed: CommonlyUsed;
		private data = [];
		private listData = [];
		private topThree = [];
		private playerRank = [];

		private game: GamePage;
		constructor(game: GamePage) {
			super();
			this.game = game;
			this.list_leaderBoard.array = []
			this.btn_close.on(Laya.Event.CLICK, this, this.onClickClose);
			//this.btn_tool.on(Laya.Event.CLICK, this, this.onClickTool);

			this.commonlyUsed = new CommonlyUsed;
		}

		private onClickClose(): void {
			this.listData = [];
			this.topThree = [];
			this.playerRank = [];
			this.list_leaderBoard.renderHandler = new Laya.Handler(this, this.onRenderDisabled);
			this.close();
		}

		public onOpened(): void {

			rest.friend.bbmCalcPoint(0, 10, (data) => {

				this.data = data.data.topRank;
				this.text_myRanking.style.align = "center";
				// this.text_myRanking._getCSSStyle().valign = "middle";
				if (data.data.playerRank == null) {
					this.text_myRanking.innerHTML =
						"<font  style='fontSize:22' color='#666666' 'bold = true'>" + "抱歉您不在排行榜内" + "</font>";
				} else {
					this.playerRank = data.data.playerRank.playerRank;
					var html: String = "<span style='fontSize:22;vertical-align:middle;color:#666666;'>" + "我的排名:" + "</span>";
					html += "<span style='fontSize:24;color:#6958fb'  >" + this.playerRank + "</span>";
					this.text_myRanking.innerHTML = "" + html;
					// "<font  style='fontSize:22' color='#666666' >" + "我的排名:" + "<font  style='fontSize:26' color='#6958fb' >" + this.playerRank + "</font></font>";

				}

				for (var i = 0; i < 3; i++) {
					this.topThree.push(this.data[i]);
				};
				for (var i = 3; i < this.data.length; i++) {
					this.listData.push(this.data[i]);
				};
				console.log(this.topThree);
				console.log(this.listData);
				this.commonlyUsed.avatar(this.data[0].playerHeadIcon, this.img_num1);
				this.text_num1Name.text = this.topThree[0].playerNickName;
				this.text_num1Quantity.text = this.topThree[0].playerBbmCalcPoint + '';

				this.commonlyUsed.avatar(this.data[1].playerHeadIcon, this.img_num2);
				this.text_num2Name.text = this.topThree[1].playerNickName;
				this.text_num2Quantity.text = this.topThree[1].playerBbmCalcPoint + '';

				this.commonlyUsed.avatar(this.data[2].playerHeadIcon, this.img_num3);
				this.text_num3Name.text = this.topThree[2].playerNickName;
				this.text_num3Quantity.text = this.topThree[2].playerBbmCalcPoint + '';

				this.list_leaderBoard.array = this.listData;
				this.list_leaderBoard.renderHandler = new Laya.Handler(this, this.onRender);
				this.list_leaderBoard.mouseHandler = new Laya.Handler(this, this.onMouse);
			})
		}

		private onRender(cell, index): void {
			if (index >= this.data.length) {
				return;
			}
			var text_friendName: Laya.Label = cell.getChildByName("text_friendName") as Laya.Label;
			var lab_fame: Laya.Label = cell.getChildByName("lab_fame") as Laya.Label;
			var img_avatar: Laya.Image = cell.getChildByName("img_avatar") as Laya.Image;
			var text_quantity: Laya.Text = cell.getChildByName("text_quantity") as Laya.Text;
			var text_rankNum: Laya.Text = cell.getChildByName("text_rankNum") as Laya.Text;
			text_friendName.text = this.listData[index].playerNickName;
			text_rankNum.text = this.listData[index].playerRank + '';
			this.commonlyUsed.avatar(this.listData[index].playerHeadIcon, img_avatar)
			text_quantity.text = this.listData[index].playerBbmCalcPoint + '';
		}


		private onRenderDisabled(cell, index): void {

		}

		private onMouse(e: Event, index: number): void {
			if (e.type == Event.CLICK) {

				if ((e.target) instanceof Box) {
					//console.log(index)
				}
			}
		}
	}
}
