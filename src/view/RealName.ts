module view {
    import Browser = Laya.Browser;
    export class RealName extends ui.RealNameUI {
        private tips: number;
        constructor() {
            super();
            this.tips = 0;
            this.btn_close.on(Laya.Event.CLICK, this, this.onClickClose);
            this.confirmBtn.on(Laya.Event.CLICK, this, this.onClickConfirm);
        }
        private onClickClose() {
            this.removeSelf();
        }
        private onClickConfirm() {
            if (this.nameText.text == "" || this.cardText.text == "") {
                return;
            }
            rest.user.realName({
                name: this.nameText.text,
                card: this.cardText.text,
            }, data => {
                console.log(data)
                if (data.status == "error") {
                    this.showErrorTips(data.msg);
                } else {
                    Rest.USERINFO = data.data;
                    //console.log(Rest.USERINFO);
                    this.onClickClose();
                    this.event("HIDE");
                }
            })
        }
        private showErrorTips(msg: string): void {
            this.tips = 3;
            this.tips = 3;
            this.img_singn.visible = true;
            this.text_tips.width = msg.length * 30;
            this.img_singn.width = msg.length * 30;
            this.img_singn.centerX = 1;
            this.text_tips.text = msg;
            Laya.timer.loop(1000, this, this.tipsLoop);
        }
        private tipsLoop(): void {
            this.tips--;
            if (this.tips <= 0) {
                this.img_singn.visible = false;
                Laya.timer.clear(this, this.tipsLoop);
            }
        }
    }
}