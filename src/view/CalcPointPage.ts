/**Created by the LayaAirIDE*/
module view {

	import Browser = Laya.Browser;
	export class CalcPointPage extends ui.CalcPointPageUI {
		private game: GamePage;
		private rcord = []
		constructor(game: GamePage) {
			super();
			this.game = game;

			this.btn_close.on(Laya.Event.CLICK, this, this.onClickClose);
			this.btn_wallet.on(Laya.Event.CLICK, this, this.onClickWallet);
		}

		private onClickClose(): void {
			this.close();
		}

		private onClickWallet(): void {
			Browser.window.location.href = "http://www.bbb-home.com/appDownload.html";
		}

		public onOpened(): void {
			//持有bbm数量
			// Rest.USERINFO.marketBbm != null ? this.text_bbm.text = (Rest.USERINFO.marketBbm).toFixed(4) : this.text_bbm.text = '0';
			if (Rest.USERINFO.marketBbm > 0) {
				this.img_noBBM.visible = false;
				this.text_noBBM.visible = false;
				this.zero_dbexImg.visible = false;
				// this.text_bbm.visible = true;
				this.hold_dbexHtml.visible = true;
				this.hold_dbexHtml.style.align = "center";
				var html: String = "<span style='fontSize:30;vertical-align:top;color:#713a0e;'>" + "持有" + "</span>";
				html += "<img src='calcPoint/01.png'></img>";
				html += "<span style='fontSize:30;vertical-align:top;color:#713a0e;'  >" + "DBEX:" + "</span>";
				html += "<span style='fontSize:30;color:#713a0e'  >" + (Rest.USERINFO.marketBbm).toFixed(4) + "</span>";
				this.hold_dbexHtml.innerHTML = html + "";
				this.img_haveBBM.visible = true;
				this.html_calcPoint.visible = true;
				this.html_calcPoint.style.align = "center";
				this.html_calcPoint.width = 492;
				this.html_calcPoint.innerHTML =
					"<font style='fontSize:30' color='#713a0e'>" + "增加了" + Rest.USERINFO.marketCalcPoint + "算力" + "<br/></font>";
			} else {
				this.img_noBBM.visible = true;
				this.text_noBBM.visible = true;
			}
		}

	}
}