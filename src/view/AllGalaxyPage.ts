module view {

    import Event = laya.events.Event;
    import Box = laya.ui.Box;
    export class AllGalaxyPage extends ui.AllGalaxyPageUI {
        private data = [ ];
        private chooseHead: ChooseHead;
        private tips: number;
        private galaxyId: number;
        constructor(chooseHead: ChooseHead) {
            super();
            this.chooseHead = chooseHead;
            this.list_galaxy.array = [];
            this.enterGameBtn.on(Laya.Event.CLICK, this, this.onEenterGame);
            this.text_back.on(Laya.Event.CLICK, this, this.onGoBack);
            this.getGroomGalaxy();

        }
        private getGroomGalaxy() {
            rest.galaxy.allGalaxy((data) => {
                if (data.status == "error") {
                    this.showErrorTips(data.msg);
                } else {
                    this.data = data.data;
                    this.onOpened();
                }
            })

        }
        public onOpened(): void {
            console.log(this.data);
            this.list_galaxy.array = this.data;
            // this.list_galaxy.height = this.data.length * 120;
            this.list_galaxy.renderHandler = new Laya.Handler(this, this.onRender);
            this.list_galaxy.mouseHandler = new Laya.Handler(this, this.onmouse);
            // if (this.list_galaxy.cells[0]) {
            //     var img_pick: Laya.Image = this.list_galaxy.cells[0].getChildByName("img_pick") as Laya.Image;
            //     img_pick.visible = true;
            //     this.galaxyId = this.data[0].id;
            // }
            
        }

        private onRender(cell, index): void {
            if (index >= this.data.length) {
                return;
            }
            var img_icon: Laya.Image = cell.getChildByName("img_icon") as Laya.Image;
            var text_galaxy: Laya.Text = cell.getChildByName("text_galaxy") as Laya.Text;
            //var text_region: Laya.Text = cell.getChildByName("text_region") as Laya.Text;
            var text_computingPower: Laya.Text = cell.getChildByName("text_computingPower") as Laya.Text;
            var text_playerNumer: Laya.Text = cell.getChildByName("text_playerNumer") as Laya.Text;
            var img_pick: Laya.Image = cell.getChildByName("img_pick") as Laya.Image;
            img_pick.visible = false;
            switch (this.data[index].rankLevel) {
                case 1:
                    img_icon.skin = "changeGalaxy/xingqiu_high.png";
                    text_computingPower.text = "高";
                    text_computingPower.color = "#f48731";
                    break;
                case 2:
                    img_icon.skin = "changeGalaxy/xingqiu_mid.png";
                    text_computingPower.text = "中";
                    text_computingPower.color = "#6958fb";
                    break;
                case 3:
                    img_icon.skin = "changeGalaxy/xingqiu_low.png";
                    text_computingPower.text = "低";
                    text_computingPower.color = "#18bd36";
                    break;
            }
            text_galaxy.text = this.data[index].galaxyName;
            //text_region.text = this.data[index].region;
            text_playerNumer.text = this.data[index].galaxyPlayers + '';
            if (index == 0) {
                img_pick.visible = true;
                this.galaxyId = this.data[0].id;
            }

        }

        private onmouse(e: Event, index: number): void {
            console.log(index);
            if (e.type == Event.CLICK) {
                if ((e.target) instanceof Box) {
                    for (var i = 0; i < this.list_galaxy.cells.length; i++) {
                        var img = this.list_galaxy.cells[i].getChildByName("img_pick") as Laya.Image;
                        img.visible = false;
                    }
                    var img_pick: Laya.Image = (e.target).getChildByName("img_pick") as Laya.Image;
                    img_pick.visible = true;
                    this.galaxyId = this.data[index].id;
                }
            }
        }

        private onGoBack(): void {
            if (this.parent != null) {
                this.removeSelf();
            }
        }

        private onEenterGame() {
            this.ani_beginGameLoading.visible = true;
            this.ani_beginGameLoading.play(0,true,"ani1");
            this.enterGameBtn.disabled = true;
            this.text_back.mouseEnabled = false;
            rest.galaxy.setGalaxy({
                galaxyld: this.galaxyId,
            }, data => {
                //console.log(data)
                if (data.status == "error") {
                    this.showErrorTips(data.msg);
                    this.ani_beginGameLoading.clear();
                } else {
                    Rest.USERINFO = data.data;
                    //console.log(Rest.USERINFO);
                   if(this.chooseHead){
                        this.chooseHead.event("CreatedRole")
                    }
                    this.event("CLOSE");
                    this.parent.removeChild(this);
                }
            })

        }
        private showErrorTips(msg: string): void {
            this.tips = 3;
            this.img_createPlayer.visible = true;
            this.img_createPlayer.width = msg.length * 30;
            this.text_createPlayer.width = msg.length * 30;
            this.img_createPlayer.centerX = 1;
            this.text_createPlayer.text = msg;
            Laya.timer.loop(1000, this, this.tipsLoop);
        }
        private tipsLoop(): void {
            this.tips--;
            if (this.tips <= 0) {
                 if(this.enterGameBtn.disabled){
					this.enterGameBtn.disabled = false;
                    this.text_back.mouseEnabled = true;
				}
                this.img_createPlayer.visible = false;
                Laya.timer.clear(this, this.tipsLoop);
            }
        }
    }
}