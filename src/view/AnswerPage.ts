module view {
    export class AnswerPage extends ui.AnswerPageUI {
        private game: GamePage;
        private questionId: number = 0;
        private topics: number[] = [];
        private cur: number = 0;
        private answer: any[] = [];
        private sn: number = 0;;
        
        private power: number = 0; //每答对一题所加战力
        private submitObj: Object = {};
        private curErr: number = 0;
        private curErrTopic: any[] = [];
        private rewardState: number = 0;
        private susNumber: number = 0; //答对的题目数
        private errorNumber: number = 0;//答错的题目数
        private addForce: number = 0;//增加的战力
        private prizeForce: number = 0;//每答对一题奖励算力
        private rightNum: number = 0; //答对多少题后

       
        constructor(game: GamePage) {

            super();
            //  Laya.loader.load(["res/atlas/answer.atlas"],// "res/atlas/game.atlas", 
            //     Laya.Handler.create(this, this.onLoaded), null, Laya.Loader.ATLAS);
            this.game = game;
            


        }

        protected initialize(): void {
            super.initialize();
            this.btn_close.on(Laya.Event.CLICK, this, this.onClickClose);
            this.btn_next.on(Laya.Event.CLICK, this, this.onNextClick);
            this.btn_start.on(Laya.Event.CLICK, this, this.onStartClick);
            this.list_answer.renderHandler = new Laya.Handler(this, this.onListRender);
            this.list_errAnswer.renderHandler = new Laya.Handler(this, this.onErrListRender);
            this.btn_getForce.on(Laya.Event.CLICK, this, this.onForceClick);
            this.btn_check.on(Laya.Event.CLICK, this, this.onCheckClick);
            this.btn_nextErr.on(Laya.Event.CLICK, this, this.onErrNextClick);
            this.list_answer.selectEnable = true;
            this.list_answer.selectHandler = new Laya.Handler(this, this.listSelectHandler);
            this.txt_add.style.bold = true;
            this.txt_correct.style.align = "center";
            //this.list_answer.
            

        }


        private onLoaded(): void {
            let i: number = 3;
        }

        private onClickClose(e: Laya.Event): void {
           
            this.close();
        }

        private optionPrompt: OptionPrompt = new OptionPrompt();
        private recTopics: any[] = [];
        //private curSn:number = 0;
        private onNextClick(e: Laya.Event): void {

            //this.getTopic(this.questionId, this.topics[this.sn]);
            Laya.timer.clear(this, this.countTime);
            LoadingPage.loading();
            //this.btn_next.mouseEnabled = false;
            rest.answer.setAnswerTime(this.questionId, this.topics[this.sn], (data) => {
                //console.log(name + "__" + password)
                //console.log(data)
                
                if (data.status == "error") {
                    // this.ani_loginLoading.clear();
                    // this.showErrorTips(data.msg);
                    this.optionPrompt.play(data.msg, [Laya.stage.width / 2, Laya.stage.height / 2]);
                } else {

                    //this.event('LoginOK');
                    // this.ani_loginLoading.clear();
                    var ans: Object = {};
                    ans["topicId"] = this.topics[this.sn];
                    ans["answerId"] = this.getAnswer();

                    this.submitObj["answerLogDTOList"].push(ans);

                    this.sn++;
                    if (this.sn >= this.topics.length) {
                        this.submit();
                    } else {
                        this.getTopic(this.questionId, this.topics[this.sn]);
                    }


                }
                Http.DATA_TYPE = 'FORMDATA';
                // this.setQuestion(datas);
            })


        }


        private onErrNextClick(): void {

            this.curErr++;
            if (this.curErr >= this.curErrTopic.length) {
                //返回
                this.viewStack.selectedIndex = 3;
                return;
            }


            this.selectErr(this.curErr);

        }

        private onForceClick(): void {
            rest.answer.force(this.questionId, (data) => {
                if (data.code == 1) {
                    // this.ani_loginLoading.clear();
                    // this.showErrorTips(data.msg);
                    this.optionPrompt.play("领取成功", [Laya.stage.width / 2, Laya.stage.height / 2]);

                    let inviteCode: '';
                    rest.user.userInfo(inviteCode, (data) => {
                        Rest.USERINFO = data.data.player;
                        Rest.GALAXY = data.data.byDbexGalaxy;
                        this.game.onCreatedRole();
                    })
                } else {

                    this.optionPrompt.play(data.msg, [Laya.stage.width / 2, Laya.stage.height / 2]);
                }
            });
        }

        private onCheckClick(): void {
            this.errPage();
        }


        private getAnswer(): any[] {
            var ids: number[] = [];
            var sprites: Laya.Sprite[] = this.list_answer.cells;
            var spri: Laya.Sprite;
            for (var i: number = 0; i < sprites.length; i++) {
                spri = sprites[i];
                var cheBox: laya.ui.CheckBox = spri.getChildByName("check") as laya.ui.CheckBox;
                if (cheBox.selected) {
                    var ansId: number = this.list_answer.getItem(i).answerId;
                    ids.push(ansId);
                }


            }
            return ids;
        }

        private onStartClick(): void {
            
            this.viewStack.selectedIndex = 1;
            this.sn = 0;
            LoadingPage.loading();
            this.getTopic(this.questionId, this.topics[this.sn]);

        }

        private submit(): void {
            Http.DATA_TYPE = 'JSON';
            rest.answer.submitAnswers(this.submitObj, (data) => {
                console.log(data);
                Http.DATA_TYPE = 'FORMDATA';
                LoadingPage.dispose();
                if (data.status == "error") {
                    // this.ani_loginLoading.clear();
                    // this.showErrorTips(data.msg);
                    this.optionPrompt.play(data.msg, [Laya.stage.width / 2, Laya.stage.height / 2]);
                } else {
                    this.clearList();
                    //this.event('LoginOK');
                    // this.ani_loginLoading.clear();
                    console.log("哈利要" + data);
                    let datas: any = data.data;
                    this.errorNumber = datas.errNumber;
                    this.susNumber = datas.successNumber;
                    this.addForce = datas.rewardIsForce;

                    this.resultPage(datas);

                }
            })
        }

        private resultPage(datas: any): void {
            this.viewStack.selectedIndex = 3;
            if (this.addForce <= 0) {
                this.pic_fail.visible = true;
                this.pic_victory.visible = false;
                this.btn_getForce.visible = false;
                console.log("saddasdasd");
                this.game.event('Sounds', 8);
                console.log("!!!!!");
            }
            else {
                console.log("!!2222!");
                this.pic_fail.visible = false;
                this.pic_victory.visible = true;
                this.btn_getForce.visible = true;
                this.game.event('Sounds', 7);
                //btn_getForce
            }
            let str: string = Font.getSizeColor("#5d3500", 32, "您答对") + Font.getSizeColor("#593cce", 32, String(this.susNumber)) + Font.getSizeColor("#5d3500", 32, "题");
            this.txt_correct.innerHTML = str;
            str = Font.getSizeColor("#5d3500", 32, "增加了") + Font.getSizeColor("#593cce", 32, String(this.addForce)) + Font.getSizeColor("#5d3500", 32, "算力");
            this.txt_add.innerHTML = str;
            str = Font.getSizeColor("#5d3500", 28, "答错") + Font.getSizeColor("#ff1434", 28, String(this.errorNumber)) + Font.getSizeColor("#5d3500", 28, "题");
            this.txt_error.innerHTML = str;

            this.btn_check.visible = this.errorNumber > 0;
            this.txt_error.visible = this.errorNumber > 0;;
            this.txt_error.style.align = "center";
            this.txt_correct.style.align = "center";


        }

        private errPage(): void {
            this.curErr = 0;
            this.viewStack.selectedIndex = 2;
            LoadingPage.loading();
            rest.answer.getWrongList(this.questionId, (data) => {
                
                LoadingPage.dispose();
                if (data.status == "error") {
                    // this.ani_loginLoading.clear();
                    // this.showErrorTips(data.msg);
                    this.optionPrompt.play(data.msg, [Laya.stage.width / 2, Laya.stage.height / 2]);
                } else {

                    this.curErrTopic = data.data;
                    this.selectErr(this.curErr);

                }

            });


        }

        private selectErr(inx: number): void {
            this.txt_errTitle.text = "错题" + (inx + 1) + "/" + this.curErrTopic.length;
            this.txt_errWenti.wordWrap = true;
            this.txt_errWenti.text = this.curErrTopic[inx].question.title;
            this.list_errAnswer.array = this.curErrTopic[inx].questionAnswers;
            if (inx >= (this.curErrTopic.length - 1)) {
                this.txt_nextErr.text = "返回";
            }
            else {
                this.txt_nextErr.text = "下一题";
            }


        }

        public onListRender(cell: Laya.Box, index: number): void {

            var checkBox: Laya.CheckBox = cell.getChildByName("check") as Laya.CheckBox;
            var text: Laya.Text = cell.getChildByName("txt_answer") as Laya.Text;

            text.text = this.list_answer.array[index].answer;
            text.y = (55 - text.textHeight) / 2;

        }

        public listSelectHandler(inx: number): void {

            this.chooseList(inx);
        }

        public onErrListRender(cell: Laya.Box, index: number): void {
            var data: any = this.list_errAnswer.getItem(index);

            var text: Laya.Text = cell.getChildByName("txt_answer") as Laya.Text;
            var checkBox: Laya.CheckBox = cell.getChildByName("check") as Laya.CheckBox;

            text.text = data.answer;
            if (data.isRightAnswer > 0) {
                checkBox.selected = true;
            }
            else {
                checkBox.selected = false;
            }

            var ans: any[] = this.curErrTopic[this.curErr].playerAnswers;
            (cell.getChildByName("pic_choose") as Laya.Image).visible = false;
            for (var i: number = 0; i < ans.length; i++) {
                if (ans[i].answerId == data.answerId) {
                    (cell.getChildByName("pic_choose") as Laya.Image).visible = true;

                }
                else {
                    (cell.getChildByName("pic_choose") as Laya.Image).visible = false;
                }
            }

            text.y = (55 - text.textHeight) / 2;




        }

        public onOpened(): void {
            super.onOpened();
            //this.errPage();
            LoadingPage.loading();
            rest.answer.getQuestion((data) => {
                //1
                //
                LoadingPage.dispose();
                var datas: any = data.data;
                if (data.code == 1 || data.code == 10025 || data.code == 10026) {
                    this.questionId = datas.questionId;
                    this.topics = datas.topicId;
                    this.power = datas.rewardIsForce;
                    this.errorNumber = datas.errNumber;
                    this.susNumber = datas.topicNumber - this.errorNumber;
                    this.addForce = datas.rewardIsForceCount;
                    this.prizeForce = datas.rewardIsForce;
                }

                if (data.code == 1) {


                    this.viewStack.selectedIndex = 0;

                } else if (data.code == 10025 || data.code == 10026) {
                    this.questionId = data.data.questionId;
                    data.data.successNumber = data.data.topicNumber - data.data.errNumber;

                    this.resultPage(data.data);
                    //成功
                    // if(data.data.errNumber>0)
                    // {
                    //     this.resultPage(data.data);
                    // }
                    // else
                    // {

                    // }
                }
                else {
                    this.optionPrompt.play(data.msg, [Laya.stage.width / 2, Laya.stage.height / 2]);
                }









                if (datas) {
                    this.txt_total.text = `一共${datas.topicNumber}题`;
                    var all: number = datas.topicNumber * this.prizeForce;

                    if (datas.rewardQuestionNumber == 1) {
                        this.txt_reward.text = `每答对一题+${this.prizeForce}算力`;

                    } else if (datas.rewardQuestionNumber > 1 && datas.rewardQuestionNumber < datas.topicNumber) {
                        this.txt_reward.text = `回答正确${datas.rewardQuestionNumber}题后，每答对一题+${this.prizeForce}算力`;
                    }
                    else {
                        this.txt_reward.text = `全部回答正确+${this.prizeForce}算力`;
                    }
                    this.txt_power1.text = this.txt_reward.text;
                    this.txt_power.text = this.txt_reward.text;
                }




                Http.DATA_TYPE = 'FORMDATA';
                this.submitObj["questionId"] = this.questionId;
                this.submitObj["answerLogDTOList"] = [];
                // this.setQuestion(datas);
            });
        }

        private getTopic(questionId: number, topicId: number): void {
            var Token = localStorage.getItem("Token");
            rest.answer.getTopic(questionId, topicId, Token, (data) => {
                LoadingPage.dispose();
                if (data.msg == "error") {
                    console.log(data.msg);
                }
                else {
                    
                    var datas: any = data.data;
                    let inx: number = this.topics.indexOf(this.topics[this.sn]);
                    this.txt_wenti.innerHTML = Font.getSizeColor("#5a3ace", 24, (inx + 1) + ".") + Font.getSizeColor("#5d3500", 24, datas.title);
                    this.clearList();
                    this.list_answer.array = datas.answerList;


                    if (this.sn >= this.topics.length - 1) {
                        this.txt_next.text = "提交";
                    }
                    else {
                        this.txt_next.text = "下一题";

                    }
                    this.tinx = 30000;
                    this.txt_sec.text = String(this.tinx / 1000);
                    Laya.timer.loop(1000, this, this.countTime);
                }

            });


        }

        private tinx: number = 0;
        private countTime(): void {
            this.tinx -= 1000;
            this.txt_sec.text = String(this.tinx / 1000);
            if (this.tinx <= 0) {
                Laya.timer.clear(this, this.countTime);
                this.onNextClick(null);
                
            }
        }

        public onClosed(): void {
            Laya.timer.clear(this, this.time);

        }

        private count: number;
        private beginTime(): void {
            this.count = 0;
            this.txt_sec.text = String(this.count);
            Laya.timer.loop(1000, this, this.time)
        }

        private endTime(): void {
            Laya.timer.clear(this, this.time);

        }

        private time(): void {
            this.count++;
            this.txt_sec.text = String(this.count);
            if (this.count == 30) {
                this.endTime();

            }
        }

        private clearList(): void {
            
            var len: number = this.list_answer.cells.length;
            for (var i: number = 0; i < len; i++) {
                var checkBox: Laya.CheckBox = this.list_answer.getCell(i).getChildByName("check") as Laya.CheckBox;
                checkBox.selected = false;

            }
        }

        private chooseList(inx: number): void {
            var checkBox: Laya.CheckBox = this.list_answer.getCell(inx).getChildByName("check") as Laya.CheckBox;
            var len: number = this.list_answer.cells.length;
            checkBox.selected = true;
            for (var i: number = 0; i < len; i++) {
                if (inx != i) {
                    checkBox = this.list_answer.getCell(i).getChildByName("check") as Laya.CheckBox;
                    checkBox.selected = false;
                }

            }
        }

        



    }
}