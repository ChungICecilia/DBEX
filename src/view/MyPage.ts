/**Created by the LayaAirIDE*/
module view {
	import SoundManager = Laya.SoundManager;
	import Handler = Laya.Handler;
	export class MyPage extends ui.MyPageUI {

		private game: GamePage;
		private commonlyUsed: CommonlyUsed;
		constructor(game: GamePage) {
			super();
			this.game = game;
			this.commonlyUsed = new CommonlyUsed;
			this.btn_close.on(Laya.Event.CLICK, this, this.onClickClose);
		}

		private onClickClose(): void {
			this.close();
		}

		public onOpened(): void {
			switch (Rest.USERINFO.bgmStatus) {
				case 1:
					this.ckb_music.selected = false;
					break;
				case 2:
					this.ckb_music.selected = true;
					break;
			}

			switch (Rest.USERINFO.btnMusicStatus) {
				case 1:
					this.ckb_sound.selected = false;
					break;
				case 2:
					this.ckb_sound.selected = true;
					break;
			}

			this.ckb_music.on(Laya.Event.CLICK, this, this.onMusic);
			this.ckb_sound.on(Laya.Event.CLICK, this, this.onSound);
			this.text_nickname.text = Rest.USERINFO.nickName || '';
			//描边
			//this.text_nickname.stroke = 2
			//this.text_nickname.strokeColor = "#D35E36";
			this.text_bbm.text = (Rest.USERINFO.baseBbm + Rest.USERINFO.starterBbm).toFixed(4) || '0';
			this.text_quantity.text = Rest.USERINFO.bbmCalcPoint + Rest.USERINFO.marketCalcPoint + Rest.USERINFO.newsCalcPoint || '0';
			this.commonlyUsed.avatar(Rest.USERINFO.headIcon, this.img_avatar);
		}

		
		//背景音乐
		private onMusic(): void {
			if (this.ckb_music.selected) {
				//SoundManager.stopMusic();
				rest.user.setMusicStatus({ type: 1, status: 2, }, data => {
					Rest.USERINFO.bgmStatus = data.data.bgmStatus
					console.log(data)
				})
				this.game.event("GameMusic", false);

			} else {
				rest.user.setMusicStatus({ type: 1, status: 1, }, data => {
					Rest.USERINFO.bgmStatus = data.data.bgmStatus;
					
					SoundManager.playMusic("res/sounds/dbexBGM.mp3", 0, new Handler(this, this.onComplete));
				})
				this.game.event("GameMusic", true);
			}
		}

		//音效
		private onSound(): void {
			if (this.ckb_sound.selected) {
				rest.user.setMusicStatus({ type: 2, status: 2, }, data => {
					Rest.USERINFO = data.data.btnMusicStatus
				})
				this.game.event("GameSound", false);
			} else {
				rest.user.setMusicStatus({ type: 2, status: 1, }, data => {
					Rest.USERINFO = data.data.btnMusicStatus
				})
				this.game.event("GameSound", true);
			}
		}


		private onComplete(): void {
			console.log("播放完成");
		}
	}
}