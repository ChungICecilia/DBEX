/**Created by the LayaAirIDE*/
module view {
	import Browser = Laya.Browser;
	export class DesktopPage extends ui.DesktopPageUI {

		private data = [];
		private email: any

		constructor() {
			super();

			this.btn_close.on(Laya.Event.CLICK, this, this.onClickClose);
		}

		private onClickClose(): void {
			this.close();
		}

		public onOpened(): void {
			if (Browser.onAndriod || Browser.onAndroid) {
				this.img_up.skin="share/newTree_13.png";
				this.img_down.skin = "share/newTree_14.png";
			} else {
				this.img_up.skin="share/newTree_16.png";
				this.img_down.skin = "share/newTree_17.png";
			}
		}
	}
}