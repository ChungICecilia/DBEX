/**Created by the LayaAirIDE*/
module view {
	export class LoginPage extends ui.LoginPageUI {
		private signUpPage: view.SignUpPage;
		private ChangeGalaxyPage: view.ChangeGalaxyPage;
		private forgetPasswordPages: ForgetPasswordPages;
		private tips: number;
		private commonlyUsed: CommonlyUsed;
		constructor() {
			super();
			this.tips = 0;
			this.mobile.text = "";
			this.password.text = "";
			this.commonlyUsed = new CommonlyUsed();
			this.submit.on(Laya.Event.CLICK, this, this.onSubmit);
			this.sign.on(Laya.Event.CLICK, this, this.onSign);
			this.text_forgetPassword.on(Laya.Event.CLICK, this, this.onClickForgetPassword);
			//加载图集资源
			Laya.loader.load(["res/atlas/changeGalaxy.atlas", "res/atlas/public.atlas"]);

		}

		private onClosePrompt(): void {
			Laya.timer.clear(this, this.tipsLoop);
		}
		private AniComplete() {

		}


		private onSubmit(): void {
			
			//  this.event('LoginOK');
			this.ani_loginLoading.visible = true;
			// if(this.ani_loginLoading.visible){
			this.submit.disabled = true;
			this.sign.mouseEnabled = false;
			// }
			this.ani_loginLoading.play(0, true, "ani1");
			var name = this.mobile.text || '';
			var password = this.password.text || '';
			var invite_code = this.commonlyUsed.UrlSearch();

			if (name == "" || password == "") {
				this.ani_loginLoading.clear();
				this.showErrorTips("手机号码或密码不能为空!");
				return;
			}
			else {
				Http.DATA_TYPE = 'JSON';
				rest.user.login({ name, password, invite_code }, (data => {
					//console.log(name + "__" + password)
					//console.log(data)
					if (data.status == "error") {
						this.ani_loginLoading.clear();
						this.showErrorTips(data.msg);
					} else {

						this.event('LoginOK');
						// this.ani_loginLoading.clear();
					}
					Http.DATA_TYPE = 'FORMDATA';
				}))
			}
		}

		private showErrorTips(msg: string): void {
			this.tips = 2;
			this.img_loin.visible = true;
			this.img_loin.width = msg.length * 30;
			this.text_loginTips.width = msg.length * 30;
			this.img_loin.centerX = 1;
			this.text_loginTips.text = msg;
			Laya.timer.loop(1000, this, this.tipsLoop);
		}

		private tipsLoop(): void {
			this.tips--;
			if (this.tips <= 0) {
				this.img_loin.visible = false;
				if (this.submit.disabled) {
					this.submit.disabled = false;
					this.sign.mouseEnabled = true;
				}
				Laya.timer.clear(this, this.tipsLoop);
			}
		}

		private onSign(): void {
			// this.event("SignUpOK");
			//跳转注册页面
			this.signUpPage = new view.SignUpPage();
			Laya.stage.addChild(this.signUpPage);
		}

		private onClickForgetPassword(): void {
			this.forgetPasswordPages = new ForgetPasswordPages();
			Laya.stage.addChild(this.forgetPasswordPages);
		}

		// 关闭登录
		public close(): void {
			if (this.parent != null) {
				this.parent.removeChild(this);
			}
		}
	}
}