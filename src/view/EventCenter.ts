/**
* name 
*/
module view{
	export class EventCenter{
		private static dispatcher = new Laya.EventDispatcher;
		constructor(){

		}
		
		public static add(type:string,caller:any, listener:Function,args?: Array<any>):void{
			this.dispatcher.on(type, caller,listener ,args );
		}
		/**
		 * 快速派发一个GameEvent事件
		 * @param	type 事件名
		 * @param	arg 可附带的数据对象
		 * @return 是否成功派发
		 */
 		public static dispatchWith( type:string , args?: Array<any> ):boolean
		{
			return this.dispatcher.event( type ,args );
		}
		/**
		 * 普通事件,和flash本身一样
		 * @param	任何基于Event的对象
		 * @return 
		 */
		 public static dispatchEvent(type:string,args?: Array<any>):boolean {
			
			return this.dispatcher.event(type,args);
		}
		public static remove(type:string,caller:any, listener:Function,onceOnly:boolean = false):void{
			this.dispatcher.off( type,caller,listener ,onceOnly);
		}
	}
}