/**Created by the LayaAirIDE*/


module view {

	import Browser = Laya.Browser;
	export class ShareItPage extends ui.ShareItPageUI {

		private shareLink = '';
		private inviteHref;
		private copyDiv;
		private game: GamePage;
		constructor(game: GamePage) {
			super();
			this.game = game;
			let strings: String[] = Browser.window.location.href.split("?");
			this.shareLink = strings[0] + '?userCode=' + Rest.INVITECODE;
			this.inviteHref = Laya.Browser.window.document.getElementById("bar");
			this.copyDiv = Laya.Browser.window.document.getElementById("copy_btn");
			this.btn_close.on(Laya.Event.CLICK, this, this.onClickClose);
		}


		private onClickClose(): void {
			this.hiddenCopyBtn();
			this.close();
		}
		//<div style=" background: rgba(255, 255, 255, 0); position: absolute; bottom:26%; text-align: center;  width: 100%; visibility: hidden"id="copy_div">
		public onOpened(): void {
			//  Rest.OPTION
			this.html_tips.style.lineHeight = 24;
			this.html_tips.style.align = "center";
			this.html_tips.innerHTML =
				"<font  style='fontSize:26' color='#333333'>" + '可分享给好友,好友成功注册增加算力' +
				"<font style='fontSize:30' color='red'>" + '5' +
				"<font style='fontSize:26' color='#333333'>" + '点,' +
				"<font style='fontSize:30' color='red'>" + '1' +
				"<font style='fontSize:26' color='#333333'>" + '张复活卷,' +
				"<font style='fontSize:30' color='red'>" + '2' +
				"<font style='fontSize:26' color='#333333'>" + '积分!' +
				"<br/></font>" + "<br/></font>" + "<br/></font>" + "<br/></font>" + "<br/></font>" + "<br/></font>" + "<br/></font> ";
			this.showCopyBtn();
			console.log(Rest.INVITECODE);
		}

		private hiddenCopyBtn() {
			if (this.inviteHref && this.copyDiv) {
				this.copyDiv.style.visibility = "hidden";
				this.inviteHref.style.visibility = "hidden";
			}
		}

		private showCopyBtn() {
			if (this.inviteHref && this.copyDiv) {
				this.copyDiv.style.visibility = "visible";
				this.inviteHref.style.visibility = "visible";
				this.inviteHref.innerHTML = this.shareLink;
				console.log(this.inviteHref.text);
			}
		}
	}
}