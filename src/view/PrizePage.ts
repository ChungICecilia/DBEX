/**
* name 
*/
module view{
	export class PrizePage extends ui.PrizePageUI{
		private activityId:number = 0;
		private game: GamePage;
		constructor(game:GamePage){
			super();
			this.game = game;
			this.txt_add.style.bold = true;
			this.txt_add.style.align = "center";
			this.btn_close.on(Laya.Event.CLICK, this, this.onClickClose);
			this.btn_get.on(Laya.Event.CLICK, this, this.onGetClick);
		}

		public setData(data:any):void
		{	
			var desc:string = "";
			if(data.answerStatus==1)
			{
				
		
				desc = "您获得参与奖";
			}else if(data.answerStatus==2)
			{
				
		
				desc = "您获得大奖";
			}else
			{
				
			
				desc = "您获得参与奖";
			}

			this.txt_people.text= String(data.participantsPeopleNumber);
			this.txt_prizeMan.text = String(data.getRewardsPeopleNumber);
			this.txt_total.text = String(data.rewardQuantity );
			this.txt_bigPrize.text = String(data.finishReward);
			this.txt_canyu.text = String(data.participationReward );
			this.txt_title.text = String(data.description);
			
			this.activityId = data.activityId;


			this.txt_add.innerHTML = Font.getSizeColor("#5d3500", 28, desc)+Font.getSizeColor("#593cce", 46, String(data.getRewardQuantity)) + Font.getSizeColor("#5d3500", 46, "DBEX");
			this.event('Sounds', 7);
		}

		private onClickClose():void
		{
			this.close();
		}

		private OptionPrompt:OptionPrompt = new OptionPrompt();
		private onGetClick():void
		{
			rest.chong.reward(this.activityId,(data) => {
				if (data.status == "error") {
					// this.ani_loginLoading.clear();
					// this.showErrorTips(data.msg);
					console.log(data.msg);
					this.OptionPrompt.play(data.msg, [Laya.stage.width / 2, Laya.stage.height / 2]);
				} else {
					//this.setInfo(data.data);
					if (data.code) {
						//this.getTopic(data.data.topicId);
						console.log("奖励领取成功");
						this.close();
						let inviteCode: '';
						rest.user.userInfo(inviteCode, (data) => {
							Rest.USERINFO = data.data.player;
							Rest.GALAXY = data.data.byDbexGalaxy;
							this.game.onCreatedRole();
						})
					} else {
						console.log(data.msg);
					}

				}
			});
		}
	}

	
}