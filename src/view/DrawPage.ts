/**
* name 
*/
module view{
	export class DrawPage extends ui.DrawPageUI{
		private game:GamePage;
		private tl:Laya.Animation;
		private light:Laya.Animation;
		//物品个数;
		private count:number = 8;
		//角度;
		private angle:number;
		//最少旋转圈数;
		private rotateNum:number = 8;
		
		private prizes:any[];
		//重点突出的奖励
		private importId:number;
		constructor(game:GamePage){
			super();
			this.angle = 360/this.count;
			this.game = game;
		}

		protected initialize(): void {
            super.initialize();
			this.btn_close.on(Laya.Event.CLICK, this, this.onClickClose);
			this.btn_draw.on(Laya.Event.CLICK, this, this.onDrawClick);			
		}

		private onClickClose(e: Laya.Event): void {
           
            this.close();
        }

		

		

		

		/**
		 * 
		 */
		public onClosed(): void {
            
			this.pic_circle.rotation = 0;
			if(this.tween)
				Laya.Tween.clear(this.tween);
			if(this.tl)
			{
				this.tl.stop();
				this.tl.clear();
				this.tl = null;
				
			}
			if(this.light)
			{
				this.light.stop();
				this.light.clear();
				this.light = null;
			}
        }

		/**
		 * 
		 */
		public onOpened():void
		{
			
			this.pic_circle.rotation = 0;
			this.getPrizesList();
			if(!this.tl){
				this.tl = new Laya.Animation();
				this.tl.loadAnimation("choujiang.ani");
				this.addChild(this.tl);
				this.tl.x = 370;
				this.tl.y = 648;
				this.tl.mouseEnabled = false;
				this.tl.mouseThrough = true;
			}

			if(!this.light){
				this.light = new Laya.Animation();
				this.light.loadAnimation("choujiang1.ani");
				this.addChild(this.light);
				this.light.x = 372;
				this.light.y = 648;
				this.light.visible = false;
			}


			this.setChildIndex(this.txt_score,this.numChildren-1);
			this.tl.play(0,true,"ani1");
			
			this.btn_draw.mouseEnabled = true;


		}

		private optionPrompt:OptionPrompt = new OptionPrompt();
		private drawType:number = 1;
		private curScore:number = 0;
		private getPrizesList():void
		{
			rest.draw.list((data)=>{
				
				
				if(data.code==1)
				{
					this.prizes = data.data.rewardSectorReturnDTO;
					this.makePrizeList(this.prizes);
					if(data.data.drawStatus)
					{
						
						this.drawType = 2;
						this.txt_score.text = Rest.OPTION.draw_every_draw_costs+"积分";
					}
					else
					{
						this.txt_score.text = "免费";
						this.drawType = 1;
					}
					this.curScore = data.data.rewardPoints;
					this.txt_curScore.text = "当前积分为:"+this.curScore;
				}
				
				
				else
				{
					OptionPrompt.ins.play(data.msg);
				}
			});
		}

		private makePrizeList(prizes:any[]):void
		{
			var len:number = prizes.length;
			var i:number = 0;
			var obj:any;
			var skin:string = "";
			var name:string = "";
			for(i=0;i<len;i++)
			{
				obj = prizes[i];
				switch(obj.rewardType)
				{
					case 1:
						skin = "drawPrize/power.png";
						name = "算力";
						break;
					case 4:
						skin = "drawPrize/zuan.png";
						name = "DBEX";
						break;
					case 3:
						skin = "drawPrize/exp.png";
						name = "经验";
						break;
				}
				var sp:any = this["pic_prize"+i];
				
				
				sp.skin = skin;
				this["txt_prize"+i].text = name+"+"+obj.rewardQuantity;
			
				if(obj.isImportant==1 && obj.rewardType==4)
				{
					sp.skin = "drawPrize/09.png";
			

				}
			}
		}

		private tween:laya.utils.Tween;
		//开始抽奖
		private draw(id:number):void
		{
			this.tl.stop();
			this.tl.visible = false;
			this.pic_circle.rotation %= 360;
			var need:number = this.angle*id+360*this.rotateNum-this.pic_circle.rotation;
			this.light.play(0,true);
			this.light.visible = true;
			this.tween = Laya.Tween.to(this.pic_circle,{rotation:need+this.pic_circle.rotation},4000,Laya.Ease.circInOut,Laya.Handler.create(this,this.onReset,[]));

			console.log(this.angle);
		}

		private getPrizeInx(id:number):number
		{
			
			
			var i:number = 0;
			
			if(this.prizes)
			{
				var len:number = this.prizes.length;
				for(i=0;i<len;i++)
				{
					if(this.prizes[i].id == id)
					{
						return i;
						
					}
				}

			}
			return -1;
		}

		private onDrawClick(e: Laya.Event): void {
			this.btn_draw.mouseEnabled = false;
			rest.draw.drawPrize(this.drawType,(data)=> {
				if(data.code==1)
				{
					
					if(this.drawType==1)
					{
						this.drawType = 2;
						this.txt_score.text = Rest.OPTION.draw_every_draw_costs+"积分";
					}
					else
					{
						this.curScore -= Rest.OPTION.draw_every_draw_costs;	
					}
					
					this.draw(this.getPrizeInx(data.data));
					
				}else
				{
					OptionPrompt.ins.play(data.msg);
					this.btn_draw.mouseEnabled = true;
				}
				this.txt_curScore.text = "当前积分为:"+this.curScore;
			});
			
			
			
			
			
        }

		private onReset():void
		{
			this.light.stop();
			this.light.visible = false;
			this.tl.stop();
			this.tl.visible = true;
			this.tl.on(Laya.Event.COMPLETE, this, this.lightComplete);
			this.tl.play(0,false,"ani3");
			if(this.tween)
				Laya.Tween.clear(this.tween);
			let inviteCode: '';
			rest.user.userInfo(inviteCode, (data) => {
				Rest.USERINFO = data.data.player;
				Rest.GALAXY = data.data.byDbexGalaxy;
				this.game.onCreatedRole();
			})
			
			if(this.drawType==2)
			{
				this.txt_score.text = Rest.OPTION.draw_every_draw_costs+"积分";
			}
		}

		private lightComplete():void
		{
			this.tl.stop();
			this.tl.off(Laya.Event.COMPLETE,this,this.lightComplete)
			this.tl.play(0,true,"ani1");
			this.btn_draw.mouseEnabled = true;
		}
	}


}