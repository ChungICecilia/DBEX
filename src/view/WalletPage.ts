module view {

    export class WalletPage extends ui.WalletPageUI {
        private tips: number;
        constructor() {
            super();
            this.btn_close.on(Laya.Event.CLICK, this, this.onClickClose);
            this.btn_wallet.on(Laya.Event.CLICK, this, this.onClickWallet);
        }

        private onClickClose(): void {
            this.text_bbm.text = '';
            this.text_rmb.text = '';
            this.close();
        }

        public onOpened(): void {
            console.log(Rest.OPTION.estimated_value_ratio);
            let num = Rest.USERINFO.baseBbm + Rest.USERINFO.starterBbm;
            console.log(num);
            this.text_bbm.text = "持有DBEX:" + num.toFixed(4) || "持有DBEX：0";
            // Rest.OPTION.  transform_rate
            let y = num * Rest.OPTION.estimated_value_ratio + '';
            if ("NaN" == y) {
                this.text_rmb.text = "预估值0元";
            } else {
                this.text_rmb.text = "预估价值:" + (num * Rest.OPTION.estimated_value_ratio).toFixed(4) + "元";
            }
        }

        private onClickWallet(): void {
            // Browser.window.location.href = "http://www.bbb-home.com/appDownload.html";
            this.showErrorTips("迪拜交易所正在搭建中，敬请期待哦！");
        }

        private showErrorTips(msg: string): void {
            this.tips = 3;
            this.img_createPlayer.visible = true;
            this.img_createPlayer.width = msg.length * 30;
            this.text_createPlayer.width = msg.length * 30;
            this.img_createPlayer.centerX = 1;
            this.text_createPlayer.text = msg;
            Laya.timer.loop(1000, this, this.tipsLoop);
        }
        private tipsLoop(): void {
            this.tips--;
            if (this.tips <= 0) {
                if (this.disabled) {
                    this.disabled = false;
                }
                this.img_createPlayer.visible = false;
                Laya.timer.clear(this, this.tipsLoop);
            }
        }
    }
}