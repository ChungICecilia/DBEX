module view {
    export class Background extends Laya.Sprite {
        //主页背景
        private bg: Laya.Sprite;
        private game: GamePage;
        private bgAni: Laya.Animation;
        constructor(game: GamePage) {
            super();
            this.game = game;
            Laya.loader.load([ "res/atlas/friend.atlas"]);
            this.init();
        }

        private init(): void {
            this.bg = new Laya.Sprite();
            this.bg.loadImage("res/singleImages/bg.jpg", 0, 0, Laya.stage.width, Laya.stage.height);
            this.addChild(this.bg);
            this.bgAni = new Laya.Animation();
			this.bgAni.loadAnimation("bg.ani", Laya.Handler.create(this, this.onLoaded), null);
            this.bgAni.pos(375,220);
            this.addChild(this.bgAni);
            this.bgAni.play(0,true,"ani1");//
        }
        private onLoaded(){
            this.bgAni.on(Laya.Event.COMPLETE, this, null);
        }

    }
}