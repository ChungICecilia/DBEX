/**Created by the LayaAirIDE*/
module view {

	export class QSJPage extends ui.QSJPageUI {
		private game: GamePage;
		constructor(game: GamePage) {
			super();
			this.game = game;

			this.btn_close.on(Laya.Event.CLICK, this, this.onClickClose);

		}

		private onClickClose(): void {
			this.close();
		}
	}
}