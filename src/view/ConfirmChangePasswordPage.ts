/**
* name 
*/
module view {
	export class ConfirmChangePasswordPage extends ui.RegistPageUI {
		private phoneNum: string;
		private verCode: string;
		private usercode: string;
		private tips: number;
		constructor(phoneNum: string, verCode: string, usercode: string) {
			super();
			this.phoneNum = phoneNum;
			this.verCode = verCode;
			this.usercode = usercode;
			this.registBtn.on(Laya.Event.CLICK, this, this.regist);
			this.loginClick.on(Laya.Event.CLICK, this, this.onClickClose);
			this.text_text.text = "确认";
			this.loginClick.visible = false;
		}

		private regist() {
			if (this.psw_again.text == this.pswText.text) {
				var code = this.verCode;
				var passWord = this.psw_again.text;
				var phone = this.phoneNum;
				var repeatPassword = this.pswText.text;
				Http.DATA_TYPE = 'JSON';
				rest.user.changePassword({ code, passWord, phone, repeatPassword }, (data => {
					if (data.status == "error") {
						this.tips = 3;
						this.img_singn.visible = true;
						this.img_singn.width = data.msg.length * 30;
						this.img_singn.centerX = 1;
						this.text_tips.width = data.msg.length * 30;
						this.text_tips.text = data.msg;
						Laya.timer.loop(1000, this, this.tipsLoop);
					} else {
						this.onClickClose();
					}
					Http.DATA_TYPE = 'FORMDATA';
				}));
			} else {
				this.tips = 3;
				this.img_singn.visible = true;
				this.text_tips.text = "请输入相同的密码";
				this.text_tips.width = 330;
				this.img_singn.width = 330;
				this.img_singn.centerX = 1;
				Laya.timer.loop(1000, this, this.tipsLoop);
			}

		}

		private onClickClose(): void {

			if (this.parent != null) {
				this.event('CLOSE');
				this.parent.removeChild(this);
				this.img_singn.visible = false;
				Laya.timer.clear(this, this.tipsLoop);
			}
		}
		private tipsLoop(): void {
			this.tips--;
			if (this.tips <= 0) {
				this.img_singn.visible = false;
				Laya.timer.clear(this, this.tipsLoop);
			}
		}
	}
}