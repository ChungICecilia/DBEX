/**
* name
*/
module view {
	export class ChongPage extends ui.ChongPageUI {
		private game: GamePage;
		private numSp: NumSprite;
		private prevTime: number;
		private startTime: number;
		private endTime: number;
		private curTime: number;
		private totalNum: number;
		private rewardQuantity: number;
		private ansInx: number;

		private activityId: number = 0;
		private topicId: number = 0;
		private answerList: any[];
		private backRoll: number;

		private answerTime: number;

		constructor(game: GamePage) {
			super();
			this.game = game;
			this.btn_start.on(Laya.Event.CLICK, this, this.onClickStart);
			this.btn_next.on(Laya.Event.CLICK, this, this.onClickNext);
			this.btn_reward.on(Laya.Event.CLICK, this, this.onClickReward);
			this.btn_live.on(Laya.Event.CLICK, this, this.onClickLive);
			this.numSp = new NumSprite();
			this.numSp.on("LoadOk", this, this.onLoaded);
			this.mainPg.addChild(this.numSp);
			this.list_answers.renderHandler = new Laya.Handler(this, this.onListRender);
			this.txt_wenti.style.fontSize = 24;
			this.txt_wenti.style.wordWrap = true;

			this.numSp.y = 243;
			this.mainPg.addChild(this.numSp);
			this.list_answers.renderHandler = new Laya.Handler(this, this.onListRender);

		}

		protected initialize(): void {
			super.initialize();
			this.btn_close.on(Laya.Event.CLICK, this, this.onClickClose);
			this.list_answers.selectEnable = true;
			this.list_answers.selectHandler = new Laya.Handler(this, this.listSelectHandler);
			var str: string = this.txt_describe.text;
			str = str.replace("@", Rest.OPTION.revive_use_card);
			 
			this.txt_describe.text = str;
		}

		private onLoaded(): void {
			var total: number = this.numSp.totalWidth + this.pic_dbex.width;
			var tx: number = (this.mainPg.width - total) / 2;
			this.numSp.x = tx;
			this.pic_dbex.x = this.numSp.x + this.numSp.totalWidth - 20;
			this.numSp.y = this.pic_dbex.y;
			this.numSp.x += 10;
			this.pic_dbex.x += 10;

		}
		private onClickClose(): void {
			this.close();
		}

		private optionPrompt: OptionPrompt = new OptionPrompt();
		public onOpened(): void {
			this.ansInx = 0;
			
			LoadingPage.loading();
			rest.chong.info((data) => {
				LoadingPage.dispose();
				if (data.code == 1) {

					this.topicInx = 0;
					this.setInfo(data.data, data.code);
					this.viewStack.selectedIndex = 0;

				} else {
					if (data.code == -101) {
						this.setInfo(data.data, data.code);
					} else {
						this.optionPrompt.play(data.msg, [Laya.stage.width / 2, Laya.stage.height / 2]);
					}

				}


				


			});

		}

		private setInfo(datas: any, state: number): void {
			this.activityId = datas.activityId;
			this.totalNum = datas.questionNumber;
			this.txt_total.text = `一共${datas.questionNumber}题`;
			this.txt_des.text = `答对${datas.questionNumber}道题的所有玩家共同分享`;
			this.answerTime = datas.answerTime;

			this.rewardQuantity = datas.rewardQuantity;
			this.numSp.setNumber(datas.rewardQuantity, "res/atlas/dnum.atlas", -20);

			var date: Date = new Date();

			console.log(datas);
			var str = datas.previewTime.replace(/-/g, '/');
			this.prevTime = (new Date(str).getTime());

			str = datas.startTime.replace(/-/g, '/');
			this.startTime = (new Date(str)).getTime();

			str = datas.endTime.replace(/-/g, '/');
			this.endTime = (new Date(str)).getTime();

			this.curTime = (new Date()).getTime();
			this.pic_leftTime.visible = false;
			this.backRoll = datas.backRoll;
			if (this.curTime < this.startTime) {
				//预览时间
				if (this.curTime > this.prevTime) {

					this.tInx = this.startTime - this.curTime;
					this.txt_time.text = DateUtil.second2String(this.tInx/1000);
					Laya.timer.loop(1000, this, this.time)
				}
				else {
					this.txt_time.text = "未开始";
				}
			}
			else if (this.curTime >= this.startTime && this.curTime < this.endTime) {
				if (state == -101) {

					if (datas.state == 2) {
						this.goCorrectPg();
					} else if (datas.state = 3) {
						this.goErrorPg();
					}
				} else if (state) {


					this.txt_time.text = "开始答题";

				}
				this.pic_leftTime.visible = true;
				this.curTime = new Date().getTime();
				this.aInx = this.endTime - this.curTime;
				this.txt_leftTime.text = "活动剩余时间:" + DateUtil.second2String(this.aInx/1000)
				Laya.timer.loop(1000, this, this.atime);

			}
			else {
				this.txt_time.text = "不在答题时间";
			}

			//console.log(oldTime);
		}

		private tInx: number;
		private time(): void {
			this.tInx -= 1000;
			this.txt_time.text = DateUtil.second2String(this.tInx/1000);

			if (this.tInx < 0) {
				Laya.timer.clear(this, this.time)
			}


		}

		private sInx: number;
		private onAnswerTime(): void {
			this.sInx -= 1000;
			this.txt_ansTime.text = String(this.sInx / 1000);


			if (this.sInx <= 0) {

				this.onClickNext();

			}
		}

		public onClosed(): void {
			
			Laya.timer.clear(this, this.time);
			Laya.timer.clear(this, this.atime);
			Laya.timer.clear(this, this.onAnswerTime);
			
		}
		
		private onClickStart(): void {

			this.curTime = (new Date()).getTime();

			if (this.curTime < this.endTime && this.curTime > this.startTime) {
				
				this.answerPage();
			}
		}

		private onClickNext(): void {
			Http.DATA_TYPE = 'JSON';
			let submitObj: Object = new Object();
			submitObj["topicId"] = this.topicId;
			submitObj["headerId"] = this.activityId;
			submitObj["answerIdList"] = this.getAnswer();
			Laya.timer.clear(this, this.onAnswerTime);
			LoadingPage.loading();
			rest.chong.answer(submitObj, (data) => {
				LoadingPage.dispose();
				Http.DATA_TYPE = 'FORMDATA';
				if (data.status == "error") {
					// this.ani_loginLoading.clear();
					// this.showErrorTips(data.msg);
					this.optionPrompt.play(data.msg, [Laya.stage.width / 2, Laya.stage.height / 2]);
				} else {

					//this.event('LoginOK');
					// this.ani_loginLoading.clear();

					let datas: any = data.data;
					this.backRoll = datas.backRoll;

					var errNum: number = datas.errNumber;
					var sucNum: number = datas.successNumber;
					var reward: number = datas.rewardIsForce;
					if (data.code == 1) {
						this.curTime = (new Date()).getTime();

						if (this.curTime < this.endTime && this.curTime > this.startTime) {
							this.game.event('Sounds', 6);
							this.answerPage();
						}

					}
					else if (data.code == 2001) {
						this.goCorrectPg();
					}
					else {
						this.goErrorPg();
					}
					//this.resultPage(datas);

				}
			})
		}

		private onClickReward(): void {
			rest.chong.reward(this.activityId, (data) => {
				if (data.status == "error") {
					// this.ani_loginLoading.clear();
					// this.showErrorTips(data.msg);
					this.optionPrompt.play(data.msg, [Laya.stage.width / 2, Laya.stage.height / 2]);

				} else {
					//this.setInfo(data.data);
					if (data.code) {
						//this.getTopic(data.data.topicId);

						this.optionPrompt.play("奖励领取成功", [Laya.stage.width / 2, Laya.stage.height / 2]);
						this.close();
						let inviteCode: '';
						rest.user.userInfo(inviteCode, (data) => {
							Rest.USERINFO = data.data.player;
							Rest.GALAXY = data.data.byDbexGalaxy;
							this.game.onCreatedRole();
						})
					} else {
						this.optionPrompt.play(data.msg, [Laya.stage.width / 2, Laya.stage.height / 2]);
					}

				}

			});
		}

		private onClickLive(): void {
			LoadingPage.loading();
			rest.chong.live(this.activityId, (data) => {
				LoadingPage.dispose();
				if (data.status == "error") {
					// this.ani_loginLoading.clear();
					// this.showErrorTips(data.msg);
					this.optionPrompt.play(data.msg, [Laya.stage.width / 2, Laya.stage.height / 2]);
				} else {
					//this.setInfo(data.data);
					if (data.code) {
						this.topicInx = 0;
						this.answerPage();
					}

				}

			});
		}

		private answerPage(): void {
			this.viewStack.selectedIndex = 1;

			this.getTopicId();


		}

		private getTopicId(): void {
			LoadingPage.loading();
			this.ansInx++;
			rest.chong.getTopicId((data) => {
				if (data.status == "error") {
					// this.ani_loginLoading.clear();
					// this.showErrorTips(data.msg);
				} else {
					//this.setInfo(data.data);
					this.getTopic(data.data.topicId);
				}

			});
		}

		private topicInx: number;
		private getTopic(topicId: number): void {
			Laya.timer.clear(this, this.onAnswerTime);
			this.topicId = topicId;
			rest.chong.getTopic(topicId, (data) => {
				LoadingPage.dispose();
				if (data.status == "error") {
					// this.ani_loginLoading.clear();
					// this.showErrorTips(data.msg);
					this.optionPrompt.play(data.msg, [Laya.stage.width / 2, Laya.stage.height / 2]);
				} else {
					this.topicInx++;
					let datas: any = data.data;

					this.txt_wenti.innerHTML = Font.getSizeColor("#5d3500", 24, (this.topicInx) + ".") + Font.getSizeColor("#5d3500", 24, datas.title);
					this.list_answers.array = datas.answerList;
					this.answerList = datas.answerList;
					this.sInx = this.answerTime * 1000;
					this.txt_ansTime.text = String(this.sInx / 1000);
					Laya.timer.loop(1000, this, this.onAnswerTime);
					this.clearList();

				}

			});
		}

		private aInx: number;
		private atime(): void {
			this.aInx -= 1000;
			if (this.aInx < 0) {
				Laya.timer.clear(this, this.atime);
				this.txt_leftTime.text = "活动剩余时间:" + DateUtil.second2String(0);

				return;
			}
			this.txt_leftTime.text = "活动剩余时间:" + DateUtil.second2String(this.aInx/1000);


		}

		public onListRender(cell: Laya.Box, index: number): void {

			var text: Laya.Text = cell.getChildByName("txt_answer") as Laya.Text;
			text.text = this.list_answers.array[index].answer;

			text.y = (55 - text.textHeight) / 2;

		}

		public listSelectHandler(index: number): void {


			this.chooseList(index);
		}

		private clearList(): void {
			var len: number = this.list_answers.cells.length;
			for (var i: number = 0; i < len; i++) {
				var checkBox: Laya.CheckBox = this.list_answers.getCell(i).getChildByName("check") as Laya.CheckBox;
				checkBox.selected = false;

			}
		}

		private chooseList(inx: number): void {
			var checkBox: Laya.CheckBox = this.list_answers.getCell(inx).getChildByName("check") as Laya.CheckBox;
			var len: number = this.list_answers.cells.length;
			checkBox.selected = true;
			for (var i: number = 0; i < len; i++) {
				if (inx != i) {
					checkBox = this.list_answers.getCell(i).getChildByName("check") as Laya.CheckBox;
					checkBox.selected = false;
				}

			}
		}




		private getAnswer(): any[] {
			var ids: number[] = [];
			var sprites: Laya.Sprite[] = this.list_answers.cells;
			var spri: Laya.Sprite;
			for (var i: number = 0; i < sprites.length; i++) {
				spri = sprites[i];
				var cheBox: laya.ui.CheckBox = spri.getChildByName("check") as laya.ui.CheckBox;
				if (cheBox.selected) {
					var ansId: number = this.list_answers.getItem(i).answerId;
					ids.push(ansId);
				}


			}
			return ids;
		}

		private goCorrectPg(): void {

			this.viewStack.selectedIndex = 2;
			this.txt_corNum.text = `您答对了${this.totalNum}题`;
			this.game.event('Sounds', 7);
		}

		private goErrorPg(): void {
			this.viewStack.selectedIndex = 3;
			this.txt_desLive.text = `${this.rewardQuantity}个DBEXD等着您呢，复活重来！`;
			console.log(this.backRoll);
			this.txt_liveNum.text = `复活卷:${this.backRoll}`;
			this.txt_liveNeed.text = "(复活x" + Rest.OPTION.revive_use_card + "张)";
			this.game.event('Sounds', 8);

		}
		//==============================================答题页面===============================================


	}

}