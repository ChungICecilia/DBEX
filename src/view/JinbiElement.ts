/**
* name 
*/
module view{
	export class JinbiElement{
		private _skin:Laya.View;
		private _info:Object;
		private angle:number = 2;
		private speed:number;;
		private rY:number;
		private rX:number;
		private txt:Laya.Text;
		private curLeg:number;
		constructor(skin:Laya.View){
			this._skin = skin;
			this.rX = this._skin.x;
			this.rY = this._skin.y;
			this._skin.visible = true;
			this.speed = 0.01 + Math.random() * 0.02;
			this._skin.on(Laya.Event.CLICK, this, this.click);
		}
		/**
		 * 
		 * 缓动 
		 **/
		private tween(): void{
			/**上下回弹*/
			this._skin.y=this.rY + Math.sin(this.angle)*10;
			this.angle+=this.speed;
		}
		/**
		 * 
		 * 点击
		 **/
		private click(): void{
			rest.work.pickCoin({id:this._info[`id`]}, data => {
                console.log(this._info[`id`]);
                if (data.status == "error") {

                } else {
					Rest.USERINFO = data.data;
					var leg = Rest.COINSLIST.length;
					for (var i = 0; i < leg; i++) {
						var obj = Rest.COINSLIST[i];
						if(obj[`id`] == this._info[`id`])
						{
							Rest.COINSLIST.splice(i,1);
							i = leg;
							curLeg =Rest.COINSLIST.length; 
							this.moveTween(curLeg);
						}
					}
                }
			});
		}
		/**
		 * 
		 * 飘往
		 **/
		public moveTween(leg:number): void{
			Laya.timer.clear(this,this.tween);
			var flyX:number = 550;
			var flyY:number = 20;
			Laya.Tween.to(this._skin,{x:flyX,y:flyY,scaleX:0,scaleY:0},2000,Laya.Ease.quintInOut,Laya.Handler.create(this, () => {
				this._skin.x = this.rX;
				this._skin.y = this.rY;
				EventCenter.dispatchEvent("refreshUserInfo");
				var ind:number = 10 - (leg - curLeg);
				console.info(ind);
				if(leg >= 10)
				{
					console.info(this.info + ">>>>>>>>>>>>>>");
					this.info = Rest.COINSLIST[ind-1];
					Laya.Tween.to(this._skin,{scaleX:1,scaleY:1},1000,Laya.Ease.bounceOut);
					EventCenter.dispatchEvent("moreCoins");
				}else
				{
					this.hide();
				}
			}));
		}
		/**
		 * 
		 * 隐藏
		 **/
		public hide(): void{
			this.info = null;
			this._skin.visible = false;
		}

		public get info() {
            return this._info;
        }
        public set info(value:Object){
           this._info = value;
		   if(!value)
		   {
			   return;
		   }
	       this.txt =  this._skin[`txt_value`];
		   this.txt.text = this._info[`gainBbmCount`] + "";
		   Laya.timer.frameLoop(1,this,this.tween,null);
		   this._skin.visible = true;
        }
	}
}