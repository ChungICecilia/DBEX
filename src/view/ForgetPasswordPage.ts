/**
* name s
*/
module view {
	export class ForgetPasswordPages extends ui.SignUpPageUI {
		private confirmChangePasswordPage: view.ConfirmChangePasswordPage;
		private commonlyUsed: CommonlyUsed;
		private num: number
		private tips: number
		constructor() {
			super();

			this.mobile.text = "";
			this.code.text = "";
			this.num = 60;
			this.tips = 0;
			this.commonlyUsed = new CommonlyUsed;
			this.submitBtn.on(Laya.Event.CLICK, this, this.onSubmit);
			this.lab_num.on(Laya.Event.CLICK, this, this.oncode);
			this.img_singn.on(Laya.Event.CLICK, this, this.onClosePrompt);
			this.btn_back.visible = false;
		}
		private onBack() {
			this.removeSelf();
		}
		private onClosePrompt(): void {
			this.img_singn.visible = false;
			Laya.timer.clear(this, this.tipsLoop);
		}

		private oncode(): void {
			if (this.mobile.text != "" && this.mobile.text.length == 11) {
				this.num = 60;
				this.lab_num.text = "重新发送" + this.num + "秒";

				var phone = this.mobile.text;
				var type = 1;
				Http.DATA_TYPE = 'JSON';
				rest.user.getMSBack({ phone, type }, (data => {
					if (data.status == "error") {
						this.tips = 3;
						this.img_singn.visible = true;
						this.text_tips.width = data.msg.length * 30;
						this.img_singn.width = data.msg.length * 30;
						this.img_singn.centerX = 1;
						this.text_tips.text = data.msg;
						Laya.timer.loop(1000, this, this.tipsLoop);
					}
					Http.DATA_TYPE = 'FORMDATA';
				}))
				this.lab_num.visible = true;
				this.submitBtn.disabled = false;
				Laya.timer.loop(1000, this, this.onLoop);
			}
			else {
				this.tips = 3;
				this.img_singn.visible = true;
				this.text_tips.text = "请检查您的手机号码!";
				this.text_tips.width = 330;
				this.img_singn.width = 330;
				this.img_singn.centerX = 1;
				Laya.timer.loop(1000, this, this.tipsLoop);
			}

		}

		private onSubmit(): void {
			var phone = this.mobile.text || ''
			var userCode = this.commonlyUsed.UrlSearch();
			let code = this.code.text || ''
			if (this.mobile.text == "" || this.mobile.text.length != 11 || this.code.text == "") {
				this.submitBtn.disabled = true;
				this.tips = 2;
				this.img_singn.visible = true;
				this.text_tips.text = "请输入正确的手机号码和验证码!";
				this.text_tips.width = 400;
				this.img_singn.width = 400;
				this.img_singn.centerX = 1;
				Laya.timer.loop(1000, this, this.tipsLoop);
			}
			else {
				Http.DATA_TYPE = 'JSON';
				rest.user.sMsg({ code, phone }, (data => {
					if (data.code == 1) {
						this.confirmChangePasswordPage = new view.ConfirmChangePasswordPage(phone, code, userCode);
						this.confirmChangePasswordPage.on("CLOSE", this, this.onClickClose);
						Laya.stage.addChild(this.confirmChangePasswordPage);
					} else {
						this.submitBtn.disabled = true;
						this.tips = 2;
						this.img_singn.visible = true;
						this.text_tips.text = data.msg;
						this.text_tips.width = data.msg.length * 36;
						this.img_singn.width = data.msg.length * 36;
						this.img_singn.centerX = 1;
						Laya.timer.loop(1000, this, this.tipsLoop);
					}
					Http.DATA_TYPE = 'FORMDATA';
				}))
			}
		}

		private onClickClose(): void {

			if (this.parent != null) {
				this.parent.removeChild(this);
				this.img_singn.visible = false;
				Laya.timer.clear(this, this.tipsLoop);
			}
		}

		private onLoop(): void {
			this.num--;
			this.lab_num.text = "重新发送" + this.num + "秒";
			if (this.num <= 0) {
				this.lab_num.text = "获取验证码";
				this.lab_num.align = "center";
				Laya.timer.clear(this, this.onLoop);
			}
		}

		private tipsLoop(): void {
			this.num = 0;
			this.tips--;
			if (this.tips <= 0) {
				this.img_singn.visible = false;
				if (this.submitBtn.disabled) {
					this.submitBtn.disabled = false;
				}
				Laya.timer.clear(this, this.tipsLoop);
			}
		}
	}
}