/**Created by the LayaAirIDE*/
module view {

	export class TreePage extends ui.TreePageUI {
		private game: GamePage;
		private rcord = []
		constructor(game: GamePage) {
			super();
			this.game = game;

			this.list_tree.array = []
			this.btn_close.on(Laya.Event.CLICK, this, this.onClickClose);

			this.onRenderNEWS();
		}

		private onClickClose(): void {
			this.close();
		}

		public onRenderNEWS(): void {
			rest.friend.wateringRecord((data) => {
				console.log(data);
				this.rcord = data.data;
				this.list_tree.array = this.rcord;
			})

			this.list_tree.renderHandler = new Laya.Handler(this, this.onRender);
		}

		private onRender(cell, index): void {

			if (index >= this.rcord.length) {
				return;
			}
			var html_rcord: Laya.HTMLDivElement = cell.getChildByName("html_rcord") as Laya.HTMLDivElement;
			html_rcord.style.lineHeight = 24;
			html_rcord.style.align = "center";
			html_rcord.width = 513;
			html_rcord.innerHTML =
				"<font  style='fontSize:26' color='#056f00'>" + this.rcord[index].nickName + "<font style='fontSize:24' color='#402812'>" + '帮你搬砖,经验值' + "<font style='fontSize:26' color='#056f00'>" + "+" + this.rcord[index].wateringExps + "<br/></font>" + "<br/></font>" + "<br/></font> ";

		}
	}
}