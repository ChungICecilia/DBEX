module view {

    import Handler = laya.utils.Handler;
    import Event = laya.events.Event;
    import Button = laya.ui.Button;
    export class TaskPage extends ui.TaskPageUI {

        private game: GamePage;
        private data = [];
        
        private guid: Laya.Animation;

        constructor(game: GamePage) {
            super();
            this.game = game;
            this.list_task.array = [];

            this.btn_close.on(Laya.Event.CLICK, this, this.onClickClose);

            // switch (UILayout.stepNum) {//Rest.USERINFO.guidMoveSteps) {//
            //     case 6:
            //     case 7:
            this.guid = new Laya.Animation();
            this.guid.loadAnimation("guid.ani", Laya.Handler.create(this, this.onLoaded), null);
            this.guid.visible = false;
            this.addChild(this.guid);
            //         break;
            // }
        }

        // 动画加载完成
        private onLoaded(): void {
            this.guid.on(Laya.Event.COMPLETE, this, null);
        }

        private onClickClose(): void {
            if (7 == UILayout.stepNum) {//Rest.USERINFO.guidMoveSteps
                UILayout.stepNum = 8;
                this.game.ui.onStep(8);
                this.game.ui.guid.visible = true;
                this.guid.rotation = 0;
                this.game.ui.guid.pos(this.game.ui.btn_shareIt.x + (this.game.ui.btn_shareIt.width >> 1) + 10, this.game.ui.btn_shareIt.y - (this.game.ui.btn_shareIt.height - 10));
                this.guid.removeSelf();

            }
            // this.event("redPoint");
            this.game.ui.redPoint();
            this.close();
        }

        public onOpened(): void {
            
            rest.task.tasks(data => {
                this.data = data.data;
                this.list_task.array = this.data;
            })
            this.list_task.renderHandler = new Laya.Handler(this, this.onRender);
            this.list_task.mouseHandler = new Laya.Handler(this, this.onMouse);
        }

        private onRender(cell, index): void {
            if (index >= this.data.length) {
                return;
            }

            var boxTask: Laya.Box = this.list_task.getChildByName("box_task") as Laya.Box;

            var sp_1: Laya.Sprite = cell.getChildByName("sp_1") as Laya.Sprite;
            var img_gift: Laya.Image = sp_1.getChildByName("img_gift") as Laya.Image;
            var img_giftOpen: Laya.Image = sp_1.getChildByName("img_giftOpen") as Laya.Image;
            var text_title: Laya.Text = sp_1.getChildByName("text_title") as Laya.Text;
            var text_rewardQuantity: Laya.Text = sp_1.getChildByName("text_rewardQuantity") as Laya.Text;
            var progressBar: Laya.ProgressBar = sp_1.getChildByName("progressBar") as Laya.ProgressBar;
            var text_probText: Laya.Text = sp_1.getChildByName("text_probText") as Laya.Text;
            var img_complete: Laya.Image = sp_1.getChildByName("img_complete") as Laya.Image;
            var btn_receive: Laya.Button = sp_1.getChildByName("btn_receive") as Laya.Button;
            var btn_undone: Laya.Button = sp_1.getChildByName("btn_undone") as Laya.Button;


            var sp_2: Laya.Sprite = cell.getChildByName("sp_2") as Laya.Sprite;
            var img_checkInGift: Laya.Image = sp_2.getChildByName("img_checkInGift") as Laya.Image;
            var img_checkInGiftOpen: Laya.Image = sp_2.getChildByName("img_checkInGiftOpen") as Laya.Image;
            var text_checkInReward: Laya.Text = sp_2.getChildByName("text_checkInReward") as Laya.Text;
            var img_checkInComplete: Laya.Image = sp_2.getChildByName("img_checkInComplete") as Laya.Image;
            var btn_checkInReceive: Laya.Image = sp_2.getChildByName("btn_checkInReceive") as Laya.Image;
            var btn_undone2: Laya.Button = sp_2.getChildByName("btn_undone2") as Laya.Button;
            var text_title2: Laya.Text = sp_2.getChildByName("text_title2") as Laya.Text;


            var sp_3: Laya.Sprite = cell.getChildByName("sp_3") as Laya.Sprite;
            var text_transactionTitle: Laya.Text = sp_3.getChildByName("text_transactionTitle") as Laya.Text;
            var text_transactionText: Laya.Text = sp_3.getChildByName("text_transactionText") as Laya.Text;
            var text_transactionReward: Laya.Text = sp_3.getChildByName("text_transactionReward") as Laya.Text;
            var text_arithmeticForce: Laya.Text = sp_3.getChildByName("text_arithmeticForce") as Laya.Text;
            var text_BBMQuantity: Laya.Text = sp_3.getChildByName("text_BBMQuantity") as Laya.Text;



            console.log(this.data[index]);
            switch (this.data[index].task.type) {
                case 1:
                    // let num = Rest.USERINFO.baseBbm + Rest.USERINFO.starterBbm;
                    sp_2.visible = false;
                    sp_1.visible = false;
                    sp_3.visible = true;
                    text_arithmeticForce.text = "+" + (+this.data[index].instance.pickedCoins / Rest.OPTION.transform_rate).toFixed(4);

                    text_transactionTitle.text = this.data[index].task.taskDesc;
                    text_BBMQuantity.text = this.data[index].instance.pickedCoins;
                    text_transactionText.text = this.data[index].task.rewardsDesc;
                    this.data[index].instance.pickedCoins != 0 ? text_arithmeticForce.text = '+' + (+this.data[index].instance.pickedCoins / Rest.OPTION.transform_rate).toFixed(4) : text_arithmeticForce.text = '+' + 0;
                    break;

                // case 2:
                //     sp_2.visible = true;
                //     text_checkInReward.text = this.data[index].task.rewardsCalcPoints;
                //     if (this.data[index].instance.rewardStatus == 1) {
                //         img_checkInGift.visible = true;
                //         btn_checkInReceive.visible = true;
                //     } else {
                //         img_checkInGift.visible = false;
                //         btn_checkInReceive.visible = false;
                //         img_checkInGiftOpen.visible = true;
                //         img_checkInComplete.visible = true;
                //     }

                //     switch (UILayout.stepNum) {//Rest.USERINFO.guidMoveSteps
                //         case 6:
                //             var point = sp_2.localToGlobal(new Laya.Point(0, 0));
                //             // console.log("point.x:" + point.x, "point.y:" + point.y);
                //             this.guid.pos(point.x + btn_checkInReceive.x + (btn_checkInReceive.width >> 1), point.y + btn_checkInReceive.y - btn_checkInReceive.height - 10);
                //             // this.guid.pos(this.btn_close.x + (this.btn_close.width >> 1),this.btn_close.y - (this.btn_close.height >> 1) );
                //             this.guid.play(0, true, "ani1");
                //             this.guid.visible = true;
                //             // this.list_task.refresh();
                //             break;
                //         case 7:
                //             // this.guid.pos(this.btn_close.x + (this.btn_close.width >> 1), this.btn_close.y - (this.btn_close.height));
                //             // this.guid.play(0, true, "ani1");
                //             // this.guid.visible = true;
                //             break;
                //     }

                //     break;

                case 2:
                    sp_3.visible = false;
                    sp_1.visible = false;
                    sp_2.visible = true;
                    text_title2.text = this.data[index].task.taskDesc;
                    text_checkInReward.text = this.data[index].task.rewardsCalcPoints;
                    if (this.data[index].instance.finishStatus == 1) {
                        img_checkInGift.visible = true;
                        btn_undone2.visible = true;
                    } else {
                        if (this.data[index].instance.rewardStatus == 1) {
                            img_checkInGift.visible = true;
                            btn_checkInReceive.visible = true;
                        } else {
                            img_checkInGift.visible = false;
                            btn_checkInReceive.visible = false;
                            img_checkInGiftOpen.visible = true;
                            img_checkInComplete.visible = true;
                        }
                    }
                    break;
                case 3:
                    sp_3.visible = false;
                    sp_2.visible = false;
                    sp_1.visible = true;
                    text_title.text = this.data[index].task.taskDesc;
                    text_rewardQuantity.text = "" + this.data[index].task.rewardsCalcPoints;
                    text_probText.text = this.data[index].instance.pickedCoins + "/" + this.data[index].task.requirePickedCoins;
                    progressBar.value = this.data[index].instance.pickedCoins / this.data[index].task.requirePickedCoins;
                    switch (this.data[index].instance.rewardStatus) {
                        case 1:
                            if (this.data[index].instance.finishStatus == 1) {
                                img_gift.visible = true;
                                btn_undone.visible = true;
                            } else {
                                img_gift.visible = true;
                                btn_receive.visible = true;
                                btn_undone.visible = false;
                            }
                            break;

                        case 2:
                            img_gift.visible = false;
                            btn_receive.visible = false;
                            img_giftOpen.visible = true;
                            img_complete.visible = true;
                            break;
                    }
                    break;
                case 4:
                    sp_3.visible = false;
                    sp_1.visible = false;
                    sp_2.visible = true;
                    text_title2.text = this.data[index].task.taskDesc;
                    text_checkInReward.text = this.data[index].task.rewardsCalcPoints;
                    if (this.data[index].instance.finishStatus == 1) {
                        img_checkInGift.visible = true;
                        btn_checkInReceive.visible = true;
                    } else {
                        if (this.data[index].instance.rewardStatus == 1) {
                            img_checkInGift.visible = true;
                            btn_checkInReceive.visible = true;
                        } else {
                            img_checkInGift.visible = false;
                            btn_checkInReceive.visible = false;
                            img_checkInGiftOpen.visible = true;
                            img_checkInComplete.visible = true;
                        }
                    }

                    switch (UILayout.stepNum) {//Rest.USERINFO.guidMoveSteps
                        case 6:
                            var point = sp_2.localToGlobal(new Laya.Point(0, 0));
                            // console.log("point.x:" + point.x, "point.y:" + point.y);
                            this.guid.pos(point.x + btn_checkInReceive.x + (btn_checkInReceive.width >> 1), point.y + btn_checkInReceive.y - btn_checkInReceive.height - 10);
                            // this.guid.pos(this.btn_close.x + (this.btn_close.width >> 1),this.btn_close.y - (this.btn_close.height >> 1) );
                            this.guid.play(0, true, "ani1");
                            this.guid.visible = true;
                            // this.list_task.refresh();
                            break;
                        case 7:
                            var point = sp_2.localToGlobal(new Laya.Point(0, 0));
                            this.guid.pos(point.x + this.btn_close.x - (this.btn_close.width >> 1), 170);
                            this.guid.rotation = 270;
                            this.guid.play(0, true, "ani1");
                            this.guid.visible = true;
                            break;
                    }
                    break;
            }
        }

        private onMouse(e: Event, index: number): void {
            if (e.type == Event.CLICK) {
                let cell = e.target;

                if (cell instanceof Button) {
                    if (this.data[index].task.type == 4) {
                        if (6 == UILayout.stepNum) {//Rest.USERINFO.guidMoveSteps
                            UILayout.stepNum = 7;
                            this.guid.visible = true;
                            this.game.ui.onStep(7);
                            var point = this.btn_close.localToGlobal(new Laya.Point(0, 0));
                            this.guid.pos(point.x + (this.btn_close.width >> 1), point.y - (this.btn_close.height));
                            // this.guid.pos(point.x,point.y);
                            this.list_task.refresh();
                        }

                    }

                    var taskId = this.data[index].instance.id;
                    var inviteCode: '';
                    rest.task.askRewards(taskId, (data) => {
                        rest.user.userInfo(inviteCode, (data => {
                            console.log(data);
                            Rest.USERINFO = data.data.player;
                            if (data.data) {
                                this.game.event('UPDATEUSER');
                                this.data[index].instance.rewardStatus = 2
                                this.list_task.array = this.data;
                            }
                        }))
                    })
                }
            }
        }

    }

}