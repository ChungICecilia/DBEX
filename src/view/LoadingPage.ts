/**
* name 
*/
module view{
	export class LoadingPage extends Laya.Sprite{
		private ani:Laya.Animation;
		public static loadingPage:LoadingPage;
		
		constructor(){
			super();
			
			this.mouseEnabled = true;
			this.mouseThrough = false;
			this.size(750,1207);
			
			
			
			
		}

		public static loading():void
		{
			if(LoadingPage.loadingPage && LoadingPage.loadingPage.parent)
			{
				
				return;
			}
			if(!LoadingPage.loadingPage)
			{
				LoadingPage.loadingPage = new LoadingPage();
				LoadingPage.loadingPage.zOrder = 10001;

			}
			LoadingPage.loadingPage.mouseThrough = false;
			LoadingPage.loadingPage.makeAni();
			Laya.stage.addChild(LoadingPage.loadingPage);
			
		}

		public static dispose():void
		{
			if(LoadingPage.loadingPage)
			{
				LoadingPage.loadingPage.removeSelf();
				LoadingPage.loadingPage.dispose();
			}
			
		}
		
		public makeAni():void
		{
			this.ani = new Laya.Animation();
            //加载动画文件
            this.ani.loadAnimation("loading1.ani");
            //添加到舞台
            this.addChild(this.ani);
			this.ani.x = Laya.stage.width/2;
			this.ani.y = Laya.stage.height/2;
            //播放Animation动画
            this.ani.play();
			
			
		}

		private dispose():void
		{
			if(this.ani)
			{
				this.ani.stop();
				this.ani.removeSelf();
				this.ani.destroy();
				this.ani = null;
			}
			
		}
	}
}