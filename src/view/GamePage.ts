// 程序入口
module view {

    import SoundManager = Laya.SoundManager;
    import Handler = Laya.Handler;
    export class GamePage extends Laya.Sprite {

        public backgroud: view.Background;
        public ui: view.UILayout;
        public Answer: view.AnswerPage;
        private ChooseHead: view.ChooseHead;
        private ChangeGalaxyPage: view.ChangeGalaxyPage;
        private CalcPointPage: view.CalcPointPage;
        private noticePanel:NoticePanel;
        private prizePage: view.PrizePage;

        private round: Laya.Rectangle;
        private yun: Laya.Animation;

        private music: boolean;
        private buttonSound: boolean;
        public first: boolean;

        constructor() {
            super();
            this.music = true;
            this.buttonSound = true;
            this.first = false;
            // this.yun = new Laya.Animation();
            // this.yun.loadAnimation("yun.ani", Laya.Handler.create(this, this.onLoaded), null);
            //加载图集资源 
            Laya.loader.load(["res/atlas/animal/lou.atlas", "res/atlas/uiLayout.atlas", "res/atlas/chooseHead.atlas",
                "res/atlas/animal/jingdutiao.atlas", "res/atlas/animal/wallet.atlas", "res/atlas/calcPoint.atlas", "res/atlas/wallet.atlas","res/atlas/chong.atlas"],
                new Handler(this, this.init));

        }

        public init(): void {

            this.backgroud = new view.Background(this);
            this.addChild(this.backgroud);
            // this.ui.img_bg.skin = "res/singleImages/bg.jpg";
            var inviteCode: '';
            rest.user.userInfo(inviteCode, (data => {
                if (data.status == "error") {
                    // GamePage.first = true;
                    if (this.ChooseHead == null) {
                        this.ChooseHead = new view.ChooseHead(this);
                        this.ChooseHead.on("CreatedRole", this, this.onCreatedRole);
                    }
                    this.ChooseHead.popup(true, true);
                } else {
                    //console.log(data)
                    if (data.data.player.refGalaxyId == null) {
                        if (this.ChangeGalaxyPage == null) {
                            this.ChangeGalaxyPage = new view.ChangeGalaxyPage(this.ChooseHead);
                        }
                        Laya.stage.addChild(this.ChangeGalaxyPage);
                    }
                    Rest.USERINFO = data.data.player;
                    Rest.GALAXY = data.data.byDbexGalaxy;
                    Rest.SKIP = data.data.isSkip;
                }

                this.ui = new view.UILayout(this);
                this.addChild(this.ui);
                this.ui.on('Sounds', this, this.playSounds);
                this.on('Sounds', this, this.playSounds);
                if (Rest.USERINFO.guidMoveSteps != 0) {
                    if (this.CalcPointPage == null) {
                        this.CalcPointPage = new CalcPointPage(this);
                    }
                    this.CalcPointPage.popup(false, true);
                }



            }))
            rest.chong.getPrizeNotice((data) => {
                var datas: any = data.data;
                if(datas && datas.length>0)
                {
                    
                    if (this.prizePage == null) {
                        this.prizePage = new PrizePage(this);
                        this.prizePage.setData(datas[0]);
                    }
                    this.prizePage.popup(false, true);
                }

            });

            // this.addChild(this.yun);
            // this.yun.pos(((Laya.stage.width - this.round.width) >> 1) + 365, ((Laya.stage.height - this.round.height) >> 1) - 100);
            // this.yun.play(0, true, "yun");

            // this.ui = new view.UILayout(this);
            // this.addChild(this.ui);

            switch (Rest.USERINFO.bgmStatus) {
                case 1:
                    
                    SoundManager.playMusic("res/sounds/dbexBGM.mp3", 0, new Handler(this, this.onComplete));
                    this.onPlayMusic(true);
                    break;
                case 2:
                    this.onPlayMusic(false);
                    break;
            }
            switch (Rest.USERINFO.btnMusicStatus) {
                case 1:
                    this.onPlaySound(true);
                    break;
                case 2:
                    this.onPlaySound(false);
                    break;
            }

            // this.ui.on('Sounds', this, this.playSounds);
            this.on('GameMusic', this, this.onPlayMusic);
            this.on('GameSound', this, this.onPlaySound);
            //更新用户信息
            this.on('UPDATEUSER', this, this.onUpdateUser);
            this.event('CLOSE');
            if (this.noticePanel == null) {
                        this.noticePanel = new NoticePanel();
            }
            this.noticePanel.popup(false, true);
        }

        //背景音乐
        private onPlayMusic(music): void {
            this.music = music
            if (!music) {
                SoundManager.stopMusic();
            }
        }

        //音效
        private onPlaySound(sound) {
            this.buttonSound = sound;
            if (!sound) {
                SoundManager.stopAllSound();
            }
        }

        private onUpdateUser() {
            this.ui.updateUserInfo();
        }

        private playSounds(num): void {
            console.log(num);
            switch (num) {
                case 1:
                    if (this.buttonSound)
                        SoundManager.playSound("res/sounds/click.mp3", 1, new Handler(this, this.onComplete));
                    break;
                case 2:
                    if (this.buttonSound)
                        SoundManager.playSound("res/sounds/receive.mp3", 1, new Handler(this, this.onComplete));
                    break;
                case 3:
                    if (this.buttonSound)
                        SoundManager.playSound("res/sounds/watering.mp3", 1, new Handler(this, this.onComplete));
                    break;
                case 4:
                    if (this.buttonSound)
                        SoundManager.playSound("res/sounds/addBBM.mp3", 1, new Handler(this, this.onComplete));
                    break;
                case 5:
                    if (this.buttonSound)
                        SoundManager.playSound("res/sounds/open.mp3", 1, new Handler(this, this.onComplete));
                    break;
                case 6:
                    if (this.buttonSound)
                        SoundManager.playSound("res/sounds/answerRight.mp3", 1, new Handler(this, this.onComplete));
                    break;
                case 7:
                    if (this.buttonSound)

                        SoundManager.playSound("res/sounds/gainPrize.mp3", 1, new Handler(this, this.onComplete));
                    break;
                case 8:
                    if (this.buttonSound)
                        SoundManager.playSound("res/sounds/noPrize.mp3", 1, new Handler(this, this.onComplete));
                    break;
            }
        }

        private onComplete(): void {
            console.log("播放完成");
        }

        public onCreatedRole(): void {

            this.ui.updateUserInfo();
            this.ui.userTree();
            this.ui.bbmShow();
            this.ui.notic();
            this.ui.redPoint();
            this.ui.PlayTreeAni();
        }

        // 加载完成
        // private onLoaded(): void {
        //     this.yun.on(Laya.Event.COMPLETE, this, null);
        //     this.round = this.yun.getBounds();
        //     // this.init();
        // }


    }
}